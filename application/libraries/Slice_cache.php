<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slice_cache extends Slice {

    protected $CI;
    
    /**
	 *  All of the compiler methods used by Slice to simulate 
	 *  Laravel Blade Template
	 *
	 *  @var   array
	 */
	private $_compilers	= array(
		'directive',
		'comment',
		'ternary',
		'preserved',
		'echo',
		'variable',
		'forelse',
		'empty',
		'endforelse',
		'opening_statements',
		'else',
		'continueIf',
		'continue',
		'breakIf',
		'break',
		'closing_statements',
		'each',
		'unless',
		'endunless',
		'includeIf',
		'include',
		'extends',
		'yield',
		'show',
		'opening_section',
		'closing_section',
		'php',
		'endphp',
		'lang',
		'choice'
	);
	
	// --------------------------------------------------------------------------

    public function view($template, $data = NULL, $return = FALSE)
	{
		if (isset($data))
		{
			$this->set($data);
		}

		//	Compile and execute the template
		$content = $this->_run($this->_compile($template), $this->_data);

		if ( ! $return)
		{
			$this->CI->output->append_output($content);
		}

		return $content;
	}
    
    protected function _compile($template)
	{
        $this->CI->load->helper('string');
        $this->CI->load->model('Settings_model');
        $wc = $this->CI->Settings_model->get_setting('file_cache_duration');
        $ct = $this->CI->Settings_model->get_setting('caching_type');
        $n = $this->CI->Settings_model->get_setting('cache_duration');
        
		$view_path	= $this->exists($template, TRUE);
		$cache_name	= 'slice-'.md5($view_path);

		//	Verifies if exists a cached version of the file
		if ($cached_version = $this->CI->cache->$ct->get($cache_name))
		{
			if (ENVIRONMENT == 'production')
			{
				return $cached_version;
			}

			$cached_meta = $this->CI->cache->$ct->get_metadata($cache_name);

			if ($cached_meta['mtime'] > filemtime($view_path))
			{
				return $cached_version;
			}
		}

		$content = file_get_contents($view_path);

		//	Compile the content
		foreach ($this->_compilers as $compiler)
		{
			$method = "_compile_{$compiler}";
			$content = $this->$method($content);
		}

		//	Store in the cache
		$this->CI->cache->$ct->save($cache_name, $content, $n);

		return $content;
	}
    
}