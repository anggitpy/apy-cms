<?php
/*
|--------------------------------------------------------------------------
| Image Preset Sizes
|--------------------------------------------------------------------------
|
| Specify the preset sizes you want to use in your code. Only these preset 
| will be accepted by the controller for security.
|
| Each preset exists of a width and height. If one of the dimensions are 
| equal to 0, it will automatically calculate a matching width or height 
| to maintain the original ratio.
|
| If both dimensions are specified it will automatically crop the 
| resulting image so that it fits those dimensions.
|
*/

$config["image_sizes"]["single"] = array(770, 380);
$config["image_sizes"]["heading_big"] = array(586, 490);
$config["image_sizes"]["heading_small"]   = array(293, 245);
$config["image_sizes"]["hcategory"]   = array(368, 300);
$config["image_sizes"]["channel"]   = array(330, 260);
$config["image_sizes"]["thumb_big"]   = array(270, 200);
$config["image_sizes"]["thumb_med"]   = array(100, 80);
$config["image_sizes"]["thumb_small"]   = array(80, 70);