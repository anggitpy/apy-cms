<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Ad_model extends CI_Model 
{
    
    function __construct()
	{
		parent::__construct();
		$this->load->model('Settings_model');
		$this->load->helper('string');
	}
    
    public function count_inventory_client($client_id)
    {
        $this->db->from('ads');				
		$this->db->where('ads_client', $client_id);				 
		return $this->db->count_all_results();	
    }
    
    public function clients_json()
	{		
		$data = array();
		
		$page = $this->input->get('page')-1;
		$per_page = $this->input->get('per_page');
		$sort_init = $this->input->get('sort');
		$sort = str_replace('|',' ',$sort_init);
		$filter = $this->input->get('filter');	
		
		$offset = $per_page * $page;						
		
		$this->db->from('ads_client');				
		
		$this->db->order_by($sort);
		
		$this->db->group_start();
		$this->db->like('client_name', $filter);
		$this->db->or_like('client_address', $filter);
		$this->db->or_like('client_phone', $filter);
		$this->db->or_like('client_email', $filter);
		$this->db->or_like('client_cp', $filter);		
		$this->db->group_end();
		
		$this->db->limit($per_page, $offset);	
		
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{						
				$data[] = array(
                    'client_id' => $row['client_id'],
                    'client_name' => $row['client_name'],
                    'client_address' => $row['client_address'],
                    'client_phone' => $row['client_phone'],
                    'client_email' => $row['client_email'],
                    'client_cp' => $row['client_cp'],
                    'client_added' => $row['client_added'],
                    'agent_id' => $row['user_id'],
                    'agent_name' => $this->ion_auth->user($row['user_id'])->row()->first_name,
                    'count_inventory' => $this->count_inventory_client($row['client_id'])
                );
			}
		}
		$query->free_result();  
		return $data;	
	}
    
    public function count_clients_json()
	{		
		$data = array();
		
		$page = $this->input->get('page')-1;
		$per_page = $this->input->get('per_page');
		$sort_init = $this->input->get('sort');
		$sort = str_replace('|',' ',$sort_init);
		$filter = $this->input->get('filter');	
		
		$offset = $per_page * $page;						
		
		$this->db->from('ads_client');				
		
		$this->db->order_by($sort);
		
		$this->db->group_start();
		$this->db->like('client_name', $filter);
		$this->db->or_like('client_address', $filter);
		$this->db->or_like('client_phone', $filter);
		$this->db->or_like('client_email', $filter);
		$this->db->or_like('client_cp', $filter);		
		$this->db->group_end();
		
		$this->db->limit($per_page, $offset);					 
		return $this->db->count_all_results();	
	}
    
    public function inventory_json()
	{		
		$data = array();
		
		$page = $this->input->get('page')-1;
		$per_page = $this->input->get('per_page');
		$sort_init = $this->input->get('sort');
		$sort = str_replace('|',' ',$sort_init);
		$filter = $this->input->get('filter');	
		
		$offset = $per_page * $page;						
		
		$this->db->from('ads');
        $this->db->join('ads_client', 'client_id = ads_client');
		
		$this->db->order_by($sort);
		
		$this->db->group_start();
		$this->db->like('client_name', $filter);
		$this->db->or_like('ads_description', $filter);
		$this->db->or_like('ads_start_date', $filter);
		$this->db->or_like('ads_end_date', $filter);		
		$this->db->group_end();
		
		$this->db->limit($per_page, $offset);	
		
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{						
				$data[] = array(
                    'ads_id' => $row['ads_id'],
                    'client_name' => $row['client_name'],
                    'ads_description' => $row['ads_description'],
                    'ads_name' => $row['ads_name'],
                    'ads_start_date' => $row['ads_start_date'],
                    'ads_end_date' => $row['ads_end_date'],
                    'ads_code' => $row['ads_code'],
                    'client_email' => $row['client_email'],
                    'client_cp' => $row['client_cp'],
                    'agent_id' => $row['ads_user_id'],
                    'agent_name' => $this->ion_auth->user($row['ads_user_id'])->row()->first_name,
                );
			}
		}
		$query->free_result();  
		return $data;	
	}
    
    public function count_inventory_json()
	{		
		$data = array();
		
		$page = $this->input->get('page')-1;
		$per_page = $this->input->get('per_page');
		$sort_init = $this->input->get('sort');
		$sort = str_replace('|',' ',$sort_init);
		$filter = $this->input->get('filter');	
		
		$offset = $per_page * $page;						
		
		$this->db->from('ads');
        $this->db->join('ads_client', 'client_id = ads_client');
		
		$this->db->order_by($sort);
		
		$this->db->group_start();
		$this->db->like('client_name', $filter);
		$this->db->or_like('ads_description', $filter);
		$this->db->or_like('ads_start_date', $filter);
		$this->db->or_like('ads_end_date', $filter);		
		$this->db->group_end();
		
		$this->db->limit($per_page, $offset);					 
		return $this->db->count_all_results();	
	}
	
	public function save_inventory()
	{
		$user = $this->ion_auth->user()->row();
		$data = array(
			'ads_description' => $this->input->post('ads_description'),
			'ads_name' => $this->input->post('ads_name'),
			'ads_start_date' => $this->input->post('ads_start_date'),
			'ads_end_date' => $this->input->post('ads_end_date'),
			'ads_code' => $this->input->post('ads_code'),
			'ads_default' => $this->input->post('ads_default'),
			'ads_client' => $this->input->post('ads_client'),
			'ads_user_id' => $user->id
		);
		$this->db->insert('ads', $data);
		return;
	}
	
	public function update_inventory($ads_id)
	{
		$user = $this->ion_auth->user()->row();
		$data = array(
			'ads_description' => $this->input->post('ads_description'),
			'ads_name' => $this->input->post('ads_name'),
			'ads_start_date' => $this->input->post('ads_start_date'),
			'ads_end_date' => $this->input->post('ads_end_date'),
			'ads_code' => $this->input->post('ads_code'),
			'ads_default' => $this->input->post('ads_default'),
			'ads_client' => $this->input->post('ads_client'),
			'ads_user_id' => $user->id
		);
		$this->db->where('ads_id', $ads_id);
		$this->db->update('ads', $data);
		return;
	}
	
	public function select_client()
	{
		$data = array();
		$this->db->from('ads_client');		
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			$data[''] = 'Select Client';
			foreach ($query->result_array() as $row)
			{
				$data[$row['client_id']] = $row['client_name'];
			}
		}
		$query->free_result();  
		return $data;			
	}
	
	public function select_inventory_by_id($ads_id)
	{
		$data = array();
		$this->db->from('ads');			
		$this->db->where('ads_id', $ads_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			$data = $query->row();
		}
		$query->free_result();  
		return $data;
	}
	
	public function select_client_by_id($client_id)
	{
		$data = array();
		$this->db->from('ads_client');			
		$this->db->where('client_id', $client_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			$data = $query->row();
		}
		$query->free_result();  
		return $data;
	}
	
	public function save_client()
	{
		$user = $this->ion_auth->user()->row();
		$date = date('Y-m-d');
		$data = array(
			'client_name' => $this->input->post('client_name'),
			'client_address' => $this->input->post('client_address'),
			'client_phone' => $this->input->post('client_phone'),
			'client_email' => $this->input->post('client_email'),
			'client_cp' => $this->input->post('client_cp'),
			'client_added' => $date,
			'user_id' => $user->id
		);
		$this->db->insert('ads_client', $data);
		return $this->db->insert_id();
	}
	
	public function update_client($client_id)
	{
		$user = $this->ion_auth->user()->row();
		$date = date('Y-m-d');
		$data = array(
			'client_name' => $this->input->post('client_name'),
			'client_address' => $this->input->post('client_address'),
			'client_phone' => $this->input->post('client_phone'),
			'client_email' => $this->input->post('client_email'),
			'client_cp' => $this->input->post('client_cp'),
			'client_added' => $date,
			'user_id' => $user->id
		);
		$this->db->where('client_id', $client_id);
		$this->db->update('ads_client', $data);
		return;
	}
	
	public function insert_asset($post_image_data)
	{
		$date = date('Y-m-d');
		$user = $this->ion_auth->user()->row();	
		$data = array(
			'file_name' => $post_image_data['file_name'],
			'banner_url' => $this->input->post('url'),
			'description' => $this->input->post('description'),
			'date' => $date,
			'user_id' => $user->id,
		);
		$this->db->insert('ads_assets', $data);
		return $this->db->insert_id();
	}
	
	public function get_assets($count = NULL)
	{
		$data = array();		

		$page = $this->input->get('page')-1;
		$per_page = $this->input->get('per_page');
		$sort_init = $this->input->get('sort');
		$sort = str_replace('|',' ',$sort_init);
		$filter = $this->input->get('filter');		
		
		$offset = $per_page * $page;
		
		$this->db->from('ads_assets');
		$this->db->join('users', 'id = user_id');
		$this->db->order_by($sort);
		
		$this->db->group_start();
		$this->db->like('description', $filter);
		$this->db->group_end();
		
		if($count)
		{
			return $this->db->count_all_results(); 
		}
		else
		{
			$this->db->limit($per_page, $offset);			
			$query = $this->db->get();
			if ($query->num_rows() > 0)
			{
				foreach ($query->result() as $row)
				{
					$data[] = $row;
				}
			}
			$query->free_result();    
			return $data; 
		}		
	}
	
}