<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Posts_model extends CI_Model 
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('slug');
		$this->load->helper('text');
		$this->load->helper('image');
		$this->load->library('image_lib');
	}
	

	public function get_subtitles()
	{
		$data = array();
		$this->db->from('subtitle');
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$data[] = $row;
			}
		}
		$query->free_result();  
		return $data;
	}
	
	public function get_posts($count = NULL, $post_type = NULL)
	{
		$data = array();
		$user = $this->ion_auth->user()->row();

		$page = $this->input->get('page')-1;
		$per_page = $this->input->get('per_page');
		$sort_init = $this->input->get('sort');
		$sort = str_replace('|',' ',$sort_init);
		$filter = $this->input->get('filter');		
		
		$offset = $per_page * $page;
		
		$this->db->from('posts');	
		$this->db->join('subtitle','posts.post_subtitle = subtitle.subtitle_id', 'left');
		$this->db->join('categories','posts.post_category = categories.cat_id');
		$this->db->join('users','id = post_author');
		if($post_type)
			$this->db->where('post_type', $post_type);

		$this->db->order_by($sort);
		
		$this->db->group_start();
		$this->db->like('post_title', $filter);
		$this->db->or_like('subtitle', $filter);
		$this->db->or_like('cat_name', $filter);
		$this->db->or_like('post_content', $filter);
		$this->db->or_like('post_excerpt', $filter);
		$this->db->group_end();
		
		if($count)
		{
			return $this->db->count_all_results(); 
		}
		else
		{
			$this->db->limit($per_page, $offset);			
			$query = $this->db->get();
			if ($query->num_rows() > 0)
			{
				foreach ($query->result() as $row)
				{
					$data[] = array(
						'post_id' => $row->post_id,
						'post_title_limit' => word_limiter($row->post_title,4),
						'post_title' => $row->post_title,
						'post_subtitle' => $row->post_subtitle,
						'post_author' => $row->post_author,
						'post_slug' => $row->post_slug,
						'post_category' => $row->post_category,
						'post_excerpt' => $row->post_excerpt,
						'post_content' => $row->post_content,
						'post_status' => $row->post_status,
						'post_type' => $row->post_type,
						'post_views' => $row->post_views,
						'comment_status' => $row->comment_status,
						'post_sticky' => $row->post_sticky,
						'post_featured' => $row->post_featured,
						'image_id' => $row->image_id,
						'post_image' => $row->post_image,
						'image_title' => $row->image_title,
						'image_caption' => $row->image_caption,
						'post_date' => $row->post_date,
						'post_time' => $row->post_time,
						'post_date_modified' => $row->post_date_modified,
						'subtitle_id' => $row->subtitle_id,
						'subtitle' => $row->subtitle,
						'subtitle_date_created' => $row->subtitle_date_created,
						'subtitle_modified' => $row->subtitle_modified,
						'cat_id' => $row->cat_id,
						'cat_name' => $row->cat_name,
						'category_slug' => $row->category_slug,
						'category_description' => $row->category_description,
						'category_icon' => $row->category_icon,
						'parent_id' => $row->parent_id,
						'first_name' => $row->first_name,
						'tags' => $this->get_tags($row->post_id),
					);
				}
			}
			$query->free_result();    
			return $data; 
		}		
	}
	
	public function get_tags_by_post($post_id)
	{
		$data = array();
		$this->db->from('post_tag');
		$this->db->join('tags', 'post_tag.tag_id = tags.tag_id');
		$this->db->where('post_id', $post_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$data[] = $row['tag_id'];
			}
		}
		$query->free_result();  
		return $data;
	}
	
	public function get_tags($post_id)
	{
		$data = array();
		$this->db->from('post_tag');
		$this->db->join('tags', 'post_tag.tag_id = tags.tag_id');
		$this->db->where('post_id', $post_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$data[] = $row;
			}
		}
		$query->free_result();  
		return $data;
	}
	
	public function get_images($count = NULL)
	{
		$data = array();
		$user = $this->ion_auth->user()->row();

		$page = $this->input->get('page')-1;
		$per_page = $this->input->get('per_page');
		$sort_init = $this->input->get('sort');
		$sort = str_replace('|',' ',$sort_init);
		$filter = $this->input->get('filter');		
		
		$offset = $per_page * $page;
		
		$this->db->from('images');	

		$this->db->order_by($sort);
		
		$this->db->group_start();
		$this->db->like('image_title', $filter);
		$this->db->or_like('image_caption', $filter);
		$this->db->or_like('image_file_name', $filter);
		$this->db->group_end();
		
		if($count)
		{
			return $this->db->count_all_results(); 
		}
		else
		{
			$this->db->limit($per_page, $offset);			
			$query = $this->db->get();
			if ($query->num_rows() > 0)
			{
				foreach ($query->result() as $row)
				{
					$data[] = array(
						'image_id' => $row->image_id,
						'attached' => $row->attached,
						'image_title' => $row->image_caption,
						'image_file_name' => $row->image_file_name,
						'image_file_size' => $row->image_file_size,
						'image_thumb' => $row->image_thumb,
						'timestamp' => $row->timestamp,
						'to_editor' => image('/uploads/'.$row->image_file_name, 'heading_big')
					);
				}
			}
			$query->free_result();    
			return $data; 
		}		
	}
	
	public function get_single_post($post_id)
	{
		$data = array();
		$this->db->from('posts');	
		$this->db->join('subtitle','posts.post_subtitle = subtitle.subtitle_id', 'left');
		$this->db->join('categories', 'categories.cat_id = posts.post_category');
		$this->db->join('images', 'images.image_id = posts.image_id');
		$this->db->where('posts.post_id', $post_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			$data = $query->row();
		}
		$query->free_result();  
		return $data;
	}
	
	public function get_single_image($image_id)
	{
		$data = array();
		$this->db->from('images');			
		$this->db->where('image_id', $image_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			$data = $query->row();
		}
		$query->free_result();  
		return $data;
	}
	
	public function insert_post($post_image_data)
	{
		$date = date('Y-m-d');
		$time = date('H:i:s');
		$user = $this->ion_auth->user()->row();		
		
		$data_post = array(
			'post_title' => $this->input->post('post_title'),
			'post_subtitle' => $this->input->post('post_subtitle'),
			'post_author' => $user->id,
			'post_slug' => slug($this->input->post('post_title')),
			'post_category' => $this->input->post('post_category'),
			'post_excerpt' => $this->input->post('post_excerpt'),
			'post_content' => $this->input->post('post_content'),
			'post_status' => $this->input->post('post_status'),
			'post_sticky' => $this->input->post('post_sticky'),
			'post_featured' => $this->input->post('post_featured'),
			'youtube' => $this->input->post('youtube'),
			'comment_status' => $this->input->post('comment_status'),
			'post_image' => $post_image_data['file_name'],
			'image_title' => $this->input->post('image_title'),
			'image_caption' => $this->input->post('image_caption'),	
			'post_keywords' => $this->input->post('keywords'),	
			'scheduled_date' => $this->input->post('scheduled_date'),
			'post_date' => $date,
			'post_time' => $time,
		);
		$data_image = array(
			'attached' => 1,
			'image_title' => $this->input->post('image_title'),
			'image_caption' => $this->input->post('image_caption'),
			'image_file_name' => $post_image_data['file_name'],
			'image_file_size' => $post_image_data['file_size'],
			'image_thumb' => $post_image_data['raw_name'].'_thumb'.$post_image_data['file_ext'],
		);		
		$this->db->trans_start();	
		
		$this->db->insert('posts', $data_post);
		
		$post_id = $this->db->insert_id();
		$tags = $this->input->post('tags');	
		if($tags)
		{			
			foreach($tags as $tag)
			{
				$data_tag[] = array (
					'post_id' => $post_id,
					'tag_id' => $tag
				);
			}	
			$this->db->insert_batch('post_tag', $data_tag);	
		}				
		
		if(!empty($this->input->post('image_id')))
		{
			$image_id = $this->input->post('image_id');
		}
		else
		{
			$this->db->insert('images', $data_image);
			$image_id = $this->db->insert_id();
		}		
				
		$data_update_post = array(
			'image_id' => $image_id,
		);
		$this->db->where('post_id', $post_id);
		$this->db->update('posts', $data_update_post);
		
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) 
		{			
			$this->db->trans_rollback();
			return FALSE;
		} 
		else 
		{
			return $post_id;
		}		
	}
	
	public function update_post($post_id, $post_image_data)
	{
		$post = $this->get_single_post($post_id);
		$date = date('Y-m-d');
		$time = date('H:i:s');
		$user = $this->ion_auth->user()->row();		
		
		if(!empty($this->input->post('image_id')))
		{
			$image_id = $this->input->post('image_id');
			$data_image = $this->get_single_image($image_id);
			$post_image_data['file_name'] = $data_image->image_file_name;			
		}
		else
		{
			$image_id = $post->image_id;			
		}
		
		$data_post = array(
			'post_title' => $this->input->post('post_title'),
			'post_subtitle' => $this->input->post('post_subtitle'),
			'post_author' => $user->id,
			'post_slug' => slug($this->input->post('post_title')),
			'post_category' => $this->input->post('post_category'),
			'post_excerpt' => $this->input->post('post_excerpt'),
			'post_content' => $this->input->post('post_content'),
			'post_status' => $this->input->post('post_status'),
			'post_sticky' => $this->input->post('post_sticky'),
			'post_featured' => $this->input->post('post_featured'),
			'youtube' => $this->input->post('youtube'),
			'comment_status' => $this->input->post('comment_status'),
			'post_image' => $post_image_data['file_name'],
			'post_keywords' => $this->input->post('keywords'),	
			'image_id' => $image_id,
			'image_title' => $this->input->post('image_title'),
			'image_caption' => $this->input->post('image_caption'),
			'scheduled_date' => $this->input->post('scheduled_date'),
		);
		$data_image = array(
			'attached' => 1,
			'image_title' => $this->input->post('image_title'),
			'image_caption' => $this->input->post('image_caption'),
			'image_file_name' => $post_image_data['file_name'],
			'image_file_size' => $post_image_data['file_size'],
			'image_thumb' => $post_image_data['raw_name'].'_thumb'.$post_image_data['file_ext'],
		);		
		$this->db->trans_start();	
		
		$this->db->where('post_id', $post_id);
		$this->db->update('posts', $data_post);
		
		$this->db->where('post_id', $post_id);
		$this->db->delete('post_tag');					
		$tags = $this->input->post('tags');	
		if($tags)
		{			
			foreach($tags as $tag)
			{
				$data_tag[] = array (
					'post_id' => $post_id,
					'tag_id' => $tag
				);
			}	
			$this->db->insert_batch('post_tag', $data_tag);	
		} 		
		
		if (!empty($_FILES['post_image']['name']))
		{
			$this->db->insert('images', $data_image);
			$image_id = $this->db->insert_id();		
			$data_update_post = array(
				'image_id' => $image_id,
			);
			$this->db->where('post_id', $post_id);
			$this->db->update('posts', $data_update_post);
		}
		
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) 
		{			
			$this->db->trans_rollback();
			return FALSE;
		} 
		else 
		{
			return TRUE;
		}		
	}
    
    // Pilih Kategori di Post
	public function select_category()
	{
		$data = array();
		$this->db->from('categories');		
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			$data[''] = 'Select Category';
			foreach ($query->result_array() as $row)
			{
				$data[$row['cat_id']] = $row['cat_name'];
			}
		}
		$query->free_result();  
		return $data;			
	}
	
	//Pilih Tag di Post
	public function select_tag()
	{
		$data = array();
		$this->db->from('tags');	
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$data[$row['tag_id']] = $row['tag_name'];
			}
		}
		$query->free_result();  
		return $data;				
	}
	
}