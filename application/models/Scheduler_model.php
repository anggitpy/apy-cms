<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Scheduler_model extends CI_Model 
{
	
	public function __construct()
	{
		parent::__construct();
	}
    
    public function get_scheduled_posts($count = NULL)
	{
		$data = array();
		$date = date('Y-m-d');
		$time = date('H:i:s');
				
		$this->db->from('posts');			
		$this->db->where('post_status', 'scheduled');	
		$this->db->where('DATE(scheduled_date) <=', $date);
		$this->db->where('TIME(scheduled_date) <=', $time);
		
		if($count)
		{
			return $this->db->count_all_results();
		}
		else
		{
			$query = $this->db->get();
			if ($query->num_rows() > 0)
			{
				foreach ($query->result() as $row)
				{
					$data[] = $row;
				}
			}
			$query->free_result();    
			return $data; 		
		}							
	}
	
	public function publish_scheduled_posts()
	{
		$date = date('Y-m-d');
		$time = date('H:i:s');
		$posts = $this->get_scheduled_posts();
		if($posts)			
		{
			foreach($posts as $post)
			{
				$data[] = array(
					'post_id' => $post->post_id,
					'post_status' => 'publish',
					'post_date' => $date,
					'post_time' => $time
				);
			}
			$this->db->update_batch('posts', $data, 'post_id');
			return TRUE;
		}
		else
		{
			return FALSE;
		}		
	}
	
}