<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Breaking_model extends CI_Model 
{
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function save_news()
	{
		$date = date('Y-m-d');
		$time = date('H:i:s');
		$data = array(
			'text' => $this->input->post('text'),
			'date' => $date,
			'time' => $time,
		);
		$this->db->insert('breaking_news', $data);
		return;
	}
	
	public function update_news($br_id)
	{
		$date = date('Y-m-d');
		$time = date('H:i:s');
		$data = array(
			'text' => $this->input->post('text'),
			'date' => $date,
			'time' => $time,
		);
		$this->db->where('br_id', $br_id);
		$this->db->update('breaking_news', $data);
		return;
	}
    
	public function get_breaking($count = NULL)
	{
		$data = array();		

		$page = $this->input->get('page')-1;
		$per_page = $this->input->get('per_page');
		$sort_init = $this->input->get('sort');
		$sort = str_replace('|',' ',$sort_init);
		$filter = $this->input->get('filter');		
		
		$offset = $per_page * $page;
		
		$this->db->from('breaking_news');	
		$this->db->order_by($sort);
		
		$this->db->group_start();
		$this->db->like('text', $filter);
		$this->db->or_like('date', $filter);
		$this->db->group_end();
		
		if($count)
		{
			return $this->db->count_all_results(); 
		}
		else
		{
			$this->db->limit($per_page, $offset);			
			$query = $this->db->get();
			if ($query->num_rows() > 0)
			{
				foreach ($query->result() as $row)
				{
					$data[] = $row;
				}
			}
			$query->free_result();    
			return $data; 
		}		
	}
	
	public function get_breaking_by_id($br_id)
	{
		$data = array();
		$this->db->from('breaking_news');					
		$this->db->where('br_id', $br_id);		
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			$data = $query->row();
		}
		$query->free_result();  
		return $data;			
	}
	
	
}