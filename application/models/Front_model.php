<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Front_model extends CI_Model 
{
	
	public function __construct()
	{
		parent::__construct();		
	}
	
	public function search_post_box($limit, $offset, $count = NULL)
	{
		$search_query = $this->input->get('q');
		$data = array();
		$this->db->from('posts');
		$this->db->join('subtitle','posts.post_subtitle = subtitle.subtitle_id', 'left');
		$this->db->join('categories', 'categories.cat_id = posts.post_category');
		$this->db->group_start();
		$this->db->like('post_title', $search_query);
		$this->db->or_like('post_content', $search_query);
		$this->db->group_end();
		$this->db->limit($limit, $offset);
		if($count)
		{
			return $this->db->count_all_results();
		}
		else
		{
			$query = $this->db->get();
			if ($query->num_rows() > 0)
			{
				foreach ($query->result() as $row)
				{
					$data[] = $row;
				}
			}
			$query->free_result();  
			return $data;
		}
	}
	
	public function get_posts_by_sub($sub_id, $limit, $offset, $count = NULL)
	{
		$data = array();
		$this->db->from('posts');	
		$this->db->join('subtitle','posts.post_subtitle = subtitle.subtitle_id', 'left');
		$this->db->where('posts.post_subtitle', $sub_id);			
		$this->db->limit($limit, $offset);			
		if($count)
		{
			return $this->db->count_all_results();
		}
		else
		{
			$query = $this->db->get();
			if ($query->num_rows() > 0)
			{
				foreach ($query->result() as $row)
				{
					$data[] = $row;
				}
			}
			$query->free_result();  
			return $data;
		}
	}
	
	public function get_posts_in_tags($tag_id, $limit, $offset, $count = NULL) // Tags Taxonomy
	{
		$data = array();
		$this->db->from('post_tag');	
		$this->db->join('posts','posts.post_id = post_tag.post_id');
		$this->db->join('subtitle','posts.post_subtitle = subtitle.subtitle_id', 'left');
		$this->db->where('posts.post_status', 'publish');
		$this->db->where('posts.post_type', 'post');
		$this->db->where_in('post_tag.tag_id', $tag_id);			
		$this->db->limit($limit, $offset);			
		if($count)
		{
			return $this->db->count_all_results();
		}
		else
		{
			$query = $this->db->get();
			if ($query->num_rows() > 0)
			{
				foreach ($query->result() as $row)
				{
					$data[] = $row;
				}
			}
			$query->free_result();  
			return $data;
		}
	}
	
	public function get_similar_posts($post_id, $limit)
	{
		$post = $this->get_post($post_id);
		$tags = $this->similar_tags($post_id);
		
		$data = array();

		if(empty($tags))
		{
			$this->db->from('posts');
			$this->db->join('subtitle','posts.post_subtitle = subtitle.subtitle_id', 'left');
			$this->db->where('posts.post_id !=', $post_id);
			$this->db->where('posts.post_status', 'publish');
			$this->db->where('posts.post_type', 'post');
			$this->db->where_in('posts.post_category', $post->post_category);	
		}
		else
		{
			
			$this->db->from('post_tag');	
			$this->db->join('posts','posts.post_id = post_tag.post_id');
			$this->db->join('subtitle','posts.post_subtitle = subtitle.subtitle_id', 'left');		
			
			$this->db->group_start();
			$this->db->where('posts.post_id !=', $post_id);
			$this->db->where('posts.post_status', 'publish');
			$this->db->where('posts.post_type', 'post');
			$this->db->where_in('post_tag.tag_id', $tags);		
			$this->db->group_end();
					
		}
		$this->db->order_by('posts.post_id','desc');
		$this->db->limit($limit);	
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{			
			foreach ($query->result() as $row)
			{
				$data[] = $row;
			}
		}
		$query->free_result();  
		return $data;	
		
	}
	
	public function get_categories()
	{
		$data = array();
		$this->db->from('categories');		
		$this->db->where('sort !=', NULL);		
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{			
			foreach ($query->result() as $row)
			{
				$data[] = $row;
			}
		}
		$query->free_result();  
		return $data;			
	}
	
	public function get_category($cat_id)
	{
		$data = array();
		$this->db->from('categories');					
		$this->db->where('cat_id', $cat_id);		
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			$data = $query->row();
		}
		$query->free_result();  
		return $data;			
	}
	
	public function get_subtitle($sub_id)
	{
		$data = array();
		$this->db->from('subtitle');					
		$this->db->where('subtitle_id', $sub_id);		
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			$data = $query->row();
		}
		$query->free_result();  
		return $data;			
	}
	
	public function get_tag($tag_id)
	{
		$data = array();
		$this->db->from('tags');					
		$this->db->where('tag_id', $tag_id);		
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			$data = $query->row();
		}
		$query->free_result();  
		return $data;			
	}
    
    public function get_posts($cat = NULL, $post_type = NULL, $post_featured = NULL, $limit = NULL, $offset = NULL, $exception = NULL, $count = NULL)
	{
		$data = array();
		$this->db->from('posts');	
		$this->db->join('subtitle','posts.post_subtitle = subtitle.subtitle_id', 'left');
		$this->db->join('categories', 'categories.cat_id = posts.post_category');
		$this->db->join('images', 'images.image_id = posts.image_id');
		
		if($limit || $offset)
			$this->db->limit($limit, $offset);
	
		$this->db->where('post_status', 'publish');
		$this->db->where('post_type', 'post');		
		
		if($cat)
		{
			$this->db->where('post_category', $cat);			
			$this->db->or_where('parent_id', $cat);			
		}
		
		if($post_type)
			$this->db->where('post_type', $post_type);
		
		if($post_featured)
			$this->db->where('post_featured', $post_featured);
		
		if($exception)
		{
			foreach($exception as $ex)
			{
				$this->db->where('post_category !=', $ex);
			}
		}
		
		$this->db->order_by('post_date', 'desc');
		$this->db->order_by('post_time', 'desc');
		
		if($count)
		{
			return $this->db->count_all_results();
		}
		else
		{
			$query = $this->db->get();
			if ($query->num_rows() > 0)
			{
				foreach ($query->result() as $row)
				{
					$data[] = $row;
				}
			}
			$query->free_result();  
			return $data;
		}
		
	}
	
	public function get_popular_posts($limit, $interval)
	{
		$data = array();
						
		$today = date('Y-m-d');				
		$date = date_create(date('Y-m-d'));
		date_sub($date, date_interval_create_from_date_string("$interval days"));
		$date_limit = date_format($date, 'Y-m-d');
		
		$this->db->from('popular_cache');	
		$this->db->join('posts', 'popular_cache.post_id = posts.post_id');	
		$this->db->join('subtitle','posts.post_subtitle = subtitle.subtitle_id', 'left');
		$this->db->join('categories', 'categories.cat_id = posts.post_category');
		$this->db->join('images', 'images.image_id = posts.image_id');	

		$this->db->limit($limit);
		$this->db->order_by('post_count','desc');
		
		$this->db->where('post_date >=', $date_limit);
		//$this->db->where('post_date <=', $today);
		
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$data[] = $row;
			}
		}
		$query->free_result();  
		return $data;
	}
	
	public function get_post($post_id, $counter = NULL)
	{
		$data = array();
		$this->db->from('posts');	
		$this->db->join('subtitle','posts.post_subtitle = subtitle.subtitle_id', 'left');
		$this->db->join('categories', 'categories.cat_id = posts.post_category');
		$this->db->join('images', 'images.image_id = posts.image_id');
		$this->db->where('posts.post_id', $post_id);
				
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			$data = $query->row();
		}
		$query->free_result();  
		return $data;
	}
	
	public function get_tags($post_id)
	{
		$data = array();
		$this->db->from('post_tag');
		$this->db->join('tags', 'post_tag.tag_id = tags.tag_id');
		$this->db->where('post_id', $post_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$data[] = $row;
			}
		}
		$query->free_result();  
		return $data;
	}
	
	public function get_latest_trending_tags($limit)
	{
		$data = array();
		$this->db->from('tags');		
		$this->db->where('trending', 1);
		$this->db->limit($limit);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$data[] = $row;
			}
		}
		$query->free_result();  
		return $data;
	}
	
	public function similar_tags($post_id)
	{
		$data = array();
		$this->db->from('post_tag');
		$this->db->join('tags', 'post_tag.tag_id = tags.tag_id');
		$this->db->where('post_id', $post_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$data[] = $row->tag_id;
			}
		}
		$query->free_result();  
		return $data;
	}
	
	function update_counter($id) 
	{
		$this->db->where('post_id', $id);
		$query = $this->db->get('popular_cache');
		if ($query->num_rows() > 0)
		{
			$this->db->set('post_views', 'post_views + 1', FALSE);	
			$this->db->set('post_count', 'post_views / 3', FALSE);
			$this->db->where('post_id', $id);
			$this->db->update('popular_cache');			
		}
		else
		{
			$data = array(
				'post_id' => $id,
				'post_views' => 1,
				'post_count' => 1
			);
			$this->db->insert('popular_cache', $data);	
		}
			
	}
	
}