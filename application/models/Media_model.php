<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Media_model extends CI_Model 
{
	
	public function __construct()
	{
		parent::__construct();
	}
    
    public function get_media($count = NULL)
	{
		$data = array();		

		$page = $this->input->get('page')-1;
		$per_page = $this->input->get('per_page');
		$sort_init = $this->input->get('sort');
		$sort = str_replace('|',' ',$sort_init);
		$filter = $this->input->get('filter');		
		
		$offset = $per_page * $page;
		
		$this->db->from('images');	
		$this->db->order_by($sort);
		
		$this->db->group_start();
		$this->db->like('image_title', $filter);
		$this->db->or_like('image_caption', $filter);
		$this->db->group_end();
		
		if($count)
		{
			return $this->db->count_all_results(); 
		}
		else
		{
			$this->db->limit($per_page, $offset);			
			$query = $this->db->get();
			if ($query->num_rows() > 0)
			{
				foreach ($query->result() as $row)
				{
					$data[] = $row;
				}
			}
			$query->free_result();    
			return $data; 
		}		
	}
	
	public function insert_media($image_data)
	{
		$data = array(
			'attached' => 0,
			'image_title' => $this->input->post('image_title'),
			'image_caption' => $this->input->post('image_caption'),
			'image_file_name' => $image_data['file_name'],
			'image_file_size' => $image_data['file_size'],
			'image_thumb' => $image_data['raw_name'].'_thumb'.$image_data['file_ext'],
		);
		$this->db->insert('images', $data);
		return;
	}
	
}