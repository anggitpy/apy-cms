<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Category_model extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('slug');
	}
	
	public function get_popular_category($limit) // for footer
	{
		$data = array();
		$this->db->select('COUNT(apy_posts.post_category) AS count_cat, cat_id, cat_name, category_slug, category_description');
		$this->db->from('posts');			
		$this->db->join('categories', 'categories.cat_id = posts.post_category');
		$this->db->group_by('posts.post_category');
		$this->db->order_by('COUNT(apy_posts.post_category)','desc');
		$this->db->limit(12);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{			
			foreach ($query->result() as $row)
			{
				$data[] = $row;
			}
		}
		$query->free_result();  
		return $data;	
	}	
	
	public function get_category_by_id($cat_id)
	{
		$data = array();
		$this->db->from('categories');	
		$this->db->where('cat_id', $cat_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			$data = $query->row();
		}
		$query->free_result();  
		return $data;
	}
	
	public function update_category($cat_id)
	{
		$data = array(
			'cat_name' => $this->input->post('category_name'),
			'category_slug' => slug($this->input->post('category_name')),
			'category_description' => $this->input->post('description'),
			'parent_id' => $this->input->post('category_parent'),
		);
		$this->db->where('cat_id', $cat_id);
		$this->db->update('categories', $data);
		return;
	}
	
	public function insert_category()
	{
		$data = array(
			'cat_name' => $this->input->post('category_name'),
			'category_slug' => slug($this->input->post('category_name')),
			'category_description' => $this->input->post('description'),
			'parent_id' => $this->input->post('category_parent'),
		);
		$this->db->insert('categories', $data);
		return;
	}
	
    // Select Category Parent
	public function category_parent()
	{
		$this->load->database();
		$data = array();
		$this->db->from('categories');
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			$data[''] = 'Pilih Kategori';
			foreach ($query->result_array() as $row)
			{
				$data[$row['cat_id']] = $row['cat_name'];
			}
		}
		$query->free_result();  
		return $data;			
	}
    
	public function get_all_categories()
	{
		$this->load->database();
		$data = array();
		$this->db->from('categories');
		$this->db->where('cat_id !=', 1);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$data[] = $row;
			}
		}
		$query->free_result();  
		return $data;			
	}
    
    public function get_categories()
    {
        $this->db->select('*');
        $this->db->from('categories');
        $this->db->where('parent_id', 0);

        $parent = $this->db->get();
        
        $categories = $parent->result();
        $i=0;
        foreach($categories as $p_cat){

            $categories[$i]->sub = $this->sub_categories($p_cat->cat_id);
            $i++;
        }
        return $categories;
    }

    public function sub_categories($id)
    {
        $this->db->select('*');
        $this->db->from('categories');
        $this->db->where('parent_id', $id);

        $child = $this->db->get();
        $categories = $child->result();
        $i=0;
        foreach($categories as $p_cat){

            $categories[$i]->sub = $this->sub_categories($p_cat->cat_id);
            $i++;
        }
        return $categories;       
    }    
	
	public function delete($cat_id)
	{		
		$this->load->model('Front_model');
		$posts = $this->Front_model->get_posts($cat_id);
		foreach($posts as $post)
		{			
			$data_post = array (				
				'post_category' => 1
			);		
			$this->db->where('post_id', $post->post_id);
			$this->db->update('posts', $data_post);
		}
		//$this->db->update_batch('posts', $data_post, 'post_id');	
		$this->db->where('cat_id', $cat_id);
		$this->db->delete('categories');
		return;
	}
	
}