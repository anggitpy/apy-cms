<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Subtitles_model extends CI_Model 
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('slug');
	}
    
   public function get_subtitles($count = NULL)
	{
		$data = array();		

		$page = $this->input->get('page')-1;
		$per_page = $this->input->get('per_page');
		$sort_init = $this->input->get('sort');
		$sort = str_replace('|',' ',$sort_init);
		$filter = $this->input->get('filter');		
		
		$offset = $per_page * $page;
		
		$this->db->from('subtitle');	
		$this->db->order_by($sort);
		
		$this->db->group_start();
		$this->db->like('subtitle', $filter);		
		$this->db->group_end();
		
		if($count)
		{
			return $this->db->count_all_results(); 
		}
		else
		{
			$this->db->limit($per_page, $offset);			
			$query = $this->db->get();
			if ($query->num_rows() > 0)
			{
				foreach ($query->result() as $row)
				{
					$data[] = $row;
				}
			}
			$query->free_result();    
			return $data; 
		}		
	}
	
	public function save_subtitle()
	{
		$date = date('Y-m-d');
		$data = array(
			'subtitle' => $this->input->post('subtitle'),
			'subtitle_date_created' => $date
		);
		$this->db->insert('subtitle', $data);
		return $this->db->insert_id();
	}
	
	public function get_subtitle_by_id($subtitle_id)
	{		
		$data = array();			
		$this->db->from('subtitle');	
		$this->db->where('subtitle_id', $subtitle_id);				
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			$data = $query->row();
		}
		$query->free_result();    
		return $data; 
	}
	
}