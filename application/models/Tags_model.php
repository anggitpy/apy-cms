<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Tags_model extends CI_Model 
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('slug');
	}
    
	public function get_tags($count = NULL)
	{
		$data = array();		

		$page = $this->input->get('page')-1;
		$per_page = $this->input->get('per_page');
		$sort_init = $this->input->get('sort');
		$sort = str_replace('|',' ',$sort_init);
		$filter = $this->input->get('filter');		
		
		$offset = $per_page * $page;
		
		$this->db->from('tags');	
		$this->db->order_by($sort);
		
		$this->db->group_start();
		$this->db->like('tag_name', $filter);
		$this->db->or_like('tag_description', $filter);
		$this->db->group_end();
		
		if($count)
		{
			return $this->db->count_all_results(); 
		}
		else
		{
			$this->db->limit($per_page, $offset);			
			$query = $this->db->get();
			if ($query->num_rows() > 0)
			{
				foreach ($query->result() as $row)
				{
					$data[] = $row;
				}
			}
			$query->free_result();    
			return $data; 
		}		
	}
	
	public function save_tag()
	{
		$date = date('Y-m-d');
		$data = array(
			'tag_name' => $this->input->post('tag_name'),
			'tag_description' => $this->input->post('tag_description'),
			'tag_slug' => slug($this->input->post('tag_name')),
			'trending' => $this->input->post('trending'),
			'tag_updated' => $date
		);
		$this->db->insert('tags', $data);
		return $this->db->insert_id();
	}
	
	public function update_tag($tag_id)
	{
		$date = date('Y-m-d');
		$data = array(
			'tag_name' => $this->input->post('tag_name'),
			'tag_description' => $this->input->post('tag_description'),
			'tag_slug' => slug($this->input->post('tag_name')),
			'trending' => $this->input->post('trending'),
			'tag_updated' => $date
		);
		$this->db->where('tag_id', $tag_id);
		$this->db->update('tags', $data);
		return;
	}
	
	public function get_tag_by_id($tag_id)
	{		
		$data = array();			
		$this->db->from('tags');	
		$this->db->where('tag_id', $tag_id);				
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			$data = $query->row();
		}
		$query->free_result();    
		return $data; 
	}
	
}