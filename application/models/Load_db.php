<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Load_db extends CI_Model 
{
	
	public function __construct()
	{
		parent::__construct();		
	}
    
    public function get_all()
	{				
		$wc = $this->load->database('wc2018', TRUE);
		$data = array();
		$wc->from('kelas');		
		$query = $wc->get();
		if ($query->num_rows() > 0)
		{			
			foreach ($query->result() as $row)
			{
				$data[] = $row;
			}
		}
		$query->free_result();  
		return $data;	
	}
	
}