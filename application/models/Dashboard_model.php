<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Dashboard_model extends CI_Model 
{
	
	public function __construct()
	{
		parent::__construct();
	}
    
	
	public function count_posts($status)
	{
		$this->db->from('posts');
		$this->db->where('post_status', $status);	
		return $this->db->count_all_results();
	}
	
	public function count_categories()
	{
		$this->db->from('categories');		
		return $this->db->count_all_results();
	}
	
	public function count_tags()
	{
		$this->db->from('tags');
		return $this->db->count_all_results();
	}
	
	public function count_ads_inventory()
	{
		$this->db->from('ads');
		return $this->db->count_all_results();
	}
	
	public function count_ads_client()
	{
		$this->db->from('ads_client');
		return $this->db->count_all_results();
	}
	
	public function count_ads_assets()
	{
		$this->db->from('ads_assets');
		return $this->db->count_all_results();
	}
	
	public function count_breaking_news()
	{
		$this->db->from('breaking_news');
		return $this->db->count_all_results();
	}
	
}