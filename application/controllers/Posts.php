<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('print_d');
		$this->load->helper('image');
		$this->load->helper('text');
		$this->load->library('image_lib');
		$this->load->library('upload');
		$this->load->model('Posts_model');
		$group = array('admin');
		if(!$this->ion_auth->in_group($group))
		{
			redirect('auth/login', 'refresh');
		}
	}
	
	public function index()
	{
		$data['title'] = 'Daftar Artikel/Berita';
		$this->slice_cache->view('admin.posts', $data);
	}
	
	public function upload_to_editor()
	{
		if ($_FILES['file']['name']) 
		{
            if (!$_FILES['file']['error']) 
			{
                $name = md5(rand(100, 200));
                $ext = explode('.', $_FILES['file']['name']);
                $filename = $name . '.' . $ext[1];
                $destination = './uploads/' . $filename; 
                $location = $_FILES["file"]["tmp_name"];
                move_uploaded_file($location, $destination);
                echo 'http://test.yourdomain.al/images/' . $filename;
            }
            else
            {
				echo  $message = 'Ooops!  Your upload triggered the following error:  '.$_FILES['file']['error'];
            }
        }
	}
	
	public function post_data()
	{
		$data['total'] = $this->Posts_model->get_posts(1,'post');
		$data['per_page'] = $this->input->get('per_page');
		$data['current_page'] = $this->input->get('page');
		$data['last_page'] = ceil($data['total']/$data['per_page']);
		$data['from'] = ($data['current_page'] * $data['per_page']) - ($data['per_page'] - 1);
		$data['to'] = $data['per_page'] * $data['current_page'];
		$data['data'] = $this->Posts_model->get_posts('','post');
						
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	
	public function add()
	{
		$data['title'] = 'Tambah Berita';
		$data['post_category'] = $this->Posts_model->select_category();
		$data['select_tag'] = $this->Posts_model->select_tag();
		$data['comment_status'] = array(
			'' => 'Comment Status',
			'open' => 'Diizinkan',
			'closed' => 'Tertutup'
		);		
		$data['post_status'] = array(
			'' => 'Post Status',
			'publish' => 'Terbit',
			'draft' => 'Draft',
			'scheduled' => 'Terjadwal'
		);	
		$data['post_sticky'] = array(
			'' => 'Sticky?',
			'1' => 'Ya',
			'0' => 'Tidak'
		);	
		$data['post_featured'] = array(
			'' => 'Featured?',
			'1' => 'Ya',
			'0' => 'Tidak'
		);
		$this->slice_cache->view('admin.posts_add', $data);
	}
	
	public function save()
	{
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = 1280;
		$config['file_ext_tolower'] = TRUE;
		$config['max_width'] = 0;
		$config['max_height'] = 0;
		$this->upload->initialize($config);		
		if (!$this->upload->do_upload('post_image'))
		{			
			$error = $this->upload->display_errors('<div class="alert alert-info alert-styled-left alert-bordered"><button type="button" class="close" data-dismiss="alert"><span>×</span></button>', '</div>');
			$this->session->set_flashdata('upload_message', $error);
			$this->session->set_flashdata('pk_message', 'Berhasil menambahkan berita');	
			$post_image_data['file_name'] = $this->input->post('image_file_name');
			$post_id = $this->Posts_model->insert_post($post_image_data);
			redirect('posts/edit/'.$post_id);
		}
		else
		{
			$post_image_data = $this->upload->data();			
			$post_id = $this->Posts_model->insert_post($post_image_data);
			
			$config['image_library'] = 'gd2';
			$config['source_image'] = './uploads/'.$post_image_data['file_name'];
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = FALSE;
			$config['width']         = 100;
			$config['height']       = 50;
			$config['encrypt_name']       = TRUE;
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			
			$this->session->set_flashdata('pk_message', 'Berhasil menambahkan berita');
			redirect('posts/edit/'.$post_id);
		}
	}
	
	public function edit($post_id)
	{		
		$data['post'] = $this->Posts_model->get_single_post($post_id);
		$data['title'] = $data['post']->post_title;
		$data['post_category'] = $this->Posts_model->select_category();
		$data['select_tag'] = $this->Posts_model->select_tag();
		$data['current_tag'] = $this->Posts_model->get_tags_by_post($post_id);
		$data['comment_status'] = array(
			'' => 'Comment Status',
			'open' => 'Diizinkan',
			'closed' => 'Tertutup'
		);		
		$data['post_status'] = array(
			'' => 'Post Status',
			'publish' => 'Terbit',
			'draft' => 'Draft',
			'scheduled' => 'Terjadwal'
		);	
		$data['post_sticky'] = array(
			'' => 'Sticky?',
			'1' => 'Ya',
			'0' => 'Tidak'
		);	
		$data['post_featured'] = array(
			'' => 'Featured?',
			'1' => 'Ya',
			'0' => 'Tidak'
		);
		$this->slice_cache->view('admin.posts_edit', $data);
	}
	
	public function update($post_id)
	{
		$data['post'] = $this->Posts_model->get_single_post($post_id);
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = 1280;
		$config['file_ext_tolower'] = TRUE;
		$config['max_width'] = 0;
		$config['max_height'] = 0;
		$this->upload->initialize($config);	
		
		if (!$this->upload->do_upload('post_image'))
		{			
			$error = $this->upload->display_errors('<div class="alert alert-danger alert-styled-left alert-bordered"><button type="button" class="close" data-dismiss="alert"><span>×</span></button>', '</div>');
			$post_image_data['file_name'] = $data['post']->post_image;
			$this->session->set_flashdata('pk_message', 'Berhasil mengubah berita');			
			$update = $this->Posts_model->update_post($post_id, $post_image_data);
			redirect('posts/edit/'.$post_id);
		}
		else
		{
			$post_image_data = $this->upload->data();			
			$update = $this->Posts_model->update_post($post_id, $post_image_data);
			
			$config['image_library'] = 'gd2';
			$config['source_image'] = './uploads/'.$post_image_data['file_name'];
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = FALSE;
			$config['width']         = 100;
			$config['height']       = 50;
			$config['encrypt_name']       = TRUE;
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			
			$this->session->set_flashdata('pk_message', 'Berhasil mengubah berita');
			redirect('posts/edit/'.$post_id);			
		}		
		
	}
	
	public function image_data()
	{
		$data['total'] = $this->Posts_model->get_images(1);
		$data['per_page'] = $this->input->get('per_page');
		$data['current_page'] = $this->input->get('page');
		$data['last_page'] = ceil($data['total']/$data['per_page']);
		$data['from'] = ($data['current_page'] * $data['per_page']) - ($data['per_page'] - 1);
		$data['to'] = $data['per_page'] * $data['current_page'];
		$data['data'] = $this->Posts_model->get_images();
						
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	
	public function load_subtitle()
	{		
		$data['subtitles'] = $this->Posts_model->get_subtitles();
		
		$term = trim(strip_tags($_GET['term']));
		
		// Rudimentary search
		$matches = array();
		foreach($data['subtitles'] as $subtitle){
			if(stripos($subtitle['subtitle'], $term) !== false){
				// Add the necessary "value" and "label" fields and append to result set
				$subtitle['value'] = $subtitle['subtitle'];
				$subtitle['label'] = "{$subtitle['subtitle']}";
				$matches[] = $subtitle;
			}
		}

		// Truncate, encode and return the results
		$matches = array_slice($matches, 0, 10);
		print json_encode($matches);	
	}
	
	
	
    
}
