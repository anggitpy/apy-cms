<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feed extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('xml');
        $this->load->helper('text');		
	}
	
	public function index()
	{
		$this->load->model('Front_model');
		$data['feed_name'] = 'Poskotanews.com';
		$data['encoding'] = 'utf-8';
		$data['feed_url'] = site_url('feed');
		$data['page_description'] = 'Media Independen Online';
		$data['page_language'] = 'en-en';
		$data['creator_email'] = 'info@poskotanews.com';
        $data['posts'] = $this->Front_model->get_posts('','', '', 15, 0, array(23));      
        header("Content-Type: application/rss+xml");
		$this->load->view('public/rss', $data);
	}

    
}
