<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Media extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Media_model');
		$this->load->helper('form');
		$this->load->library('upload');
		$this->load->library('image_lib');
		$group = array('admin');
		if(!$this->ion_auth->in_group($group))
		{
			redirect('auth/login', 'refresh');
		}
	}
	
	public function index()
	{
		$data['title'] = 'Media Library';
		$this->slice_cache->view('admin.media', $data);
	}
	
	public function media_data()
	{
		$data['total'] = $this->Media_model->get_media(1);
		$data['per_page'] = $this->input->get('per_page');
		$data['current_page'] = $this->input->get('page');
		$data['last_page'] = ceil($data['total']/$data['per_page']);
		$data['from'] = ($data['current_page'] * $data['per_page']) - ($data['per_page'] - 1);
		$data['to'] = $data['per_page'] * $data['current_page'];
		$data['data'] = $this->Media_model->get_media();
						
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	
	public function add()
	{
		$data['title'] = 'Add Image';
		$this->slice_cache->view('admin.media_add', $data);
	}
	
	public function save()
	{
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = 1280;
		$config['file_ext_tolower'] = TRUE;
		$config['max_width'] = 0;
		$config['max_height'] = 0;
		$this->upload->initialize($config);
		if (!$this->upload->do_upload('post_image'))
		{
			$error = $this->upload->display_errors('<div class="alert alert-info alert-styled-left alert-bordered"><button type="button" class="close" data-dismiss="alert"><span>×</span></button>', '</div>');
			$this->session->set_flashdata('upload_message', $error);
			redirect('media/add');
		}
		else
		{
			$image_data = $this->upload->data();			
			$post = $this->Media_model->insert_media($image_data);
			
			$config['image_library'] = 'gd2';
			$config['source_image'] = './uploads/'.$image_data['file_name'];
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = FALSE;
			$config['width']         = 100;
			$config['height']       = 50;
			$config['encrypt_name']       = TRUE;
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			
			$this->session->set_flashdata('pk_message', 'Berhasil menambahkan media');
			redirect('media/');
		}
	}
    
}
