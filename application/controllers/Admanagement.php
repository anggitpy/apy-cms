<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admanagement extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
        $this->load->model('Ad_model');
        $this->load->helper('form');
		$group = array('admin');
		if(!$this->ion_auth->in_group($group))
		{
			redirect('auth/login', 'refresh');
		}
	}
	
	public function index()
	{		
		$data['title'] = 'Ad Management';
		$this->slice_cache->view('admin.admanagement', $data);
	}
	
	public function save_asset()
	{
		$config['upload_path'] = './banner/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = 1280;
		$config['file_ext_tolower'] = TRUE;
		$config['max_width'] = 0;
		$config['max_height'] = 0;
		$config['encrypt_name'] = TRUE;
		$this->upload->initialize($config);		
		if (!$this->upload->do_upload('post_image'))
		{
			$error = $this->upload->display_errors('<div class="alert alert-info alert-styled-left alert-bordered"><button type="button" class="close" data-dismiss="alert"><span>×</span></button>', '</div>');
			$this->session->set_flashdata('upload_message', $error);			
			redirect('admanagement');
		}
		else
		{
			$post_image_data = $this->upload->data();			
			$post_id = $this->Ad_model->insert_asset($post_image_data);
			$this->session->set_flashdata('pk_message', 'Berhasil menambahkan asset');
			redirect('admanagement');
		}
	}
	
	public function asset_data()
	{
		$data['total'] = $this->Ad_model->get_assets(1);
		$data['per_page'] = $this->input->get('per_page');
		$data['current_page'] = $this->input->get('page');
		$data['last_page'] = ceil($data['total']/$data['per_page']);
		$data['from'] = ($data['current_page'] * $data['per_page']) - ($data['per_page'] - 1);
		$data['to'] = $data['per_page'] * $data['current_page'];
		$data['data'] = $this->Ad_model->get_assets();
						
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	
	public function clients()
	{
        $data['title'] = 'Clients';
        $this->slice_cache->view('admin.clients', $data);
	}
    
    public function clients_data_json()
    {
        $data['total'] = $this->Ad_model->count_clients_json();
		$data['per_page'] = $this->input->get('per_page');
		$data['current_page'] = $this->input->get('page');
		$data['last_page'] = ceil($data['total']/$data['per_page']);
		$data['from'] = ($data['current_page'] * $data['per_page']) - ($data['per_page'] - 1);
		$data['to'] = $data['per_page'] * $data['current_page'];
		$data['data'] = $this->Ad_model->clients_json();
						
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
    }
    
    public function add_client()
    {
        $data['title'] = 'Add New Client';
        $this->slice_cache->view('admin.client_add', $data);
    }
	
	public function save_client()
	{
		$save = $this->Ad_model->save_client();
		$this->session->set_flashdata('pk_message', 'Berhasil menambahkan klien');
		redirect('admanagement/clients');
	}
	
	public function edit_client($client_id)
	{
		$data['title'] = 'Edit Client';
		$data['post'] = $this->Ad_model->select_client_by_id($client_id);
        $this->slice_cache->view('admin.client_edit', $data);
	}
	
	public function update_client($client_id)
	{
		$save = $this->Ad_model->update_client($client_id);
		$this->session->set_flashdata('pk_message', 'Berhasil memutakhirkan klien');
		redirect('admanagement/clients');
	}
    
    public function inventory()
    {
        $data['title'] = 'Inventory';
        $this->slice_cache->view('admin.inventory', $data);
    }
    
    public function inventory_data_json()
    {
        $data['total'] = $this->Ad_model->count_inventory_json();
		$data['per_page'] = $this->input->get('per_page');
		$data['current_page'] = $this->input->get('page');
		$data['last_page'] = ceil($data['total']/$data['per_page']);
		$data['from'] = ($data['current_page'] * $data['per_page']) - ($data['per_page'] - 1);
		$data['to'] = $data['per_page'] * $data['current_page'];
		$data['data'] = $this->Ad_model->inventory_json();
						
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
    }
	
	public function add_inventory()
	{
		$data['title'] = 'Add Inventory';
		$data['client'] = $this->Ad_model->select_client();
        $this->slice_cache->view('admin.inventory_add', $data);
	}
	
	public function save_inventory()
	{
		$this->Ad_model->save_inventory();
		$this->session->set_flashdata('pk_message', 'Berhasil menambahkan inventory');
		redirect('admanagement/inventory');
	}
	
	public function edit_inventory($ads_id)
	{
		$data['title'] = 'Edit Inventory';
		$data['client'] = $this->Ad_model->select_client();
		$data['post'] = $this->Ad_model->select_inventory_by_id($ads_id);
        $this->slice_cache->view('admin.inventory_edit', $data);
	}
	
	public function update_inventory($ads_id)
	{
		$this->Ad_model->update_inventory($ads_id);
		$this->session->set_flashdata('pk_message', 'Berhasil mengubah inventory');
		redirect('admanagement/inventory');
	}
    
}
