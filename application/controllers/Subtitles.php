<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subtitles extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Subtitles_model');
		$this->load->helper('form');
		$group = array('admin');
		if(!$this->ion_auth->in_group($group))
		{
			redirect('auth/login', 'refresh');
		}	
	}
	
	public function index()
	{		
		$data['title'] = 'Subtitles';
        $data['subtitles'] = $this->Subtitles_model->get_subtitles();      
        $this->slice_cache->view('admin.subtitles', $data);
	}
	
	public function subtitles_data()
	{
		$data['total'] = $this->Subtitles_model->get_subtitles(1);
		$data['per_page'] = $this->input->get('per_page');
		$data['current_page'] = $this->input->get('page');
		$data['last_page'] = ceil($data['total']/$data['per_page']);
		$data['from'] = ($data['current_page'] * $data['per_page']) - ($data['per_page'] - 1);
		$data['to'] = $data['per_page'] * $data['current_page'];
		$data['data'] = $this->Subtitles_model->get_subtitles();
						
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	
	public function save_subtitle()
	{
		$save = $this->Subtitles_model->save_subtitle();
		$data['response'] = $this->Subtitles_model->get_subtitle_by_id($save);
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
    
}
