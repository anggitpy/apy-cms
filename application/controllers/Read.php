<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Read extends CI_Controller {

	public function __construct()
	{
		parent::__construct();	
		$this->load->model('Front_model');
		$this->load->model('Settings_model');
		$this->load->helper('image');
		$this->load->helper('print_d');
		$this->load->library('user_agent');				
	}
    
    public function index()
    {
        $data['title'] = 'Poskotanews';
		$data['featured_big'] = $this->Front_model->get_posts('','post', 1, 3, 0); // get post big
		$data['featured_small'] = $this->Front_model->get_posts('','post', 1, 4, 3);
		
		$data['recents'] = $this->Front_model->get_posts('','', '', 12, 0, array(23,27)); // get post except 23/27
		$data['popular'] = $this->Front_model->get_popular_posts(12, 7); // get 10 posts from the last 30 days
		
		$data['kriminal_head'] = $this->Front_model->get_posts(16,'','', 1, 0);
		$data['kriminal_prev'] = $this->Front_model->get_posts(16,'','', 3, 1);
		
		$data['mega_head'] = $this->Front_model->get_posts(22,'','', 1, 0);
		$data['mega_prev'] = $this->Front_model->get_posts(22,'','', 3, 1);
		
		$data['videos'] = $this->Front_model->get_posts(24,'','', 6, 0); // Poskota TV
		
		$data['entertainment'] = $this->Front_model->get_posts(2,'','', 1, 0);
		$data['entertainment_prev'] = $this->Front_model->get_posts(2,'','', 3, 1);
		
		$data['sports'] = $this->Front_model->get_posts(25,'','', 1, 0);
		$data['sports_prev'] = $this->Front_model->get_posts(25,'','', 3, 1);
		
		$data['gaya'] = $this->Front_model->get_posts(26,'','', 1, 0);
		$data['gaya_prev'] = $this->Front_model->get_posts(26,'','', 3, 1);
		
		$data['nah'] = $this->Front_model->get_posts(23,'','', 3, 0);
		
		if($this->Settings_model->get_setting('dedicated_mobile') == 1)
		{
			if($this->agent->is_mobile())
			{			
				$this->slice_cache->view('public.'.theme().'.mobile.index', $data);
			}
			else
			{
				$this->slice_cache->view('public.'.theme().'.index', $data);
			}	
		}
		else
		{
			$this->slice_cache->view('public.'.theme().'.index', $data);
		}
			
    }
    
    public function news($id, $post_date, $slug)
    {				
		$this->load->library('disqus');
		$data['disqus'] = $this->disqus->get_html();
		
		$this->add_count($id);		
		$data['post'] = $this->Front_model->get_post($id);
		$data['tags'] = $this->Front_model->get_tags($id);
		$data['recents'] = $this->Front_model->get_posts('','', '', 12, 0, array(23,27));
        $data['title'] = $data['post']->post_title;
		$data['popular'] = $this->Front_model->get_popular_posts(12, 7);
		$data['similar'] = $this->Front_model->get_similar_posts($id, 6);	
		$data['trending_tags'] = $this->Front_model->get_latest_trending_tags(10);
		
		if($this->Settings_model->get_setting('dedicated_mobile') == 1)
		{
			if($this->agent->is_mobile())
			{
				$this->slice_cache->view('public.'.theme().'.mobile.single', $data);
			}
			else
			{			
				$this->slice_cache->view('public.'.theme().'.single', $data);				
			}
		}
		else
		{
			$this->slice_cache->view('public.'.theme().'.single', $data);
		}
		
    }
	
	public function kanal($cat_id, $cat_slug)
	{
		$offset = $this->uri->segment(5);
		$limit = $this->Settings_model->get_setting('archive_per_page');
		$total = $this->Front_model->get_posts($cat_id, '', '', '', '', '', 1);
		
		$data['posts'] = $this->Front_model->get_posts($cat_id, '', '', $limit, $offset, '');
		$data['popular'] = $this->Front_model->get_popular_posts(12, 7);
		$data['trending_tags'] = $this->Front_model->get_latest_trending_tags(10);
		
		$data['recents'] = $this->Front_model->get_posts('','', '', 12, 0, array(23,27));
		$data['category'] = $this->Front_model->get_category($cat_id);
		$data['title'] = $data['category']->cat_name;
		
		$this->load->library('pagination');
		$config['base_url'] = site_url().'/read/kanal/'.$cat_id.'/'.$cat_slug.'/';
		$config['per_page']   = $limit;
		$config['total_rows'] = $total;			
		$config['num_links']   = 5;		
		$config['uri_segment'] = 5;
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li><a class="active" href="#">';
		$config['cur_tag_close'] ='</a></li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '<i class="fa fa-angle-right"></i>';
		$config['prev_link'] = '<i class="fa fa-angle-left"></i>';
		$config['reuse_query_string'] = TRUE;
		$this->pagination->initialize($config);	
		
		$data['page_links'] = $this->pagination->create_links();

		if($this->Settings_model->get_setting('dedicated_mobile') == 1)
		{
			if($this->agent->is_mobile())
			{
				$this->slice_cache->view('public.'.theme().'.mobile.category', $data);
			}
			else
			{
				$this->slice_cache->view('public.'.theme().'.category', $data);	
			}
		}
		else
		{
			$this->slice_cache->view('public.'.theme().'.category', $data);
		}
		
	}
	
	public function tags($tag_id, $tag_slug)
	{
		$offset = $this->uri->segment(5);
		$limit = $this->Settings_model->get_setting('archive_per_page');
		$total = $this->Front_model->get_posts_in_tags($tag_id, '', '', 1);
		
		$data['posts'] = $this->Front_model->get_posts_in_tags($tag_id, $limit, $offset);
		
		$data['recents'] = $this->Front_model->get_posts('','', '', 12, 0, array(23,27));
		$data['popular'] = $this->Front_model->get_popular_posts(12, 7);
		$data['tag'] = $this->Front_model->get_tag($tag_id);
		$data['trending_tags'] = $this->Front_model->get_latest_trending_tags(10);
		
		$data['title'] = $data['tag']->tag_name;
		
		$this->load->library('pagination');
		$config['base_url'] = site_url().'/read/tags/'.$tag_id.'/'.$tag_slug.'/';
		$config['per_page']   = $limit;
		$config['total_rows'] = $total;			
		$config['num_links']   = 5;		
		$config['uri_segment'] = 5;
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li><a class="active" href="#">';
		$config['cur_tag_close'] ='</a></li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '<i class="fa fa-angle-right"></i>';
		$config['prev_link'] = '<i class="fa fa-angle-left"></i>';
		$config['reuse_query_string'] = TRUE;
		$this->pagination->initialize($config);	
		
		$data['page_links'] = $this->pagination->create_links();
		
		if($this->Settings_model->get_setting('dedicated_mobile') == 1)
		{
			if($this->agent->is_mobile())
			{
				$this->slice_cache->view('public.'.theme().'.mobile.tag', $data);	
			}
			else
			{
				$this->slice_cache->view('public.'.theme().'.tag', $data);	
			}		
		}
		else
		{
			$this->slice_cache->view('public.'.theme().'.tag', $data);
		}
		
	}
	
	public function sub($sub_id)
	{
		$offset = $this->uri->segment(4);
		$limit = $this->Settings_model->get_setting('archive_per_page');
		$total = $this->Front_model->get_posts_by_sub($sub_id, '', '', 1);
		
		$data['posts'] = $this->Front_model->get_posts_by_sub($sub_id, $limit, $offset);
		
		$data['recents'] = $this->Front_model->get_posts('','', '', 12, 0, array(23,27));
		$data['popular'] = $this->Front_model->get_popular_posts(12, 7);
		$data['subtitle'] = $this->Front_model->get_subtitle($sub_id);
		$data['trending_tags'] = $this->Front_model->get_latest_trending_tags(10);
		
		$data['title'] = $data['subtitle']->subtitle;
		
		$this->load->library('pagination');
		$config['base_url'] = site_url().'/read/sub/'.$sub_id.'/';
		$config['per_page']   = $limit;
		$config['total_rows'] = $total;			
		$config['num_links']   = 5;		
		$config['uri_segment'] = 5;
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li><a class="active" href="#">';
		$config['cur_tag_close'] ='</a></li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '<i class="fa fa-angle-right"></i>';
		$config['prev_link'] = '<i class="fa fa-angle-left"></i>';
		$config['reuse_query_string'] = TRUE;
		$this->pagination->initialize($config);	
		
		$data['page_links'] = $this->pagination->create_links();
		
		if($this->Settings_model->get_setting('dedicated_mobile') == 1)
		{
			if($this->agent->is_mobile())
			{
				$this->slice_cache->view('public.'.theme().'.mobile.sub', $data);	
			}
			else
			{
				$this->slice_cache->view('public.'.theme().'.sub', $data);
			}
		}
		else
		{
			$this->slice_cache->view('public.'.theme().'.sub', $data);
		}				
	}
	
	public function search()
	{
		$offset = $this->uri->segment(3);
		$limit = $this->Settings_model->get_setting('archive_per_page_index');
		$total = $this->Front_model->search_post_box('', '', 1);
		
		$data['posts'] = $this->Front_model->search_post_box($limit, $offset);
		
		$data['recents'] = $this->Front_model->get_posts('','', '', 12, 0, array(23,27));
		$data['popular'] = $this->Front_model->get_popular_posts(12, 7);		
		$data['trending_tags'] = $this->Front_model->get_latest_trending_tags(10);
		
		$data['title'] = $this->input->get('q');
		$data['total'] = $total;
		
		$this->load->library('pagination');
		$config['base_url'] = site_url().'/read/search/';
		$config['per_page']   = $limit;
		$config['total_rows'] = $total;			
		$config['num_links']   = 5;		
		$config['uri_segment'] = 3;
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li><a class="active" href="#">';
		$config['cur_tag_close'] ='</a></li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '<i class="fa fa-angle-right"></i>';
		$config['prev_link'] = '<i class="fa fa-angle-left"></i>';
		$config['reuse_query_string'] = TRUE;
		$this->pagination->initialize($config);	
		
		$data['page_links'] = $this->pagination->create_links();
		
		if($this->Settings_model->get_setting('dedicated_mobile') == 1)
		{
			if($this->agent->is_mobile())
			{
				$this->slice_cache->view('public.'.theme().'.mobil.search', $data);
			}
			else
			{
				$this->slice_cache->view('public.'.theme().'.search', $data);
			}
		}
		else
		{
			$this->slice_cache->view('public.'.theme().'.search', $data);
		}		
	}
	
	public function indeks()
	{
		$offset = $this->uri->segment(3);
		$limit = $this->Settings_model->get_setting('archive_per_page_index');
		$total = $this->Front_model->get_posts('', '', '', '', '', array(23,27), 1);
		
		$data['posts'] = $this->Front_model->get_posts('', '', '', $limit, $offset, array(23,27));
		$data['popular'] = $this->Front_model->get_popular_posts(12, 7);
		$data['trending_tags'] = $this->Front_model->get_latest_trending_tags(10);
		
		$data['recents'] = $this->Front_model->get_posts('','', '', 12, 0, array(23,27));
		$data['title'] = 'Halaman Indeks';
		
		$this->load->library('pagination');
		$config['base_url'] = site_url().'/read/indeks/';
		$config['per_page']   = $limit;
		$config['total_rows'] = $total;			
		$config['num_links']   = 5;		
		$config['uri_segment'] = 3;
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li><a class="active" href="#">';
		$config['cur_tag_close'] ='</a></li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '<i class="fa fa-angle-right"></i>';
		$config['prev_link'] = '<i class="fa fa-angle-left"></i>';
		$config['reuse_query_string'] = TRUE;
		$this->pagination->initialize($config);	
		
		$data['page_links'] = $this->pagination->create_links();
		
		if($this->Settings_model->get_setting('dedicated_mobile') == 1)
		{
			if($this->agent->is_mobile())
			{
				$this->slice_cache->view('public.'.theme().'.mobile.indeks', $data);
			}
			else
			{
				$this->slice_cache->view('public.'.theme().'.indeks', $data);	
			}
		}
		else
		{
			$this->slice_cache->view('public.'.theme().'.indeks', $data);
		}			
	}
		
	private function add_count($id)
	{
		$this->load->helper('cookie');
		$check_visitor = $this->input->cookie($id, FALSE);
		$ip = $this->input->ip_address();
		if ($check_visitor == false) 
		{
			$cookie = array (
				"name"   => $id,
				"value"  => "$ip",
				"expire" =>  7200,
				"secure" => false
			);
			$this->input->set_cookie($cookie);
			$this->Front_model->update_counter($id);
		}		
	}
	
	public function wordpress($wp_year, $wp_month, $wp_date, $slug)
	{
		//redirect to old site
		//redirect('arsip/'.$wp_year.'/'.$wp_month.'/'.$wp_date.'/'.$slug);
		redirect('http://poskotanews.com/'.$wp_year.'/'.$wp_month.'/'.$wp_date.'/'.$slug, 'location', 301);
	}
    
}