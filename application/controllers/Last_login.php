<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Last_login extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('nav'));
		$group = array('admin');
		if(!$this->ion_auth->in_group($group))
		{
			redirect('auth/login', 'refresh');
		}	
	}
	
	public function index()
	{		
		$data['title'] = 'Dashboard';
		$data['users'] = $this->ion_auth->users()->result();
		$this->slice->view('admin.last_login', $data);
	}
		
}
