<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Dashboard_model');        
		$this->load->model('Settings_model');  
		$this->load->model('Front_model'); 
		$this->load->helper('image');
        $this->load->helper('date');
        
		$group = array('admin');
		if(!$this->ion_auth->in_group($group))
		{
			redirect('auth/login', 'refresh');
		}	
	}
	
	public function index()
	{        
		$data['title'] = 'Dashboard';
		$data['published_post'] = $this->Dashboard_model->count_posts('publish');
		$data['draft_post'] = $this->Dashboard_model->count_posts('draft');
		$data['categories'] = $this->Dashboard_model->count_categories();
		$data['tags'] = $this->Dashboard_model->count_tags();
		$data['ads'] = $this->Dashboard_model->count_ads_inventory();
		$data['ads_client'] = $this->Dashboard_model->count_ads_client();
		$data['ads_assets'] = $this->Dashboard_model->count_ads_assets();
		$data['breaking_news'] = $this->Dashboard_model->count_breaking_news();
		$data['recents'] = $this->Front_model->get_posts('','', '', 5, 0);
		$data['popular'] = $this->Front_model->get_popular_posts(5, 100);
        $this->slice_cache->view('admin.index', $data);
	}

    public function cache_management()
    {
        $ct = $this->Settings_model->get_setting('caching_type');
        $data['title'] = 'Cache Management';
        $data['cache_type'] = $ct;
        $data['cache_info'] = $this->cache->$ct->cache_info();
        $this->slice_cache->view('admin.cache_mgmt', $data);
    }

    public function test()
    {
        $wc = $this->Settings_model->get_setting('file_cache_duration');
        $ct = $this->Settings_model->get_setting('caching_type');
        $n = $this->Settings_model->get_setting('cache_duration');
       
        if($this->cache->$ct->is_supported())
        {
            $data = $this->cache->$ct->get('foo');
            if (!$data){
                echo 'cache miss!<br />';
                $data = 'bar';
                $this->cache->$ct->save('foo',$data, $n);
            }
            echo $data;
            echo '<pre>';
            var_dump($this->cache->$ct->cache_info());
            var_dump($this->cache->get_metadata('foo'));
            echo '</pre>';
        }
    }
    
    public function flushm()
    {
        $memcache_obj = new Memcache;
        $memcache_obj->connect('localhost', 11211);
        $memcache_obj->flush();
        $this->output->delete_cache();
        $clear = $this->cache->clean();
        $this->session->set_flashdata('pk_msg','All Cache Flushed ('.$clear.')');
        redirect('dashboard/cache_management');
    }
	
}
