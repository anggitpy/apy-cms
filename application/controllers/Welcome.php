<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
	}
	
	public function index()
	{
		$this->load->model('Category_model');
        $data['categories'] = $this->Category_model->get_categories();      
        $this->slice->view('public.test.index', $data);
	}
	
	public function api_test()
	{
		$this->load->model('Load_db');
		echo var_dump($this->Load_db->get_all());
	}
	
	public function sandbox()
	{
		echo $this->uri->uri_string();
		echo $this->router->fetch_class();
	}
    
}
