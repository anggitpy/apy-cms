<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scheduler extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Scheduler_model');
		$this->load->model('Settings_model');
	}
	
	public function index()
	{		
		if(is_cli())
		{
			$posts = $this->Scheduler_model->get_scheduled_posts();
			$count_posts = $this->Scheduler_model->get_scheduled_posts(1);
			$scheduler = $this->Scheduler_model->publish_scheduled_posts(); 
			if($scheduler == TRUE)
			{
				$this->Settings_model->cron_log('Scheduler has picked up '.$count_posts.' scheduled posts');
				echo 'Scheduler has picked up '.$count_posts.' scheduled posts'.PHP_EOL;
			}
			else
			{
				echo 'Scheduler has not picked up any scheduled posts'.PHP_EOL;
			}
		}       
	}	

}
