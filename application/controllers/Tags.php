<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tags extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Tags_model');
		$this->load->helper('form');
		$group = array('admin');
		if(!$this->ion_auth->in_group($group))
		{
			redirect('auth/login', 'refresh');
		}	
	}
	
	public function index()
	{		
		$data['title'] = 'Tags';
        $data['tags'] = $this->Tags_model->get_tags();  
		$data['trending'] = array(
			'' => 'Trending?',
			'1' => 'Ya',
			'0' => 'Tidak'
		);	
        $this->slice_cache->view('admin.tags', $data);
	}
	
	public function tags_data()
	{
		$data['total'] = $this->Tags_model->get_tags(1);
		$data['per_page'] = $this->input->get('per_page');
		$data['current_page'] = $this->input->get('page');
		$data['last_page'] = ceil($data['total']/$data['per_page']);
		$data['from'] = ($data['current_page'] * $data['per_page']) - ($data['per_page'] - 1);
		$data['to'] = $data['per_page'] * $data['current_page'];
		$data['data'] = $this->Tags_model->get_tags();
						
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	
	public function save_tag()
	{
		$save = $this->Tags_model->save_tag();
		$data['response'] = $this->Tags_model->get_tag_by_id($save);
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	
	public function edit($tag_id)
	{		
		$data['title'] = 'Edit Tags';
        $data['post'] = $this->Tags_model->get_tag_by_id($tag_id);  
		$data['trending'] = array(
			'' => 'Trending?',
			'1' => 'Ya',
			'0' => 'Tidak'
		);	
        $this->slice_cache->view('admin.tags_edit', $data);
	}
	
	public function update_tag($tag_id)
	{
		$this->Tags_model->update_tag($tag_id);
		$data['response'] = $this->Tags_model->get_tag_by_id($tag_id);
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
    
}
