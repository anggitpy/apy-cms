<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('print_d');
		$this->load->model('Category_model');
		$this->load->model('Settings_model');
		$group = array('admin','editor');
		if(!$this->ion_auth->in_group($group))
		{
			redirect('auth/login', 'refresh');
		}
	}
	
	public function index()
	{		
        $data['title'] = 'Categories';             
        $data['category_parent'] = $this->Category_model->category_parent();
        $data['categories'] = $this->Category_model->get_all_categories(); 
        $this->slice_cache->view('admin.category', $data);
	}	
	
	public function save_category()
	{
		$this->Category_model->insert_category();
		$this->session->set_flashdata('pk_msg','Kategori berhasil ditambahkan');
		redirect('category');
	}
	
	public function edit($cat_id)
	{				
        $data['title'] = 'Categories';             
		$data['category'] = $this->Category_model->get_category_by_id($cat_id);
        $data['category_parent'] = $this->Category_model->category_parent();
        $data['categories'] = $this->Category_model->get_all_categories(); 
        $this->slice_cache->view('admin.category_edit', $data);
	}
	
	public function delete($cat_id)
	{
		$cat = $this->Category_model->get_category_by_id($cat_id);
		$this->Settings_model->set_log('Berhasil menghapus kategori '.$cat_id.'&mdash;'.$cat->cat_name);
		$this->Category_model->delete($cat_id);			
		$this->session->set_flashdata('pk_msg','Kategori berhasil dihapus');
		redirect('category');
	}
	
	public function update_category($cat_id)
	{
		$cat = $this->Category_model->get_category_by_id($cat_id);
		$this->Settings_model->set_log('Berhasil mengubah kategori '.$cat_id.' ('.$cat->cat_name.') menjadi '.$this->input->post('category_name'));
		$this->Category_model->update_category($cat_id);		
		$this->session->set_flashdata('pk_msg','Kategori berhasil diubah');
		redirect('category/edit/'.$cat_id);
	}
    
}
