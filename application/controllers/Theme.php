<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Theme extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('Settings_model');
		$this->load->helper('directory');
		$group = array('admin');
		if(!$this->ion_auth->in_group($group))
		{
			redirect('auth/login', 'refresh');
		}
	}
	
	public function index()
	{	
        $data['maps'] = directory_map('./themes', 1);
        $data['title'] = 'Select Themes';
        $this->slice_cache->view('admin.theme', $data);
	}	

    public function set_theme($theme_name)
    {
        $all_group = array('admin');
		if(!$this->ion_auth->in_group($all_group))
		{
			redirect('auth/login', 'refresh');
		}
		else
		{
			$this->Settings_model->set_theme($theme_name);
            $this->session->set_flashdata('pk_msg','Theme Changed to '.$theme_name);
			redirect('theme');
		}        
    }	
    
}
