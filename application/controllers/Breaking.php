<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Breaking extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Breaking_model');
		$this->load->helper('form');
		$group = array('admin');
		if(!$this->ion_auth->in_group($group))
		{
			redirect('auth/login', 'refresh');
		}
	}
	
	public function index()
	{
		$data['title'] = 'Breaking News';	
        $this->slice_cache->view('admin.breaking', $data);
	}
	
	public function breaking_data()
	{
		$data['total'] = $this->Breaking_model->get_breaking(1);
		$data['per_page'] = $this->input->get('per_page');
		$data['current_page'] = $this->input->get('page');
		$data['last_page'] = ceil($data['total']/$data['per_page']);
		$data['from'] = ($data['current_page'] * $data['per_page']) - ($data['per_page'] - 1);
		$data['to'] = $data['per_page'] * $data['current_page'];
		$data['data'] = $this->Breaking_model->get_breaking();
						
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	
	public function save_news()
	{
		$save = $this->Breaking_model->save_news();
		$this->session->set_flashdata('pk_message', 'Berhasil menambahkan breaking news');
		redirect('breaking');
	}
	
	public function edit($br_id)
	{
		$data['title'] = 'Breaking News Edit';
		$data['post'] = $this->Breaking_model->get_breaking_by_id($br_id);
        $this->slice_cache->view('admin.breaking_edit', $data);
	}
	
	public function update_news($br_id)
	{
		$save = $this->Breaking_model->update_news($br_id);
		$this->session->set_flashdata('pk_message', 'Berhasil memperbarui breaking news');
		redirect('breaking');
	}

}
