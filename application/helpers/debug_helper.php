<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 *	@package Cyb3rNet
 *	@subpackage CI-DEBUG
 *	@version 1.0
 *	@license MIT License
 *
 *	@author Serafim Junior Dos Santos Fagundes <serafim@cyb3r.ca>
 *	@copyright Serafim Junior Dos Santos Fagundes Cyb3r Network
 *
 *	File containing functions for trace debugging
 */


/**
 *	@name CYB3RNET_CI_DEBUG Constant for detection and interaction with other libraries
 */
define( 'CYB3RNET_CI_DEBUG', '20100525_1' );


/**
 *	Output - Output format of _o( <data> );
 */
define( 'O_STYLE1', 'style="font-size:medium;"' );

define( 'O_TAGSTART', '<pre '.O_STYLE1.' >' );
define( 'O_TAGEND', '</pre>' );



/**
 *	Trace - Output format of _t( <data> );
 */
define( 'T_STYLE1', 'style="margin:5px; font-size:0.5em; background-color:#EEE; color:#444; text-shadow:1px 1px 1pz #BBB;' );

define( 'T_TAGSTART', '<code '.T_STYLE1.' >'  );
define( 'T_TAGEND', '</code>' );



/**
 *	A stopper - Output of _a( <data> );
 */
define( 'A_STYLE1', 'style=""' );

define( 'A_TAGSTART', '<pre '.A_STYLE1.' ><code>' );
define( 'A_TAGEND', '</code></pre>' );



/**
 *	Big - Internal use
 */
define( '_B_STYLE1', 'style=""' );

define( '_B_TAGSTART', '<big '._B_STYLE1.' >' );
define( '_B_TAGEND', '</big>' );


/**
 *	Break = Internal Use
 */
define( 'BR_TAG', '<br />' ); 
 
 
 
/**
 *	@var string Global trace debug statements holder
 */
$C3N_g_sStatements = '';


/**
 *	Alerts a message; Ends code processing
 *
 *	@param mixed $mVal Statment to be output on function call
 */
function _a( $mVal )
{
	$sPHPPrint = print_r( $mVal, TRUE );

	if ( strlen( $sPHPPrint ) > 0 )
	{
		$sHTMLed = A_TAGSTART.$sPHPPrint.A_TAGEND;

		if ( defined( 'CI_VERSION' ) )
		{
			show_error( $sHTMLed );
		}
		else
		{
			die( $sHTMLed );
		}
	}
}


/**
 *	Collects statements; doesn't stop code processing
 *
 *	@param mixed $mVal Statement to be collected
 */
function _t( $mVal )
{
	global $C3N_g_sStatements;
	
	static $iCount = 1;
	
	$C3N_g_sStatements .= O_TAGSTART._B_TAGSTART.$iCount++.':'._B_TAGEND.print_r( $mVal, TRUE ).BR_TAG;
}


/**
 *	Echoes the traced statements; doesn't stop code processing
 *
 *	@param bool $bIsHTML Indicates if output is HTMLed
 */
function _o( $bIsHTML = TRUE )
{
	global $C3N_g_sStatements;
	
	if ( strlen( $C3N_g_sStatements ) > 0 )
	{
		if ( $bIsHTML === FALSE )
		{
			echo strip_tags( str_replace( BR_TAG, '\n', $C3N_g_sStatements ) );
		}
		else
		{
			echo O_TAGSTART.$C3N_g_sStatements.O_TAGEND;
		}
	}
}


?>