<?php  echo '<?xml version="1.0" encoding="' . $encoding . '"?>' . "\n"; ?>
<rss version="2.0"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
    xmlns:admin="http://webns.net/mvcb/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:content="http://purl.org/rss/1.0/modules/content/">
 
    <channel>
     
    <title><?php echo $feed_name; ?></title>
 
    <link><?php echo $feed_url; ?></link>
    <description><?php echo $page_description; ?></description>
    <dc:language><?php echo $page_language; ?></dc:language>
    <dc:creator><?php echo $creator_email; ?></dc:creator>
 
    <dc:rights>Copyright <?php echo gmdate("Y", time()); ?></dc:rights>
    <admin:generatorAgent rdf:resource="http://www.poskotanews.com" />
 
    <?php foreach($posts as $post): ?>
     
        <item>
 
			<title><?php echo xml_convert($post->post_title); ?></title>
			<link><?php echo site_url('read/news/' . $post->post_id.'/'.$post->post_date.'/'.$post->post_slug) ?></link>
			<guid><?php echo site_url('read/news/' . $post->post_id.'/'.$post->post_date.'/'.$post->post_slug) ?></guid>
			<media:content
				url="<?php echo base_url('uploads/'.$post->image_file_name) ?>"
				xmlns:media="http://search.yahoo.com/mrss/"
				filesize="<?php echo $post->image_file_size ?>"
				medium="image"
				type="image/jpg"
				width="300"
				height="300"
				lang="en" />
			<description>
				<![CDATA[ <?php echo $post->post_excerpt; ?> ]]>
			</description>
            <pubDate><?php echo $post->post_date; ?></pubDate>
        </item>
 
         
    <?php endforeach; ?>
     
    </channel>
</rss>