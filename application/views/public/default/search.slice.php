@extends('public.default.layouts.master_single')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/bootstrap.min.css" media="screen">	
	<link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/jquery.bxslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/font-awesome.css" media="screen">
	<link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/magnific-popup.css" media="screen">	
	<link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/owl.carousel.css" media="screen">
    <link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/owl.theme.css" media="screen">
	<link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/ticker-style.css"/>
	<link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/app.css" media="screen">
	@include('public.default.partials.style_single')
@endsection

@section('single-newsticker')
<?php foreach(news_ticker(4) as $recent): ?>
<li class="news-item"><span class="time-news">{{ $recent->time }}</span>{{ $recent->text }}
</li>
<?php endforeach ?>
@endsection

@section('main')
<div class="col-sm-8">


	<div class="grid-box">
	
		<div class="title-section">
			<h1><span class="world">{{ $total }} Hasil untuk Kata kunci: {{ $title }}</span></h1>
		</div>

		<ul class="list-posts row">
			<?php foreach($posts as $recent): ?>
			<li class="col-md-6">
				<?php $date = str_replace('-', '', $recent->post_date); ?>
				<a href="{{ site_url('read/news/'.$recent->post_id.'/'.$date.'/'.$recent->post_slug) }}"><img src="<?php echo image('/uploads/'.$recent->post_image, 'thumb_small'); ?>" alt="{{ $recent->image_title }}" /></a>
				<div class="post-content">
					<a href="{{ site_url('read/subtile/'.$recent->post_subtitle) }}">{{ $recent->subtitle }}</a>
					<h2><a href="{{ site_url('read/news/'.$recent->post_id.'/'.$date.'/'.$recent->post_slug) }}">{{ $recent->post_title }}</a></h2>
					<ul class="post-tags">
						<li><i class="fa fa-clock-o"></i>{{ longdate_indo($recent->post_date) }}</li>
					</ul>
				</div>
			</li>
			<?php endforeach ?>
		</ul>		

	</div>
	
	<div class="pagination-box">
		<ul class="pagination-list">
			{{ $page_links }}
		</ul>
	</div>

</div>
@endsection

@section('sidebar')
<div class="col-sm-4">

	<!-- sidebar -->
	<div class="sidebar">	

		<div class="advertisement">
			<div class="desktop-advert">
				{{ get_ads(10) }}
			</div>
			<div class="tablet-advert">
				<span>Advertisement</span>
				<img src="https://placehold.it/200x200" alt="">
			</div>
			<div class="mobile-advert">
				<span>Advertisement</span>
				<img src="https://placehold.it/300x250" alt="">
			</div>
		</div>

		<div class="widget tab-posts-widget">

			<ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<a href="#recent" data-toggle="tab">Terbaru</a>
				</li>
				<li>
					<a href="#popular" data-toggle="tab">Popular</a>
				</li>											
			</ul>

			<div class="tab-content">
				<div class="tab-pane active" id="recent">
					<ul class="list-posts">
						<?php foreach($recents as $recent): ?>
						<li>
							<?php $date = str_replace('-', '', $recent->post_date); ?>
							<img src="<?php echo image('/uploads/'.$recent->post_image, 'thumb_small'); ?>" alt="{{ $recent->image_title }}" />
							<div class="post-content">
								<a href="{{ site_url('read/subtile/'.$recent->post_subtitle) }}">{{ $recent->subtitle }}</a>
								<h2><a href="{{ site_url('read/news/'.$recent->post_id.'/'.$date.'/'.$recent->post_slug) }}">{{ $recent->post_title }}</a></h2>
								<ul class="post-tags">
									<li><i class="fa fa-clock-o"></i>{{ longdate_indo($recent->post_date) }}</li>
								</ul>
							</div>
						</li>
						<?php endforeach ?>
					</ul>
				</div>
				<div class="tab-pane" id="popular">
					<ul class="list-posts">

						<?php foreach($popular as $recent): ?>
						<li>
							<?php $date = str_replace('-', '', $recent->post_date); ?>
							<img src="<?php echo image('/uploads/'.$recent->post_image, 'thumb_small'); ?>" alt="{{ $recent->image_title }}" />
							<div class="post-content">
								<a href="{{ site_url('read/subtile/'.$recent->post_subtitle) }}">{{ $recent->subtitle }}</a>
								<h2><a href="{{ site_url('read/news/'.$recent->post_id.'/'.$date.'/'.$recent->post_slug) }}">{{ $recent->post_title }}</a></h2>
								<ul class="post-tags">
									<li><i class="fa fa-clock-o"></i>{{ longdate_indo($recent->post_date) }}</li>
								</ul>
							</div>
						</li>
						<?php endforeach ?>
						
					</ul>										
				</div>									
			</div>
		</div>
		

		<div class="widget tags-widget">

			<div class="title-section">
				<h1><span>Popular Tags</span></h1>
			</div>

			<ul class="tag-list">
				<?php foreach($trending_tags as $tag): ?>
				<li><a href="{{ site_url('read/tags/'.$tag->tag_id.'/'.$tag->tag_slug) }}">{{ $tag->tag_name }}</a></li>
				<?php endforeach ?>
			</ul>

		</div>

		<div class="advertisement">
			<div class="desktop-advert">
				{{ get_ads(7) }}
			</div>
			<div class="tablet-advert">
				<span>Advertisement</span>
				<img src="https://placehold.it/200x200" alt="">
			</div>
			<div class="mobile-advert">
				<span>Advertisement</span>
				<img src="https://placehold.it/300x250" alt="">
			</div>
		</div>

	</div>
	<!-- End sidebar -->

</div>
@endsection

@section('js')
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/jquery.min.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/jquery.migrate.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/jquery.bxslider.min.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/jquery.ticker.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/jquery.imagesloaded.min.js"></script>
  	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/jquery.isotope.min.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/script.js"></script>
@endsection