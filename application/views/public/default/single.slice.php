@extends('public.default.layouts.master_single')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/bootstrap.min.css" media="screen">	
	<link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/jquery.bxslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/font-awesome.css" media="screen">
	<link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/magnific-popup.css" media="screen">	
	<link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/owl.carousel.css" media="screen">
    <link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/owl.theme.css" media="screen">
	<link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/ticker-style.css"/>
	<link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/app.css" media="screen">
	@include('public.default.partials.style_single')
@endsection

@section('single-newsticker')
<?php foreach(news_ticker(4) as $recent): ?>
<li class="news-item"><span class="time-news">{{ $recent->time }}</span>{{ $recent->text }}
</li>
<?php endforeach ?>
@endsection

@section('main')
<div class="col-sm-8">

	<!-- block content -->
	<div class="block-content">

		<!-- single-post box -->
		<div class="single-post-box">

			<div class="title-post">
				<h1><small><a href="{{ site_url('read/sub/'.$post->post_subtitle) }}">{{ $post->subtitle }}</a></small></h1>
				<h1>{{ $post->post_title }}</h1>
				<ul class="post-tags">
					<li><i class="fa fa-clock-o"></i>{{ longdate_indo($post->post_date) }}</li>					
				</ul>
			</div>

			<div class="share-post-box">
				<ul class="share-box">					
					<li><a class="facebook" href="#"><i class="fa fa-facebook"></i><span>Share on Facebook</span></a></li>
					<li><a class="twitter" href="#"><i class="fa fa-twitter"></i><span>Share on Twitter</span></a></li>
					<li><a class="google" href="#"><i class="fa fa-google-plus"></i><span></span></a></li>
					<li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i><span></span></a></li>
				</ul>
			</div>

			<div class="post-gallery">
				<?php if($post->post_category == 24): ?>
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $post->youtube }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				</div>				
				<?php else: ?>
				<img src="<?php echo image('/uploads/'.$post->post_image, 'single'); ?>" alt="{{ $post->image_title }}" />
				<span class="image-caption">{{ $post->image_caption }}</span>
				<?php endif ?>
			</div>

			<div class="post-content">
			<?php if($post->post_category == 24): ?>
				<p>{{ $post->post_excerpt }}</p>
			<?php else: ?>			
				{{ $post->post_content }}					
			<?php endif ?>
			</div>	
			
			<?php if($post->post_category != 24): ?>
			<div class="post-gallery">
				<?php if($post->youtube): ?>
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $post->youtube }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				</div>
				<?php endif ?>
			</div>
			<?php endif ?>

			<div class="post-tags-box">
				<ul class="tags-box">
					<li><i class="fa fa-tags"></i><span>Tags:</span></li>
					<?php foreach($tags as $tag): ?>
					<li><a href="{{ site_url('read/tags/'.$tag->tag_id.'/'.$tag->tag_slug) }}">{{ $tag->tag_name }}</a></li>
					<?php endforeach ?>
				</ul>
			</div>

			<div class="share-post-box">
				<ul class="share-box">					
					<li><a class="facebook" href="#"><i class="fa fa-facebook"></i>Share on Facebook</a></li>
					<li><a class="twitter" href="#"><i class="fa fa-twitter"></i>Share on Twitter</a></li>
					<li><a class="google" href="#"><i class="fa fa-google-plus"></i><span></span></a></li>
					<li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i><span></span></a></li>
				</ul>
			</div>
			
			<hr>
			
			<div class="carousel-box owl-wrapper">							
			
				<div class="title-section">
					<h1><span>Baca Juga</span></h1>
				</div>
			
				<ul class="list-posts row">
					<?php foreach($similar as $recent): ?>
					<li class="col-md-6">
						<?php $date = str_replace('-', '', $recent->post_date); ?>
						<a href="{{ site_url('read/news/'.$recent->post_id.'/'.$date.'/'.$recent->post_slug) }}"><img src="<?php echo image('/uploads/'.$recent->post_image, 'thumb_small'); ?>" alt="{{ $recent->image_title }}" /></a>
						<div class="post-content">
							<a href="{{ site_url('read/subtile/'.$recent->post_subtitle) }}">{{ $recent->subtitle }}</a>
							<h2><a href="{{ site_url('read/news/'.$recent->post_id.'/'.$date.'/'.$recent->post_slug) }}">{{ $recent->post_title }}</a></h2>
							<ul class="post-tags">
								<li><i class="fa fa-clock-o"></i>{{ longdate_indo($recent->post_date) }}</li>
							</ul>
						</div>
					</li>
					<?php endforeach ?>
				</ul>
			
				
			</div>
			<!-- End carousel box -->
			
			
			

		</div>
		<!-- End single-post box -->
		
		<?php if($post->comment_status == 'open'): ?>
		<div class="single-post-box">
			{{ $disqus }}
		</div>
		<?php endif ?>

	</div>
	<!-- End block content -->

</div>
@endsection

@section('sidebar')
<div class="col-sm-4">

	<!-- sidebar -->
	<div class="sidebar">		

		<div class="widget tab-posts-widget">

			<ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<a href="#recent" data-toggle="tab">Terbaru</a>
				</li>
				<li>
					<a href="#popular" data-toggle="tab">Popular</a>
				</li>											
			</ul>

			<div class="tab-content">
				<div class="tab-pane active" id="recent">
					<ul class="list-posts">
						<?php foreach($recents as $recent): ?>
						<li>
							<?php $date = str_replace('-', '', $recent->post_date); ?>
							<img src="<?php echo image('/uploads/'.$recent->post_image, 'thumb_small'); ?>" alt="{{ $recent->image_title }}" />
							<div class="post-content">
								<a href="{{ site_url('read/subtile/'.$recent->post_subtitle) }}">{{ $recent->subtitle }}</a>
								<h2><a href="{{ site_url('read/news/'.$recent->post_id.'/'.$date.'/'.$recent->post_slug) }}">{{ $recent->post_title }}</a></h2>
								<ul class="post-tags">
									<li><i class="fa fa-clock-o"></i>{{ longdate_indo($recent->post_date) }}</li>
								</ul>
							</div>
						</li>
						<?php endforeach ?>
					</ul>
				</div>
				<div class="tab-pane" id="popular">
					<ul class="list-posts">

						<?php foreach($popular as $recent): ?>
						<li>
							<?php $date = str_replace('-', '', $recent->post_date); ?>
							<img src="<?php echo image('/uploads/'.$recent->post_image, 'thumb_small'); ?>" alt="{{ $recent->image_title }}" />
							<div class="post-content">
								<a href="{{ site_url('read/subtile/'.$recent->post_subtitle) }}">{{ $recent->subtitle }}</a>
								<h2><a href="{{ site_url('read/news/'.$recent->post_id.'/'.$date.'/'.$recent->post_slug) }}">{{ $recent->post_title }}</a></h2>
								<ul class="post-tags">
									<li><i class="fa fa-clock-o"></i>{{ longdate_indo($recent->post_date) }}</li>
								</ul>
							</div>
						</li>
						<?php endforeach ?>
						
					</ul>										
				</div>									
			</div>
		</div>
		

		<div class="widget tags-widget">

			<div class="title-section">
				<h1><span>Tag Terbaru</span></h1>
			</div>

			<ul class="tag-list">
				<?php foreach($trending_tags as $tag): ?>
				<li><a href="{{ site_url('read/tags/'.$tag->tag_id.'/'.$tag->tag_slug) }}">{{ $tag->tag_name }}</a></li>
				<?php endforeach ?>
			</ul>

		</div>

		<div class="advertisement">
			<div class="desktop-advert">
				{{ get_ads(7) }}
			</div>
			<div class="tablet-advert">
				<span>Advertisement</span>
				<img src="upload/addsense/200x200.jpg" alt="">
			</div>
			<div class="mobile-advert">
				<span>Advertisement</span>
				<img src="upload/addsense/300x250.jpg" alt="">
			</div>
		</div>

	</div>
	<!-- End sidebar -->

</div>
@endsection

@section('js')
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/jquery.min.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/jquery.migrate.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/jquery.bxslider.min.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/jquery.ticker.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/jquery.imagesloaded.min.js"></script>
  	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/jquery.isotope.min.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/script.js"></script>
@endsection