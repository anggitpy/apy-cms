<!-- footer 
	================================================== -->
<footer>
	<div class="container">
		<div class="footer-widgets-part">
			<div class="row">
				<div class="col-md-4">
					<div class="widget text-widget">
						{{ settings('about') }}
					</div>
					<div class="widget social-widget">
						<h1>Ikuti Poskotanews</h1>
						<ul class="social-icons">
							<?php if(settings('soc_facebook')): ?>
							<li><a href="{{ settings('soc_facebook') }}" class="facebook"><i class="fa fa-facebook"></i></a></li>
							<?php endif ?>
							
							<?php if(settings('soc_google_plus')): ?>
							<li><a href="{{ settings('soc_google_plus') }}" class="google"><i class="fa fa-google-plus"></i></a></li>
							<?php endif ?>
														
							<?php if(settings('soc_twitter')): ?>
							<li><a href="{{ settings('soc_twitter') }}" class="twitter"><i class="fa fa-twitter"></i></a></li>
							<?php endif ?>
							
							<?php if(settings('soc_youtube')): ?>
							<li><a href="{{ settings('soc_youtube') }}" class="youtube"><i class="fa fa-youtube"></i></a></li>
							<?php endif ?>
							
							<?php if(settings('soc_instagram')): ?>
							<li><a href="{{ settings('soc_instagram') }}" class="instagram"><i class="fa fa-instagram"></i></a></li>
							<?php endif ?>
							
							<?php if(settings('soc_linked_in')): ?>
							<li><a href="{{ settings('soc_linked_in') }}" class="linkedin"><i class="fa fa-linkedin"></i></a></li>							
							<?php endif ?>							
						</ul>
					</div>
				</div>
				<div class="col-md-4">
					<div class="widget posts-widget">
						<h1>Terpopuler</h1>
						<ul class="list-posts">
							<?php foreach(get_popular_posts(3, 100) as $recent): ?>
							<li>
								<?php $date = str_replace('-', '', $recent->post_date); ?>
								<img src="<?php echo image('/uploads/'.$recent->post_image, 'thumb_small'); ?>" alt="{{ $recent->image_title }}" />
								<div class="post-content">
									<a href="{{ site_url('read/subtile/'.$recent->post_subtitle) }}">{{ $recent->subtitle }}</a>
									<h2><a href="{{ site_url('read/news/'.$recent->post_id.'/'.$date.'/'.$recent->post_slug) }}">{{ $recent->post_title }}</a></h2>
									<ul class="post-tags">
										<li><i class="fa fa-clock-o"></i>{{ longdate_indo($recent->post_date) }}</li>
									</ul>
								</div>
							</li>
							<?php endforeach ?>
						</ul>
					</div>
				</div>
				<div class="col-md-4">
					<div class="widget categories-widget">
						<h1>Kategori Populer</h1>
						<ul class="category-list">
							<?php foreach(get_popular_category(12) as $recent): ?>
							<li>
								<a href="{{ site_url('read/kanal/'.$recent->cat_id.'/'.$recent->category_slug) }}">{{ $recent->cat_name }} <span>{{ $recent->count_cat }}</span></a>
							</li>
							<?php endforeach ?>							
						</ul>
					</div>
				</div>
				<!--
				<div class="col-md-3">
					<div class="widget flickr-widget">
						<h1>Instagram Photos</h1>
						<ul class="flickr-list">
							<li><a href="#"><img src="{{ base_url('themes/'.theme()) }}/upload/flickr/1.jpg" alt=""></a></li>
							<li><a href="#"><img src="{{ base_url('themes/'.theme()) }}/upload/flickr/2.jpg" alt=""></a></li>
							<li><a href="#"><img src="{{ base_url('themes/'.theme()) }}/upload/flickr/3.jpg" alt=""></a></li>
							<li><a href="#"><img src="{{ base_url('themes/'.theme()) }}/upload/flickr/4.jpg" alt=""></a></li>
							<li><a href="#"><img src="{{ base_url('themes/'.theme()) }}/upload/flickr/5.jpg" alt=""></a></li>
							<li><a href="#"><img src="{{ base_url('themes/'.theme()) }}/upload/flickr/6.jpg" alt=""></a></li>
						</ul>
						<a href="#">View more photos...</a>
					</div>
				</div>
				-->
			</div>
		</div>
		<div class="footer-last-line">
			<div class="row">
				<div class="col-md-6">
					<p>&copy; COPYRIGHT 2018 poskotanews.com</p>
				</div>
				<div class="col-md-6">
					<nav class="footer-nav">
						<ul>
							<li><a href="{{ site_url() }}">Home</a></li>
							<!--
							<li><a href="about.html">About</a></li>
							<li><a href="contact.html">Contact</a></li>
							-->
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- End footer -->