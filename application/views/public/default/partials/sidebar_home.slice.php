<!-- sidebar -->
<div class="sidebar">

	<div class="advertisement">
		<div class="desktop-advert">
			<span>Advertisement</span>
			{{ get_ads(9) }}
		</div>
		<div class="tablet-advert">
			<span>Advertisement</span>
			<img src="http://placehold.it/200x200" alt="">
		</div>
		<div class="mobile-advert">
			<span>Advertisement</span>
			<img src="http://placehold.it/300x250" alt="">
		</div>
	</div>

	@yield('tab_recent')
	
	<div class="advertisement">
		<div class="desktop-advert">
			<span>Advertisement</span>
			{{ get_ads(4) }}
		</div>
		<div class="tablet-advert">
			<span>Advertisement</span>
			<img src="http://placehold.it/200x200" alt="">
		</div>
		<div class="mobile-advert">
			<span>Advertisement</span>
			<img src="http://placehold.it/300x250" alt="">
		</div>
	</div>
	
	@yield('nah-ini-dia')

	

</div>
<!-- End sidebar -->