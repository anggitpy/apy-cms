<!-- heading-news-section2
	================================================== -->
<section class="heading-news2">

	<div class="container">
		
		@include('public.default.partials.news_ticker')
		
		<div class="iso-call heading-news-box">
			
			@yield('featured_news_slide')
			
			@yield('featured_news_small')		

		</div>
	</div>

</section>
<!-- End heading-news-section -->