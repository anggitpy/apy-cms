<!-- Header
	================================================== -->
<header class="clearfix second-style">
	<!-- Bootstrap navbar -->
	<nav class="navbar navbar-default navbar-static-top" role="navigation">

		<!-- Top line -->
		<div class="top-line">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<ul class="top-line-list">
							<li>
								<span class="city-weather">Jakarta, Indonesia</span>								
								<span class="cel-temperature">&mdash; <span id="c-temp"></span>&deg; Celcius</span>
							</li>
							<li><span class="time-now">{{ date('j F Y') }} / {{ date('H:i') }}</span></li>
							<!--
							<li><a href="contact.html">Contact</a></li>							
							-->
						</ul>
					</div>	
					<div class="col-md-3">
						<ul class="social-icons">
							<li><a class="rss" href="{{ site_url('feed') }}"><i class="fa fa-rss"></i></a></li>
							<?php if(settings('soc_facebook')): ?>
							<li><a href="{{ settings('soc_facebook') }}" class="facebook"><i class="fa fa-facebook"></i></a></li>
							<?php endif ?>
							
							<?php if(settings('soc_google_plus')): ?>
							<li><a href="{{ settings('soc_google_plus') }}" class="google"><i class="fa fa-google-plus"></i></a></li>
							<?php endif ?>
														
							<?php if(settings('soc_twitter')): ?>
							<li><a href="{{ settings('soc_twitter') }}" class="twitter"><i class="fa fa-twitter"></i></a></li>
							<?php endif ?>
							
							<?php if(settings('soc_youtube')): ?>
							<li><a href="{{ settings('soc_youtube') }}" class="youtube"><i class="fa fa-youtube"></i></a></li>
							<?php endif ?>
							
							<?php if(settings('soc_instagram')): ?>
							<li><a href="{{ settings('soc_instagram') }}" class="instagram"><i class="fa fa-instagram"></i></a></li>
							<?php endif ?>
							
							<?php if(settings('soc_linked_in')): ?>
							<li><a href="{{ settings('soc_linked_in') }}" class="linkedin"><i class="fa fa-linkedin"></i></a></li>							
							<?php endif ?>
						</ul>
					</div>	
				</div>
			</div>
		</div>
		<!-- End Top line -->
		
		

		<!-- Logo & advertisement -->
		<div class="logo-advertisement">
			<div class="container">

				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="{{ site_url() }}"><img src="{{ base_url('themes/'.theme()) }}/images/poskota.png" alt=""></a>
				</div>

				<div class="advertisement">
					<div class="desktop-advert">
						<span>Advertisement</span>
						<?php echo get_ads(1) ?>
					</div>
					<div class="tablet-advert">
						<span>Advertisement</span>
						<img src="https://placehold.it/468x60" alt="">
					</div>
				</div>
			</div>
		</div>
		<!-- End Logo & advertisement -->
		
		<!-- navbar list container -->
		<div class="nav-list-container">
			<div class="container">
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-left">					

						<li class="drop"><a class="home" href="{{ site_url() }}">Home</a></li>
						
						<?php foreach(get_categories() as $cat) :?>
						<li><a class="drop" href="{{ site_url('read/kanal/'.$cat->cat_id.'/'.$cat->category_slug) }}">{{ $cat->cat_name }}</a></li>
						<?php endforeach ?>
						
						<!--
						<li><a class="video" href="video.html">Nah ini Dia</a></li>
						<li><a class="sport" href="news-category5.html">Iklan Baris</a></li>
						<li><a class="food" href="news-category1.html">Karir Pad</a></li>
						<li class="drop has-dropdown"><a class="features" href="#">Features</a>
							<ul class="dropdown features-dropdown">
								<li><a href="#">Category Layouts</a></li>								
								<li><a href="allfooter.html">All footer widgets</a></li>
								<li><a href="autor-list.html">Autor List</a></li>
								<li><a href="autor-details.html">Autor Details</a></li>
								<li><a href="404-error.html">404 Error</a></li>
								<li><a href="underconstruction.html">Underconstruction</a></li>
								<li><a href="comming-soon.html">Comming soon Page</a></li>
							</ul>
						</li>
						-->

					</ul>
					<form class="navbar-form navbar-right" role="search" action="{{ site_url('read/search') }}">
						<input type="text" id="search" name="q" placeholder="Search here">
						<button type="submit" id="search-submit"><i class="fa fa-search"></i></button>
					</form>
				</div>
				<!-- /.navbar-collapse -->
			</div>
		</div>
		<!-- End navbar list container -->
		

	</nav>
	<!-- End Bootstrap navbar -->

</header>
<!-- End Header -->