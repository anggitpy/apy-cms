<style type="text/css">

.single-post-box > .post-content {
	margin-left: 50px;
}

.single-post-box .title-post h1 {
	font-size: 28px;
	color: #000;
	line-height: 1.4
}

.single-post-box > .post-content p {
    font-size: 16px;
    font-weight: 400;
    color: #000;
    margin: 0 0 10px;
	line-height: 1.8
}
</style>