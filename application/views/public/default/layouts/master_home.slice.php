<!doctype html>
<html lang="en" class="no-js">
<head>
	<title>{{ $title }}</title>

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900,400italic' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,900,400italic' rel='stylesheet' type='text/css'>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="icon" href="{{ base_url() }}assets/favicon.png">

	
	<!-- Fav icon here -->
		
	@yield('css')

</head>
<body class="boxed">

	<!-- Container -->
	<div id="container">

		@include('public.default.partials.header')
		
		@include('public.default.partials.heading_news')		

		<!-- block-wrapper-section
			================================================== -->
		<section class="block-wrapper">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">

						<!-- block content -->
						<div class="block-content">

							<!-- grid box -->
							<div class="grid-box">

								@yield('container1')
																						

							</div>
							<!-- End grid box -->

							@yield('large-rectangle-home')

							<!-- grid-box video -->
							<div class="grid-box">

								@yield('grid-box')

							</div>
							<!-- End grid-box video-->
							
							@yield('large-rectangle-home-bottom')

							<!-- carousel box -->
							<div class="carousel-box owl-wrapper">

								@yield('carousel-box')

							</div>
							<!-- End carousel box -->

							
							

						</div>
						<!-- End block content -->

					</div>

					<div class="col-sm-4">

						@include('public.default.partials.sidebar_home')

					</div>

				</div>

			</div>
		</section>
		<!-- End block-wrapper-section -->

		@include('public.default.partials.footer')

	</div>
	<!-- End Container -->
	
	@yield('js')
	
	@yield('init')
	
<script>
$.getJSON('https://api.openweathermap.org/data/2.5/weather?q=Jakarta,id&units=metric&appid=1ec75736cb1700fcff73498fffdb36c1', function(results) {		
	$('#c-temp').html(results.main.temp);	
});
</script>

</body>
</html>