<!doctype html>
<html lang="en" class="no-js">
<head>
	<title>{{ $title }}</title>

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900,400italic' rel='stylesheet' type='text/css'>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="icon" href="{{ base_url() }}assets/favicon.png">
	
	<!-- Fav icon here -->
	
	@yield('css')

</head>
<body class="boxed">

	<!-- Container -->
	<div id="container">

		@include('public.default.partials.header')
		
		<!-- ticker-news-section
			================================================== -->
		<section class="ticker-news">

			<div class="container">
				<div class="ticker-news-box">
					<span class="breaking-news">breaking news</span>					
					<ul id="js-news">
						@yield('single-newsticker')
						<!--
						<li class="news-item"><span class="time-news">11:36 pm</span>  <a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</a> Donec odio. Quisque volutpat mattis eros... </li>						
						-->
					</ul>
				</div>
			</div>

		</section>
		<!-- End ticker-news-section -->

		<section class="block-wrapper">
			<div class="container">
				<div class="row">
					@yield('main')
					@yield('sidebar')
				</div>
			</div>
		</section>

		@include('public.default.partials.footer')

	</div>
	<!-- End Container -->
	
	@yield('js')
	
	@yield('init')
	
<script>
$.getJSON('https://api.openweathermap.org/data/2.5/weather?q=Jakarta,id&units=metric&appid=1ec75736cb1700fcff73498fffdb36c1', function(results) {		
	$('#c-temp').html(results.main.temp);	
});
</script>

</body>
</html>