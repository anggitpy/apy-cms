@extends('public.default.layouts.master_home')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/bootstrap.min.css" media="screen">	
	<link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/jquery.bxslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/font-awesome.css" media="screen">
	<link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/magnific-popup.css" media="screen">	
	<link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/owl.carousel.css" media="screen">
    <link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/owl.theme.css" media="screen">
	<link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/ticker-style.css"/>
	<link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="{{ base_url('themes/'.theme()) }}/css/app.css" media="screen">
	@include('public.default.partials.style_home')
@endsection

@section('home-newsticker')
<?php foreach(news_ticker(4) as $recent): ?>
<li class="news-item"><span class="time-news">{{ $recent->time }}</span>{{ $recent->text }}
</li>
<?php endforeach ?>
@endsection

@section('featured_news_slide')
<div class="image-slider snd-size">
	<span class="top-stories">Headline</span>
	<ul class="bxslider">
		<?php foreach($featured_big as $fbig): ?>
		<li>
			<div class="news-post image-post">
				<img src="<?php echo image('uploads/'.$fbig->post_image, 'heading_big'); ?>" alt="{{ $fbig->image_title }}" />
				<div class="hover-box">
					<div class="inner-hover">
						<?php $date = str_replace('-', '', $fbig->post_date); ?>
						<a class="category-post sport" href="sport.html">{{ $fbig->cat_name }}</a>
						<?php if($fbig->subtitle): ?>
						<h2><small><a href="{{ site_url('read/subtile/'.$fbig->post_subtitle) }}">{{ $fbig->subtitle }}</a></small></h2>
						<?php endif ?>
						<h2><a href="{{ site_url('read/news/'.$fbig->post_id.'/'.$date.'/'.$fbig->post_slug) }}">{{ $fbig->post_title }}</a></h2>
						<ul class="post-tags">
							<li><i class="fa fa-clock-o"></i>{{ date_format(date_create($fbig->post_date),'l, j F Y') }}</li>														
						</ul>
					</div>
				</div>
			</div>
		</li>
		<?php endforeach ?>		
	</ul>
</div>
@endsection

@section('featured_news_small')
<?php foreach($featured_small as $fsmall): ?>
<div class="news-post image-post default-size">
	<img src="<?php echo image('uploads/'.$fsmall->post_image, 'heading_small'); ?>" alt="{{ $fsmall->image_title }}" />
	<div class="hover-box">
		<div class="inner-hover">
			<?php $date = str_replace('-', '', $fsmall->post_date); ?>
			<a class="category-post travel" href="travel.html">{{ $fsmall->cat_name }}</a>
			<?php if($fsmall->subtitle): ?>
			<h2><small><a href="{{ site_url('read/subtile/'.$fsmall->post_subtitle) }}">{{ $fsmall->subtitle }}</a></small></h2>
			<?php endif ?>
			<h2><a href="{{ site_url('read/news/'.$fsmall->post_id.'/'.$date.'/'.$fsmall->post_slug) }}">{{ $fsmall->post_title }}</a></h2>
			<ul class="post-tags">
				<li><i class="fa fa-clock-o"></i><span>{{ date_format(date_create($fsmall->post_date),'l, j F Y') }}</span></li>			
			</ul>			
		</div>
	</div>
</div>

<?php endforeach ?>
@endsection

@section('container1')
<div class="title-section">
	<h1><span>Kriminal</span></h1>	
</div>

<div class="row">
	<div class="col-md-6">
		<div class="news-post image-post2">
			<?php foreach($kriminal_head as $kriminal): ?>
			<div class="post-gallery">
				<img src="<?php echo image('uploads/'.$kriminal->post_image, 'hcategory'); ?>" alt="{{ $kriminal->image_title }}" />
				<div class="hover-box">
					<div class="inner-hover">
						<?php $date = str_replace('-', '', $kriminal->post_date); ?>
						<h2><small><a href="{{ site_url('read/subtile/'.$kriminal->post_subtitle) }}">{{ $kriminal->subtitle }}</a></small></h2>
						<h2><a href="{{ site_url('read/news/'.$kriminal->post_id.'/'.$date.'/'.$kriminal->post_slug) }}">{{ $kriminal->post_title }}</a></h2>
						<ul class="post-tags">
							<li><i class="fa fa-clock-o"></i>{{ longdate_indo($kriminal->post_date) }}</li>							
						</ul>
					</div>
				</div>
			</div>
			<?php endforeach ?>
		</div>
	</div>

	<div class="col-md-6">
		<ul class="list-posts">
			<?php foreach($kriminal_prev as $kprev): ?>
			<li>
				<img src="<?php echo image('uploads/'.$kprev->post_image, 'thumb_med'); ?>" alt="{{ $kprev->image_title }}" />
				<div class="post-content">
					<?php $date = str_replace('-', '', $kprev->post_date); ?>
					<a href="#">{{ $kprev->subtitle }}</a>
					<h2><a href="{{ site_url('read/news/'.$kprev->post_id.'/'.$date.'/'.$kprev->post_slug) }}">{{ $kprev->post_title }}</a></h2>
					<ul class="post-tags">
						<li><i class="fa fa-clock-o"></i>{{ longdate_indo($kprev->post_date) }}</li>
					</ul>
				</div>
			</li>
			<?php endforeach ?>
		</ul>
	</div>
</div>

<div class="center-button">
	<a href="#"><i class="fa fa-angle-right"></i> Kriminal</a>
</div>

<div class="title-section">
	<h1><span>Megapolitan</span></h1>
</div>

<div class="row">
	<div class="col-md-6">
		<div class="news-post image-post2">
			<?php foreach($mega_head as $m): ?>
			<div class="post-gallery">
				<img src="<?php echo image('uploads/'.$m->post_image, 'hcategory'); ?>" alt="{{ $m->image_title }}" />
				<div class="hover-box">
					<div class="inner-hover">
						<?php $date = str_replace('-', '', $m->post_date); ?>
						<h2><small><a href="{{ site_url('read/subtile/'.$m->post_subtitle) }}">{{ $m->subtitle }}</a></small></h2>
						<h2><a href="{{ site_url('read/news/'.$m->post_id.'/'.$date.'/'.$m->post_slug) }}">{{ $m->post_title }}</a></h2>
						<ul class="post-tags">
							<li><i class="fa fa-clock-o"></i>{{ longdate_indo($m->post_date) }}</li>							
						</ul>
					</div>
				</div>
			</div>
			<?php endforeach ?>
		</div>
	</div>

	<div class="col-md-6">
		<ul class="list-posts">
			<?php foreach($mega_prev as $m): ?>
			<li>
				<img src="<?php echo image('uploads/'.$m->post_image, 'thumb_med'); ?>" alt="{{ $m->image_title }}" />
				<div class="post-content">
					<?php $date = str_replace('-', '', $m->post_date); ?>
					<a href="#">{{ $m->subtitle }}</a>
					<h2><a href="{{ site_url('read/news/'.$m->post_id.'/'.$date.'/'.$m->post_slug) }}">{{ $m->post_title }}</a></h2>
					<ul class="post-tags">
						<li><i class="fa fa-clock-o"></i>{{ longdate_indo($m->post_date) }}</li>
					</ul>
				</div>
			</li>
			<?php endforeach ?>
		</ul>
	</div>
</div>

<div class="center-button">
	<a href="#"><i class="fa fa-angle-right"></i> Megapolitan</a>
</div>

@endsection

@section('grid-box')
<div class="title-section">
	<h1><span class="world">Poskota TV</span></h1>
</div>

<div class="row">
	<?php foreach($videos as $vid): ?>

	<div class="col-md-6">
		<div class="news-post video-post">
			<?php $date = str_replace('-', '', $vid->post_date); ?>
			<img src="<?php echo image('uploads/'.$vid->post_image, 'thumb_big'); ?>" alt="{{ $vid->image_title }}" />
			<a href="https://www.youtube.com/watch?v={{ $vid->youtube }}" class="video-link"><i class="fa fa-play-circle-o"></i></a>
			<div class="hover-box">
				<h2><a href="{{ site_url('read/news/'.$vid->post_id.'/'.$date.'/'.$vid->post_slug) }}">{{ $vid->post_title }}</a></h2>
				<ul class="post-tags">
					<li><i class="fa fa-clock-o"></i>{{ longdate_indo($vid->post_date) }}</li>
				</ul>
			</div>
		</div>
	</div>

	<?php endforeach ?>
</div>
@endsection

@section('carousel-box')
<div class="title-section">
	<h1 class="really-title"><span class="tech">Berita Lainnya</span></h1>
</div>

<div class="owl-carousel" data-num="2">

	<!-- Item -->
	<div class="item">
		<div class="title-section">
			<h1><span class="world">Entertainment</span></h1>
		</div>
		
		<?php foreach($entertainment as $e): ?>
		<div class="news-post image-post2">
			<div class="post-gallery">
				<img src="<?php echo image('uploads/'.$e->post_image, 'hcategory'); ?>" alt="{{ $e->image_title }}" />
				<div class="hover-box">
					<div class="inner-hover">
						<?php $date = str_replace('-', '', $e->post_date); ?>
						<h2><a href="{{ site_url('read/news/'.$e->post_id.'/'.$date.'/'.$e->post_slug) }}">{{ $e->post_title }}</a></h2>
						<ul class="post-tags">
							<li><i class="fa fa-clock-o"></i>{{ longdate_indo($e->post_date) }}</li>							
						</ul>
					</div>
				</div>
			</div>
		</div>
		<?php endforeach ?>

		<ul class="list-posts">
			<?php foreach($entertainment_prev as $e): ?>
			<li>
				<img src="<?php echo image('uploads/'.$e->post_image, 'thumb_med'); ?>" alt="{{ $e->image_title }}" />
				<div class="post-content">
					<?php $date = str_replace('-', '', $e->post_date); ?>
					<h2><a href="{{ site_url('read/news/'.$e->post_id.'/'.$date.'/'.$e->post_slug) }}">{{ $e->post_title }}</a></h2>
					<ul class="post-tags">
						<li><i class="fa fa-clock-o"></i>{{ longdate_indo($e->post_date) }}</li>							
					</ul>
				</div>
			</li>
			<?php endforeach ?>			
		</ul>									
	</div>
	<!-- /item -->

	<!-- Item -->
	<div class="item">
		<div class="title-section">
			<h1><span class="sports">Sports</span></h1>
		</div>
		<?php foreach($sports as $s): ?>
		<div class="news-post image-post2">
			<div class="post-gallery">
				<img src="<?php echo image('uploads/'.$s->post_image, 'hcategory'); ?>" alt="{{ $s->image_title }}" />
				<div class="hover-box">
					<div class="inner-hover">
						<?php $date = str_replace('-', '', $s->post_date); ?>
						<h2><a href="{{ site_url('read/news/'.$s->post_id.'/'.$date.'/'.$s->post_slug) }}">{{ $s->post_title }}</a></h2>
						<ul class="post-tags">
							<li><i class="fa fa-clock-o"></i>{{ longdate_indo($s->post_date) }}</li>							
						</ul>
					</div>
				</div>
			</div>
		</div>
		<?php endforeach ?>

		<ul class="list-posts">
			<?php foreach($sports_prev as $s): ?>
			<li>
				<img src="<?php echo image('uploads/'.$s->post_image, 'thumb_med'); ?>" alt="{{ $s->image_title }}" />
				<div class="post-content">
					<?php $date = str_replace('-', '', $s->post_date); ?>
					<h2><a href="{{ site_url('read/news/'.$s->post_id.'/'.$date.'/'.$s->post_slug) }}">{{ $s->post_title }}</a></h2>
					<ul class="post-tags">
						<li><i class="fa fa-clock-o"></i>{{ longdate_indo($s->post_date) }}</li>							
					</ul>
				</div>
			</li>
			<?php endforeach ?>			
		</ul>							
	</div>
	<!--/item-->

	<!-- Item -->
	<div class="item">
		<div class="title-section">
			<h1><span class="fashion">Gaya Hidup</span></h1>
		</div>
		<?php foreach($gaya as $s): ?>
		<div class="news-post image-post2">
			<div class="post-gallery">
				<img src="<?php echo image('uploads/'.$s->post_image, 'hcategory'); ?>" alt="{{ $s->image_title }}" />
				<div class="hover-box">
					<div class="inner-hover">
						<?php $date = str_replace('-', '', $s->post_date); ?>
						<h2><a href="{{ site_url('read/news/'.$s->post_id.'/'.$date.'/'.$s->post_slug) }}">{{ $s->post_title }}</a></h2>
						<ul class="post-tags">
							<li><i class="fa fa-clock-o"></i>{{ longdate_indo($s->post_date) }}</li>							
						</ul>
					</div>
				</div>
			</div>
		</div>
		<?php endforeach ?>

		<ul class="list-posts">
			<?php foreach($gaya_prev as $s): ?>
			<li>
				<img src="<?php echo image('uploads/'.$s->post_image, 'thumb_med'); ?>" alt="{{ $s->image_title }}" />
				<div class="post-content">
					<?php $date = str_replace('-', '', $s->post_date); ?>
					<h2><a href="{{ site_url('read/news/'.$s->post_id.'/'.$date.'/'.$s->post_slug) }}">{{ $s->post_title }}</a></h2>
					<ul class="post-tags">
						<li><i class="fa fa-clock-o"></i>{{ longdate_indo($s->post_date) }}</li>							
					</ul>
				</div>
			</li>
			<?php endforeach ?>			
		</ul>						
	</div>
	<!--/item-->

</div>
@endsection

@section('tab_recent')

<div class="widget tab-posts-widget">

	<ul class="nav nav-tabs" id="myTab">
		<li class="active">
			<a href="#recent" data-toggle="tab">Terbaru</a>
		</li>
		<li>
			<a href="#popular" data-toggle="tab">Popular</a>
		</li>											
	</ul>

	<div class="tab-content">
		<div class="tab-pane active" id="recent">
			<ul class="list-posts">
				<?php foreach($recents as $recent): ?>
				<li>
					<?php $date = str_replace('-', '', $recent->post_date); ?>
					<img src="<?php echo image('uploads/'.$recent->post_image, 'thumb_small'); ?>" alt="{{ $recent->image_title }}" />
					<div class="post-content">
						<a href="{{ site_url('read/subtile/'.$recent->post_subtitle) }}">{{ $recent->subtitle }}</a>
						<h2><a href="{{ site_url('read/news/'.$recent->post_id.'/'.$date.'/'.$recent->post_slug) }}">{{ $recent->post_title }}</a></h2>
						<ul class="post-tags">
							<li><i class="fa fa-clock-o"></i>{{ longdate_indo($recent->post_date) }}</li>
						</ul>
					</div>
				</li>
				<?php endforeach ?>
			</ul>
		</div>
		<div class="tab-pane" id="popular">
			<ul class="list-posts">

				<?php foreach($popular as $recent): ?>
				<li>
					<?php $date = str_replace('-', '', $recent->post_date); ?>
					<img src="<?php echo image('uploads/'.$recent->post_image, 'thumb_small'); ?>" alt="{{ $recent->image_title }}" />
					<div class="post-content">
						<a href="{{ site_url('read/subtile/'.$recent->post_subtitle) }}">{{ $recent->subtitle }}</a>
						<h2><a href="{{ site_url('read/news/'.$recent->post_id.'/'.$date.'/'.$recent->post_slug) }}">{{ $recent->post_title }}</a></h2>
						<ul class="post-tags">
							<li><i class="fa fa-clock-o"></i>{{ longdate_indo($recent->post_date) }}</li>
						</ul>
					</div>
				</li>
				<?php endforeach ?>
				
			</ul>										
		</div>									
	</div>
</div>
@endsection

@section('nah-ini-dia')
<div class="widget features-slide-widget">
	<div class="title-section">
		<h1><span>Nah Ini Dia</span></h1>
	</div>
	<div class="image-post-slider">
		<ul class="bxslider">
			<?php foreach($nah as $n): ?>
			<li>
				<div class="news-post image-post2">
					<div class="post-gallery">
						<?php $date = str_replace('-', '', $recent->post_date); ?>
						<img src="<?php echo image('uploads/'.$n->post_image, 'hcategory'); ?>" alt="{{ $n->image_title }}" />
						<div class="hover-box">
							<div class="inner-hover nah-ini-dia">
								<h2><a href="{{ site_url('read/news/'.$n->post_id.'/'.$date.'/'.$n->post_slug) }}">{{ $n->post_title }}</a></h2>
								<ul class="post-tags">
									<li><i class="fa fa-clock-o"></i>{{ longdate_indo($n->post_date) }}</li>									
								</ul>
							</div>
						</div>
					</div>
				</div>
			</li>
			<?php endforeach ?>
		</ul>
	</div>
</div>
@endsection

@section('large-rectangle-home')

<div class="advertisement">
	<div class="desktop-advert">		
		{{ get_ads(2) }}
	</div>
	<div class="tablet-advert">
		<img src="http://placehold.it/468x60" alt="">
	</div>
	<div class="mobile-advert">
		<img src="http://placehold.it/300x250" alt="">
	</div>
</div>

@endsection

@section('large-rectangle-home-bottom')

<div class="advertisement">
	<div class="desktop-advert">		
		{{ get_ads(3) }}
	</div>
	<div class="tablet-advert">
		<img src="http://placehold.it/468x60" alt="">
	</div>
	<div class="mobile-advert">
		<img src="http://placehold.it/300x250" alt="">
	</div>
</div>

@endsection

@section('js')
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/jquery.min.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/jquery.migrate.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/jquery.bxslider.min.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/jquery.ticker.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/jquery.imagesloaded.min.js"></script>
  	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/jquery.isotope.min.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="{{ base_url('themes/'.theme()) }}/js/script.js"></script>
@endsection