@extends('admin.layouts.master')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css">
@include('admin.partials.vuetable')
@endsection

@section('js')
<script type="text/javascript" src="{{ base_url() }}assets/js/vue/vue.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/vue/axios.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/vue/vuetable-2.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/validator/validator.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.js"></script>
@endsection

@section('page_title')
	Pengaturan Tags
@endsection

@section('page_subtitle')
	{{ $title }}
@endsection

@section('breadcrumb')
<ul class="breadcrumb">
	<li><a href="{{ site_url() }}"><i class="icon-home2 position-left"></i> Home</a></li>
</ul>
@endsection

@section('breadcrumb_elements')
<ul class="breadcrumb-elements">
	<li><a href="{{ site_url('') }}"><i class="icon-file-plus position-left"></i> Additional</a></li>	
</ul>
@endsection

@section('main_content')	
<!-- Simple panel -->

<div class="row">
	<div class="col-md-7">
		<div class="panel panel-flat" id="app">
			<div class="panel-heading">
				<h5 class="panel-title">Tags</h5>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>				
					</ul>
				</div>
			</div>

			<div class="panel-body">
				<div class="table-responsive">		

					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon bg-primary"><i class="icon-search4"></i></span>
							<input type="text" class="form-control" v-model="searchFor" @keyup="setFilter" placeholder="Telusuri Tag">
							<span class="input-group-addon"><i @click="resetFilter" class="icon-inbox"></i></span>
						</div>
					</div>
							
					<div :class="[{'data-table': true}, loading]">					
						<vuetable ref="vuetable"
							api-url="<?php echo site_url('tags/tags_data/') ?>"
							:fields="columns"
							pagination-path=""
							:sort-order="sortOrder"
							:per-page="perPage"
							:append-params="moreParams"
							@vuetable:cell-clicked="onCellClicked"									
							:css="css.table"
							track-by="tag_id"
							@vuetable:pagination-data="onPaginationData"
							@vuetable:loading="showLoader"
							@vuetable:loaded="hideLoader">

							<template slot="actions" scope="props">
								<div class="btn-group">
									<button class="btn bg-primary btn-xs" @click="editRow(props.rowData)">Edit</button>												
								</div>
							</template>
							
						</vuetable>					
					</div>
					
					
					<div class="data-table-pagination">
						<vuetable-pagination-info ref="paginationInfo"
							:info-template="paginationInfoTemplate">
						</vuetable-pagination-info>
						<vuetable-pagination ref="pagination"
							@vuetable-pagination:change-page="onChangePage"
							:css="css.pagination">
						</vuetable-pagination>			
					</div>
								
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-md-5">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">Tambah</h5>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>				
					</ul>
				</div>
			</div>

			<div class="panel-body">
				<?php echo form_open('tags/save','id="tagSave" data-toggle="validator"') ?>	
				<div class="form-group no-margin-bottom">
					<label>Tag</label>
					<input type="text" name="tag_name" class="form-control" placeholder="Tag" required>	
					<span class="help-block with-errors"></span>
				</div>
				<div class="form-group">
					<label>Description</label>
					<input type="text" name="tag_description" class="form-control" placeholder="Tag Description">	
					<span class="help-block with-errors"></span>
				</div>
				<div class="form-group">
					<label>Trending</label>
					<?php echo form_dropdown('trending', $trending, set_value('trending'),'class="form-control" id="trending" required'); ?>
					<span class="help-block with-errors"></span>
				</div>
				<div class="form-group no-margin-bottom">
					<button type="submit" class="btn btn-danger btn-block btn-lg">Save Tag</button>
				</div>
				<?php echo form_close() ?>	
			</div>
		</div>
	</div>
	
</div>					

<!-- /simple panel -->	
@endsection				

@section('init')
<script>
Vue.use(Vuetable);
var vm = new Vue({
	el: '#app',	
	data: {
		loading: '',
		searchFor: '',
		columns: [
			{
				name: 'tag_id',
				title: '#',
				sortField: 'tag_id',
			},				
			{
				name: 'tag_name',
				title: 'Tag',
				sortField: 'tag_name'
			},						
			{
				name: 'trending',
				title: 'Trending?',
				sortField: 'trending'
			},
			{
				name: 'tag_description',
				title: 'Description',
				sortField: 'tag_description'
			},
			{
				name: '__slot:actions',
				title: 'Actions',
			}			
		],		
		moreParams: [],
		sortOrder: [{
			field: 'tag_id',
			direction: 'desc'
		}],				
		css: {
			table: {
				tableClass: 'table table-xxs',
				ascendingIcon: 'icon-arrow-up22',
				descendingIcon: 'icon-arrow-down22',	
			},		
			pagination: {
				wrapperClass: "btn-group",
				activeClass: "active",
				disabledClass: "disabled",
				pageClass: "btn btn-default",
				linkClass: "btn btn-default",
				icons: {
					first: "icon-chevron-left",
					prev: "icon-arrow-left32",
					next: "icon-arrow-right32",
					last: "icon-chevron-right"
				}
			}
		},
		//paginationComponent: 'vuetable-pagination',
		perPage: 10,
		paginationInfoTemplate: '<strong>Showing record</strong> {from} to {to} from {total} item(s)',
		
	},	
	methods: {			
		setFilter () {
			this.moreParams = {
				'filter': this.searchFor
			}
			this.$nextTick(function() {
				this.$refs.vuetable.refresh()
			})
		},
		
		formatDate(value, fmt) {
			if (value == null) return ''
			fmt = (typeof fmt == 'undefined') ? 'D MMM YYYY' : fmt
			return moment(value, 'YYYY-MM-DD').format(fmt)
		},
		formatNumber(value, fmt) {
			if (value == null) return ''
			return $.number(value, fmt)
		},		
				
		resetFilter () {
			this.searchFor = ''
			this.setFilter()
		},
		showLoader () {
			this.loading = 'loading'
		},
		hideLoader () {
			this.loading = ''
		},
		
		editRow(rowData) {
			$(location).attr('href', '{{ site_url('tags/edit/') }}' + rowData.tag_id)				
		},	
		
		onPaginationData (tablePagination) {
			this.$refs.paginationInfo.setPaginationData(tablePagination)
			this.$refs.pagination.setPaginationData(tablePagination)
		},
		onChangePage (page) {
			this.$refs.vuetable.changePage(page)
		},		
		onInitialized (fields) {
			console.log('onInitialized', fields)
			this.vuetableFields = fields
		},
		
		onCellClicked (data, field, event) {
			console.log('cellClicked: ', field.name)
			this.$refs.vuetable.toggleDetailRow(data.image_id)
		},
		
		onDataReset () {
			console.log('onDataReset')
			this.$refs.paginationInfo.resetData()
			this.$refs.pagination.resetData()
		},
		
	},
})

$('#tagSave').validator().on('submit', function(e) {
	if (e.isDefaultPrevented()) {
		console.log('Not Submit');
	} else {
		e.preventDefault();					
		var datastring = $("#tagSave").serializeArray();
		
		$.ajax({
			type: "POST",
			url: "{{ site_url('tags/save_tag') }}",
			data: datastring,
			dataType: "json",
			success: function(data) {
				swal({
					title: "Success",   
					text: "Tag "+data.response.tag_name+" successfully Added", 
					type: "success",
					onClose: function(){
						$(location).attr('href', '{{ site_url('tags') }}')
					}
				})										
			},
			error: function(jqXHR, textStatus, errorThrown){
				console.log(errorThrown)
			}
		});	
					
	}
})

</script>
@endsection