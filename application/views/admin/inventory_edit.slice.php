@extends('admin.layouts.master')

@section('js')
<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/loaders/blockui.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/pickers/pickadate/picker.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/pickers/pickadate/picker.date.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/validator/validator.min.js"></script>
@endsection

@section('page_title')
	Tambahkan Inventory
@endsection

@section('page_subtitle')
	{{ $title }}
@endsection

@section('breadcrumb')
<ul class="breadcrumb">
	<li><a href="{{ site_url() }}"><i class="icon-home2 position-left"></i> Home</a></li>
</ul>
@endsection

@section('breadcrumb_elements')
<ul class="breadcrumb-elements">
	<li><a href="{{ site_url('') }}"><i class="icon-file-plus position-left"></i> Additional</a></li>	
</ul>
@endsection

@section('main_content')	
<div class="panel panel-flat">
	<div class="panel-heading">
	
		<?php if($this->session->flashdata('pk_message')): ?>
			<div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
				<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
				{{ $this->session->flashdata('pk_message') }}
			</div>
		<?php endif ?>
	
		<h5 class="panel-title">Edit Inventory Iklan</h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>				
			</ul>
		</div>
	</div>

	<div class="panel-body">
		<?php echo form_open('admanagement/update_inventory/'.$post->ads_id,'id="inventorySave" data-toggle="validator"') ?>	
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label>Keterangan</label>
					<input type="text" name="ads_description" class="form-control" placeholder="Contoh: Top Banner Telkomsel, Homepage, Posisi Atas" required value="{{ $post->ads_description }}">	
					<span class="help-block with-errors"></span>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>Klien</label>
					<?php echo form_dropdown('ads_client', $client, $post->ads_client,'class="form-control" id="ads_client" required'); ?>
					<span class="help-block with-errors"></span>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label>Nama Iklan</label>
			<input type="text" name="ads_name" class="form-control" placeholder="Contoh: Top Banner" required value="{{ $post->ads_name }}">	
			<span class="help-block with-errors"></span>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label>Tanggal Mulai Iklan</label>
					<input type="text" name="ads_start_date" class="form-control" id="date_begin" placeholder="Tanggal Mulai Iklan" required value="{{ $post->ads_start_date }}">	
					<span class="help-block with-errors"></span>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>Tanggal Berakhir Iklan</label>
					<input type="text" name="ads_end_date" class="form-control" id="date_end" placeholder="Tanggal Berakhir Iklan" required value="{{ $post->ads_end_date }}">	
					<span class="help-block with-errors"></span>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<label>Kode Iklan</label>
			<textarea name="ads_code" class="form-control" placeholder="Kode Iklan" required>{{ $post->ads_code }}</textarea>
			<span class="help-block with-errors"></span>
			<div class="text-center">{{ $post->ads_code }}</div>
		</div>
		
		<div class="form-group">	
			<label>Kode Iklan Default</label>
			<textarea name="ads_default" class="form-control" placeholder="Kode Iklan Default digunakan jika iklan berakhir" required>{{ $post->ads_default }}</textarea>
			<span class="help-block with-errors"></span>				
		</div>
		
		
		<div class="form-group no-margin-bottom">
			<button type="submit" id="block-page" class="btn btn-danger btn-block btn-lg">Edit Inventory</button>
		</div>
		<?php echo form_close() ?>	
	</div>
</div>
@endsection				

@section('init')
<script>
$('#date_begin').pickadate({
	format: 'yyyy-mm-dd',
});
$('#date_end').pickadate({
	format: 'yyyy-mm-dd',
});
$(function() {

	$('#inventorySave').validator().on('submit', function(e) {
		if (e.isDefaultPrevented()) {
			console.log('Not Submit');
		} else {		
			$.blockUI({ 
				message: '<i class="icon-spinner4 spinner"></i>',
				timeout: 0, //unblock after 2 seconds
				overlayCSS: {
					backgroundColor: '#1b2024',
					opacity: 0.8,
					zIndex: 1200,
					cursor: 'wait'
				},
				css: {
					border: 0,
					color: '#fff',
					padding: 0,
					zIndex: 1201,
					backgroundColor: 'transparent'
				}
			});		
		}
	})	
	
})

</script>
@endsection