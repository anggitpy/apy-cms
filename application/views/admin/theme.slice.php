@extends('admin.layouts.master')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css">
<link rel="stylesheet" href="{{ base_url() }}assets/css/icons/fontawesome/styles.min.css">
@include('admin.partials.vuetable')
@endsection

@section('js')
<script type="text/javascript" src="{{ site_url() }}assets/js/plugins/media/fancybox.min.js"></script>
<script type="text/javascript" src="{{ site_url() }}assets/js/pages/gallery.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/loaders/blockui.min.js"></script>
@endsection

@section('page_title')
	Themes
@endsection

@section('page_subtitle')
	Select Themes
@endsection

@section('breadcrumb')
<ul class="breadcrumb">
	<li><a href="{{ site_url('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
</ul>
@endsection

@section('main_content')	
<div class="row">

    <?php if($this->session->flashdata('pk_msg')): ?>
    <div class="alert alert-info">
        <?php echo $this->session->flashdata('pk_msg'); ?>
    </div>
    <?php endif ?>

    <?php foreach($maps as $map): ?>
    <div class="col-lg-3 col-sm-6">
        <div class="thumbnail">
            <div class="thumb">
                <img src="{{base_url('themes/'.$map)}}/screenshot.jpg" alt="">
                <div class="caption-overflow">
                    <span>
                        <a href="{{base_url('themes/'.$map)}}screenshot.jpg" data-popup="lightbox" rel="gallery" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus3"></i></a>
                        <a href="{{ site_url('theme/set_theme/'.$map) }}" id="block-page-{{stripslashes($map)}}" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-pushpin"></i></a>
                    </span>
                </div>
            </div>
            <div class="caption">
                <h6 class="no-margin">
                    <a href="#" class="text-default">{{ stripslashes($map) }}</a>
                    <?php if(theme() == stripslashes($map)): ?>
                    <a href="#" class="text-muted"><i class="icon-folder-check pull-right"></i></a>
                    <?php endif ?>
                </h6>
            </div>
        </div>
    </div>
    <?php endforeach ?>
   
</div>
@endsection		

@section('init')
<script>
$(function() {
    <?php foreach($maps as $map): ?>
	$('#block-page-{{stripslashes($map)}}').on('click', function() {
        $.blockUI({ 
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 0, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent'
            }
        });
    });
    <?php endforeach ?>
})
</script>
@endsection