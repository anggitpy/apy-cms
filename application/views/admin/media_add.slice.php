@extends('admin.layouts.master')

@section('css')
<link rel="stylesheet" href="{{ base_url() }}assets/js/sweetalert/sweetalert2.min.css">
@endsection

@section('js')
<script type="text/javascript" src="{{ base_url() }}assets/js/validator/validator.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/uploaders/fileinput/fileinput.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/pages/uploader_bootstrap.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/sweetalert/sweetalert2.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/loaders/blockui.min.js"></script>

@endsection

@section('page_title')
	Blank Panel
@endsection

@section('page_subtitle')
	{{ $title }}
@endsection

@section('breadcrumb')
<ul class="breadcrumb">
	<li><a href="{{ site_url() }}"><i class="icon-home2 position-left"></i> Home</a></li>
</ul>
@endsection

@section('breadcrumb_elements')
<ul class="breadcrumb-elements">
	<li><a href="#"><i class="icon-file-plus position-left"></i> Additional</a></li>	
</ul>
@endsection

@section('main_content')	
<div class="row">
	<div class="col-md-6">
		<div class="panel panel-flat">
			<div class="panel-heading">
			
				<?php if($this->session->flashdata('upload_message')): ?>
					{{ $this->session->flashdata('upload_message') }}
				<?php endif ?>
			
				<h5 class="panel-title">Add Image</h5>
			
			</div>

			<div class="panel-body">
				<?php echo form_open_multipart('media/save','id="mediaSave" data-toggle="validator"') ?>
				<div class="form-group no-margin-bottom">
					<label>Judul</label>
					<input type="text" name="image_title" class="form-control" placeholder="Title" required>	
					<span class="help-block with-errors"></span>
				</div>
				<div class="form-group">
					<label>Caption</label>
					<textarea name="image_caption" class="form-control" placeholder="Caption"></textarea>
					<span class="help-block with-errors"></span>
				</div>
				<div class="form-group">
					<input type="file" name="post_image" id="post_image" class="file-input btn-block" data-show-remove="false" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-block" data-max-file-size="1280">
					<!-- get value from library -->
					<input type="hidden" name="image_id" id="image_id" value="">
					<input type="hidden" name="image_file_name" id="image_file_name" value="">
				</div>	
				
				<div class="form-group no-margin-bottom">
					<button type="submit" class="btn btn-danger btn-block btn-lg">Save Image</button>
				</div>
				<?php echo form_close() ?>	
			</div>
		</div>	
	</div>
</div>
		
@endsection				

@section('init')
<script>
	$('#mediaSave').validator().on('submit', function(e) {
		if (e.isDefaultPrevented()) {
			console.log('Not Submit');
		} else {		
			$.blockUI({ 
				message: '<i class="icon-spinner4 spinner"></i>',
				timeout: 0, //unblock after 2 seconds
				overlayCSS: {
					backgroundColor: '#1b2024',
					opacity: 0.8,
					zIndex: 1200,
					cursor: 'wait'
				},
				css: {
					border: 0,
					color: '#fff',
					padding: 0,
					zIndex: 1201,
					backgroundColor: 'transparent'
				}
			});			
		}
	})
</script>
@endsection