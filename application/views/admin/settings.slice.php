@extends('admin.layouts.master')

@section('js')
<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/loaders/blockui.min.js"></script>
@endsection

@section('page_title')
	Settings
@endsection

@section('page_subtitle')
	Setting Page
@endsection

@section('main_content')
<!-- Simple panel -->					
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Customize Application</h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
			</ul>
		</div>
	</div>

	<div class="panel-body">
	
		<?php if($this->session->flashdata('pk_msg')): ?>
		<div class="alert alert-info">
			<?php echo $this->session->flashdata('pk_msg'); ?>
		</div>
		<?php endif ?>
		
        <?php $attributes = array('class' => 'form-horizontal',); ?>
        <?php echo form_open('settings/update', $attributes); ?>
			<div class="form-body">
			
				<?php foreach($posts as $post): ?>
				<div class="form-group">
					<label class="control-label col-md-3 text-right"><?php echo $post->setting_name_alias ?></label>
                    
					<div class="col-md-<?php echo $post->field_width ?>">
						<?php if($post->field_type == 'textarea'): ?>
							<textarea name="<?php echo $post->setting_name ?>" class="form-control" rows="3"><?php echo $post->setting_value ?></textarea>
                        <?php elseif($post->field_type == 'select'): ?>
                            <?php echo form_dropdown($post->setting_name, $cache_type, $post->setting_value,'class="form-control" required'); ?>
						<?php else: ?>
							<input type="<?php echo $post->field_type ?>" name="<?php echo $post->setting_name ?>" placeholder="<?php echo $post->setting_name_alias ?>" class="form-control" value="<?php echo $post->setting_value ?>">
						<?php endif ?>
						<span class="help-block"><?php echo $post->setting_name_description ?></span>
					</div>								
                    
				</div>
				<?php endforeach ?>
				
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" id="update-setting" class="btn btn-primary btn-lg"> Save</button>
					</div>
				</div>
			</div>
		<?php echo form_close() ?>	
		
	</div>
</div>
<!-- /simple panel -->
@endsection

@section('init')
<script>
$(function() {
	$('#update-setting').on('click', function() {
        $.blockUI({ 
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 0, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent'
            }
        });
    });
})
</script>
@endsection