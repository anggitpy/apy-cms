@extends('admin.layouts.master')

@section('css')
<link rel="stylesheet" href="{{ base_url() }}assets/css/icons/fontawesome/styles.min.css">
<link rel="stylesheet" href="{{ base_url() }}assets/js/sweetalert/sweetalert2.min.css">
@include('admin.partials.vuetable')
@endsection

@section('js')
<script type="text/javascript" src="{{ base_url() }}assets/js/vue/vue.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/vue/axios.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/vue/vuetable-2.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/moment/moment.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/sweetalert/sweetalert2.min.js"></script>
@endsection

@section('page_title')
	Atur Artikel
@endsection

@section('page_subtitle')
	{{ $title }}
@endsection

@section('breadcrumb')
<ul class="breadcrumb">
	<li><a href="{{ site_url() }}"><i class="icon-home2 position-left"></i> Home</a></li>
</ul>
@endsection

@section('breadcrumb_elements')

@endsection

@section('main_content')	
<!-- Simple panel -->					
<div class="panel panel-flat" id="app">
	<div class="panel-heading">
		<h5 class="panel-title">List Posts</h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>				
			</ul>
		</div>
	</div>

	<div class="panel-body">
		<div class="table-responsive">		

			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon bg-primary"><i class="icon-search4"></i></span>
					<input type="text" class="form-control" v-model="searchFor" @keyup="setFilter" placeholder="Telusuri Artikel">
					<span class="input-group-addon"><i @click="resetFilter" class="icon-inbox"></i></span>
				</div>
			</div>
					
			<div :class="[{'data-table': true}, loading]">					
				<vuetable ref="vuetable"
					api-url="<?php echo site_url('posts/post_data/') ?>"
					:fields="columns"
					pagination-path=""
					:sort-order="sortOrder"
					:per-page="perPage"
					:append-params="moreParams"
					detail-row-component="my-detail-row"
					@vuetable:cell-clicked="onCellClicked"
					detail-row-transition="expand"					
					:css="css.table"
					track-by="post_id"
					@vuetable:pagination-data="onPaginationData"
					@vuetable:loading="showLoader"
					@vuetable:loaded="hideLoader">

					<template slot="actions" scope="props">
						<div class="btn-group">
							<button class="btn bg-primary btn-xs" @click="editRow(props.rowData)">Edit</button>
							<button class="btn bg-warning btn-xs" @click="delRow(props.rowData)">Delete</button>
						</div>
					</template>
					
				</vuetable>					
			</div>
			
			<div class="data-table-pagination">
				<vuetable-pagination-info ref="paginationInfo"
					:info-template="paginationInfoTemplate">
				</vuetable-pagination-info>
				<vuetable-pagination ref="pagination"
					@vuetable-pagination:change-page="onChangePage"
					:css="css.pagination">
				</vuetable-pagination>			
			</div>
						
		</div>
	</div>
</div>
<!-- /simple panel -->
@endsection

@section('init')
<script type="text/x-template" id="expandtemplate">
	<div @click="onClick">
		<div class="panel panel-primary panel-bordered">
			<div class="panel-heading">
				<h6 class="panel-title">Detail<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-5">	

						<div class="thumbnail">
							<div class="thumb">
								<img :src="'{{ base_url('uploads/') }}'+rowData.post_image" alt="" width="100%">								
							</div>

							<div class="caption">
								<h6 class="no-margin-top text-semibold"><a :href="'{{ site_url('read/news/') }}'+rowData.post_id+'/preview/'+rowData.post_slug" target="_blank" class="text-default">@{{ rowData.post_title }}</a> <a href="#" class="text-muted"></a></h6>
								@{{ rowData.post_excerpt }}
							</div>
						</div>
						
							
						<a href="#" class="badge badge-danger" v-for="item in rowData.tags">
							@{{ item.tag_name }}  
						</a>
						
					</div>
					<div class="col-md-7">						
						<div class="table-responsive">
							<table class="table table-xlg text-nowrap">
								<tbody>
								
									<tr>
										<td class="col-md-6">
											<div class="media-left media-middle">
												<a href="#" class="btn border-success-400 text-success-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-file-text2"></i></a>
											</div>

											<div class="media-left">
												<h5 class="text-semibold no-margin">
													@{{ rowData.cat_name }} <small class="display-block no-margin">Kategori</small>
												</h5>
											</div>
										</td>
								
										<td class="col-md-6">
											<div class="media-left media-middle">
												<a href="#" class="btn border-pink-400 text-pink-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-user"></i></a>
											</div>

											<div class="media-left">
												<h5 class="text-semibold no-margin">
													@{{ rowData.first_name }} <small class="display-block no-margin">Author</small>
												</h5>
											</div>
										</td>									
									</tr>
									
									<tr>
										<td class="col-md-6">
											<div class="media-left media-middle">
												<a href="#" class="btn border-success-400 text-success-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-stack2"></i></a>
											</div>

											<div class="media-left">
												<h5 class="text-semibold no-margin">
													@{{ rowData.post_status }} <small class="display-block no-margin">Post Status</small>
												</h5>
											</div>
										</td>
																		
										<td class="col-md-6">
											<div class="media-left media-middle">
												<a href="#" class="btn border-pink-400 text-pink-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-bubbles4"></i></a>
											</div>

											<div class="media-left">
												<h5 class="text-semibold no-margin">
													@{{ rowData.comment_status }} <small class="display-block no-margin">Comment Status</small>
												</h5>
											</div>
										</td>									
									</tr>
									
									<tr>										
										<td class="col-md-6">
											<div class="media-left media-middle">
												<a href="#" class="btn border-danger-400 text-danger-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-file-text2"></i></a>
											</div>

											<div class="media-left">
												<h5 class="text-semibold no-margin">
													@{{ rowData.subtitle }} <small class="display-block no-margin">Subtitle</small>
												</h5>
											</div>
										</td>									
										<td class="col-md-6">
											<div class="media-left media-middle">
												<a href="#" class="btn border-danger-400 text-danger-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-file-text2"></i></a>
											</div>

											<div class="media-left">
												<h5 class="text-semibold no-margin">
													@{{ rowData.post_sticky }} <small class="display-block no-margin">Post Sticky</small>
												</h5>
											</div>
										</td>	
									</tr>
									
									<tr>
										<td class="col-md-6">
											<div class="media-left media-middle">
												<a href="#" class="btn border-pink-400 text-pink-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-cog"></i></a>
											</div>

											<div class="media-left">
												<h5 class="text-semibold no-margin">
													@{{ rowData.post_featured }} <small class="display-block no-margin">Post Featured</small>
												</h5>
											</div>
										</td>	
										<td class="col-md-6">
											<div class="media-left media-middle">
												<a href="#" class="btn border-pink-400 text-pink-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-cog"></i></a>
											</div>

											<div class="media-left">
												<h5 class="text-semibold no-margin">
													@{{ rowData.post_type }} <small class="display-block no-margin">Post Type</small>
												</h5>
											</div>
										</td>
									</tr>

									
								</tbody>
							</table>
							
						</div>
					
						
					</div>
				</div>
			</div>
		</div>				    
	</div>
</script>

<script>

Vue.component('my-detail-row', {
	template: '#expandtemplate',
	props: {
		rowData: {
			type: Object,
			required: true
		}
	},
	methods: {
		onClick (event) {
			console.log('my-detail-row: on-click', event.target)
		},		
	},
	filters: {
		formatNumber(value) {
			if (value == null) return ''
			return $.number(value, 0)
		},	
		formatDate(value, fmt) {
			if (value == null) return ''
			fmt = (typeof fmt == 'undefined') ? 'D MMM YYYY' : fmt
			return moment(value, 'YYYY-MM-DD').format(fmt)
		},
	}
})

Vue.use(Vuetable);
var vm = new Vue({
	el: '#app',	
	data: {
		loading: '',
		searchFor: '',
		columns: [
			{
				name: 'post_id',
				title: '#',
				sortField: 'post_id',
			},
			{
				name: 'post_title_limit',
				title: 'Judul Berita',
				sortField: 'post_title'
			},						
			{
				name: 'cat_name',
				title: 'Kategori',
				sortField: 'cat_name'
			},
			{
				name: 'post_type',
				title: 'Type',
				sortField: 'post_type'
			},
			{
				name: 'post_status',
				title: 'Stat',
				sortField: 'post_status',				
				callback: 'status',				
			},
			{
				name: 'post_date',
				title: 'Tanggal',
				sortField: 'first_name',
				callback: 'formatDate|D MMM YYYY',
			},
			{
				name: 'post_time',
				title: 'Jam',
				sortField: 'post_time',				
			},
			{
				name: 'first_name',
				title: 'Author',
				sortField: 'first_name'
			},
			{
				name: '__slot:actions',
				title: 'Actions',
			}
		],		
		moreParams: [],
		sortOrder: [{
			field: 'post_id',
			direction: 'desc'
		}],				
		css: {
			table: {
				tableClass: 'table table-xxs',
				ascendingIcon: 'icon-arrow-up22',
				descendingIcon: 'icon-arrow-down22',	
			},		
			pagination: {
				wrapperClass: "btn-group",
				activeClass: "active",
				disabledClass: "disabled",
				pageClass: "btn btn-default",
				linkClass: "btn btn-default",
				icons: {
					first: "icon-chevron-left",
					prev: "icon-arrow-left32",
					next: "icon-arrow-right32",
					last: "icon-chevron-right"
				}
			}
		},
		//paginationComponent: 'vuetable-pagination',
		perPage: 20,
		paginationInfoTemplate: '<strong>Showing record</strong> {from} to {to} from {total} item(s)',
		
	},	
	methods: {			
		setFilter () {
			this.moreParams = {
				'filter': this.searchFor
			}
			this.$nextTick(function() {
				this.$refs.vuetable.refresh()
			})
		},
		
		formatDate(value, fmt) {
			if (value == null) return ''
			fmt = (typeof fmt == 'undefined') ? 'D MMM YYYY' : fmt
			return moment(value, 'YYYY-MM-DD').format(fmt)
		},
		formatNumber(value, fmt) {
			if (value == null) return ''
			return $.number(value, fmt)
		},
		status(value) {
			return value == 'publish'
                ? '<span class="label label-sm label-success">publish</span>'
                : '<span class="label label-sm label-danger">draft</span>'
		},
				
		resetFilter () {
			this.searchFor = ''
			this.setFilter()
		},
		showLoader () {
			this.loading = 'loading'
		},
		hideLoader () {
			this.loading = ''
		},
		
		editRow(rowData){
			var url = '{{ site_url('posts/edit/') }}' + rowData.post_id
			$(location).attr('href', '{{ site_url('posts/edit/') }}' + rowData.post_id)			
		},				
			
		onPaginationData (tablePagination) {
			this.$refs.paginationInfo.setPaginationData(tablePagination)
			this.$refs.pagination.setPaginationData(tablePagination)
		},
		onChangePage (page) {
			this.$refs.vuetable.changePage(page)
		},		
		onInitialized (fields) {
			console.log('onInitialized', fields)
			this.vuetableFields = fields
		},
		
		onCellClicked (data, field, event) {
			console.log('cellClicked: ', field.name)
			this.$refs.vuetable.toggleDetailRow(data.post_id)
		},
		
		onDataReset () {
			console.log('onDataReset')
			this.$refs.paginationInfo.resetData()
			this.$refs.pagination.resetData()
		},
		
	},
	
})

</script>
@endsection			