@extends('admin.layouts.master')

@section('page_title')
	Last Login
@endsection

@section('page_subtitle')
	List of last logged in users
@endsection

@section('breadcrumb')
<ul class="breadcrumb">
	<li><a href="{{ site_url() }}"><i class="icon-home2 position-left"></i> Home</a></li>
</ul>
@endsection

@section('main_content')	
<!-- Simple panel -->					
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">User last logged in</h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>				
			</ul>
		</div>
	</div>

	<div class="panel-body">
		
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>IP Last Logged In</th>
						<th>Name</th>
						<th>Email</th>
						<th>Date Login</th>
						<th>Time Login</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($users as $user): ?>
					<tr>
						<td><?php echo $user->id ?></td>
						<td><?php echo $user->ip_address ?></td>
						<td><?php echo $user->first_name ?></td>
						<td><?php echo $user->email ?></td>
						<td><?php echo date('j F Y',$user->last_login) ?></td>
						<td><?php echo date('H:i:s',$user->last_login) ?></td>
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>					
		</div>		
	</div>
</div>
<!-- /simple panel -->					

@endsection				