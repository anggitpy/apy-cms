@extends('admin.layouts.master')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css">
@include('admin.partials.vuetable')
@endsection

@section('js')
<script type="text/javascript" src="{{ base_url() }}assets/js/vue/vue.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/vue/axios.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/vue/vuetable-2.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/validator/validator.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/moment/moment.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/loaders/blockui.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.js"></script>
@endsection

@section('page_title')
	Breaking News
@endsection

@section('page_subtitle')
	{{ $title }}
@endsection

@section('breadcrumb')
<ul class="breadcrumb">
	<li><a href="{{ site_url() }}"><i class="icon-home2 position-left"></i> Home</a></li>
</ul>
@endsection

@section('breadcrumb_elements')
<ul class="breadcrumb-elements">
	<li><a href="{{ site_url('') }}"><i class="icon-file-plus position-left"></i> Additional</a></li>	
</ul>
@endsection

@section('main_content')	
<div class="row">
	<div class="col-md-8">
		<div class="panel panel-flat" id="app">
			<div class="panel-heading">
			
				<?php if($this->session->flashdata('upload_message')): ?>
					{{ $this->session->flashdata('upload_message') }}
				<?php endif ?>
				
				<?php if($this->session->flashdata('pk_message')): ?>
					<div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
						<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
						{{ $this->session->flashdata('pk_message') }}
					</div>
				<?php endif ?>
			
			
				<h5 class="panel-title">Breaking News Table</h5>
				<div class="heading-elements">					
				</div>
			</div>

			<div class="panel-body">
				<div class="table-responsive">		

					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon bg-primary"><i class="icon-search4"></i></span>
							<input type="text" class="form-control" v-model="searchFor" @keyup="setFilter" placeholder="Telusuri Breaking News">
							<span class="input-group-addon"><i @click="resetFilter" class="icon-inbox"></i></span>
						</div>
					</div>
							
					<div :class="[{'data-table': true}, loading]">					
						<vuetable ref="vuetable"
							api-url="<?php echo site_url('breaking/breaking_data/') ?>"
							:fields="columns"
							pagination-path=""
							:sort-order="sortOrder"
							:per-page="perPage"
							:append-params="moreParams"
							@vuetable:cell-clicked="onCellClicked"									
							:css="css.table"
							track-by="br_id"
							@vuetable:pagination-data="onPaginationData"
							@vuetable:loading="showLoader"
							@vuetable:loaded="hideLoader">

							<template slot="actions" scope="props">
								<div class="btn-group">
									<button class="btn bg-primary btn-xs" @click="editRow(props.rowData)">Edit</button>												
								</div>
							</template>
							
						</vuetable>					
					</div>
					
					
					<div class="data-table-pagination">
						<vuetable-pagination-info ref="paginationInfo"
							:info-template="paginationInfoTemplate">
						</vuetable-pagination-info>
						<vuetable-pagination ref="pagination"
							@vuetable-pagination:change-page="onChangePage"
							:css="css.pagination">
						</vuetable-pagination>			
					</div>
								
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">Add Breaking News</h5>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>				
					</ul>
				</div>
			</div>

			<div class="panel-body">
				<?php echo form_open('breaking/save_news','id="saveNews" data-toggle="validator"') ?>	
				<div class="form-group">				
					<textarea name="text" class="form-control" placeholder="Breaking News Text (HTMLs are allowed)" rows="4" maxlength="300" required></textarea>
					<span class="help-block with-errors"></span>
				</div>					
				<div class="form-group no-margin-bottom">
					<button type="submit" class="btn btn-danger btn-block btn-lg">Save News</button>
				</div>
				<?php echo form_close() ?>	
			</div>
		</div>
	</div>
	
</div>	
@endsection				

@section('init')
<script>
Vue.use(Vuetable);
var vm = new Vue({
	el: '#app',	
	data: {
		loading: '',
		searchFor: '',
		columns: [
			{
				name: 'br_id',
				title: '#',
				sortField: 'br_id',
			},				
			{
				name: 'text',
				title: 'Breaking News Text',
				sortField: 'text'
			},						
			{
				name: 'date',
				title: 'Tanggal',
				sortField: 'date',
				callback: 'formatDate|D.MMM.YYYY'
			},
			{
				name: 'time',
				title: 'Jam',
				sortField: 'time'
			},
			{
				name: '__slot:actions',
				title: 'Actions',
			}	
		],		
		moreParams: [],
		sortOrder: [{
			field: 'br_id',
			direction: 'desc'
		}],				
		css: {
			table: {
				tableClass: 'table table-xxs',
				ascendingIcon: 'icon-arrow-up22',
				descendingIcon: 'icon-arrow-down22',	
			},		
			pagination: {
				wrapperClass: "btn-group",
				activeClass: "active",
				disabledClass: "disabled",
				pageClass: "btn btn-default",
				linkClass: "btn btn-default",
				icons: {
					first: "icon-chevron-left",
					prev: "icon-arrow-left32",
					next: "icon-arrow-right32",
					last: "icon-chevron-right"
				}
			}
		},
		//paginationComponent: 'vuetable-pagination',
		perPage: 10,
		paginationInfoTemplate: '<strong>Showing record</strong> {from} to {to} from {total} item(s)',
		
	},	
	methods: {			
		setFilter () {
			this.moreParams = {
				'filter': this.searchFor
			}
			this.$nextTick(function() {
				this.$refs.vuetable.refresh()
			})
		},
		
		formatDate(value, fmt) {
			if (value == null) return ''
			fmt = (typeof fmt == 'undefined') ? 'D MMM YYYY' : fmt
			return moment(value, 'YYYY-MM-DD').format(fmt)
		},
		formatNumber(value, fmt) {
			if (value == null) return ''
			return $.number(value, fmt)
		},		
				
		resetFilter () {
			this.searchFor = ''
			this.setFilter()
		},
		showLoader () {
			this.loading = 'loading'
		},
		hideLoader () {
			this.loading = ''
		},
		
		editRow(rowData) {
			$(location).attr('href', '{{ site_url('breaking/edit/') }}' + rowData.br_id)		
		},	
		
		onPaginationData (tablePagination) {
			this.$refs.paginationInfo.setPaginationData(tablePagination)
			this.$refs.pagination.setPaginationData(tablePagination)
		},
		onChangePage (page) {
			this.$refs.vuetable.changePage(page)
		},		
		onInitialized (fields) {
			console.log('onInitialized', fields)
			this.vuetableFields = fields
		},
		
		onCellClicked (data, field, event) {
			console.log('cellClicked: ', field.name)
			this.$refs.vuetable.toggleDetailRow(data.image_id)
		},
		
		onDataReset () {
			console.log('onDataReset')
			this.$refs.paginationInfo.resetData()
			this.$refs.pagination.resetData()
		},
		
	},
})

$('#saveNews').validator().on('submit', function(e) {
	if (e.isDefaultPrevented()) {
		console.log('Not Submit');
	} else {		
		$.blockUI({ 
			message: '<i class="icon-spinner4 spinner"></i>',
			timeout: 0, //unblock after 2 seconds
			overlayCSS: {
				backgroundColor: '#1b2024',
				opacity: 0.8,
				zIndex: 1200,
				cursor: 'wait'
			},
			css: {
				border: 0,
				color: '#fff',
				padding: 0,
				zIndex: 1201,
				backgroundColor: 'transparent'
			}
		});			
	}
})

</script>
@endsection