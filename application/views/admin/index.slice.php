@extends('admin.layouts.master')

@section('css')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
@endsection

@section('js')
<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/ui/moment/moment.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/ui/fullcalendar/fullcalendar.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
@endsection

@section('page_title')
	Dashboard
@endsection

@section('page_subtitle')
	Statistics and Overview
@endsection

@section('breadcrumb')
<ul class="breadcrumb">
	<li><a href="{{ site_url('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
</ul>
@endsection

@section('main_content')	
<div class="row">
	<div class="col-sm-6 col-md-3">
		<div class="panel panel-body panel-body-accent">
			<div class="media no-margin">
				<div class="media-left media-middle">
					<i class="icon-pointer icon-3x text-success-400"></i>
				</div>

				<div class="media-body text-right">
					<h3 class="no-margin text-semibold">{{ $published_post }}</h3>
					<span class="text-uppercase text-size-mini text-muted">published posts</span>
				</div>
			</div>
		</div>
	</div>

	<div class="col-sm-6 col-md-3">
		<div class="panel panel-body">
			<div class="media no-margin">
				<div class="media-left media-middle">
					<i class="icon-enter6 icon-3x text-indigo-400"></i>
				</div>

				<div class="media-body text-right">
					<h3 class="no-margin text-semibold">{{ $draft_post }}</h3>
					<span class="text-uppercase text-size-mini text-muted">drafted posts</span>
				</div>
			</div>
		</div>
	</div>

	<div class="col-sm-6 col-md-3">
		<div class="panel panel-body">
			<div class="media no-margin">
				<div class="media-body">
					<h3 class="no-margin text-semibold">{{ $categories }}</h3>
					<span class="text-uppercase text-size-mini text-muted">Categories</span>
				</div>

				<div class="media-right media-middle">
					<i class="icon-file-text2 icon-3x text-blue-400"></i>
				</div>
			</div>
		</div>
	</div>

	<div class="col-sm-6 col-md-3">
		<div class="panel panel-body">
			<div class="media no-margin">
				<div class="media-body">
					<h3 class="no-margin text-semibold">{{ $tags }}</h3>
					<span class="text-uppercase text-size-mini text-muted">Tags</span>
				</div>

				<div class="media-right media-middle">
					<i class="icon-hash icon-3x text-pink-400"></i>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-6 col-md-3">
		<div class="panel panel-body">
			<div class="media no-margin">
				<div class="media-body">
					<h3 class="no-margin text-semibold">{{ $ads }}</h3>
					<span class="text-uppercase text-size-mini text-muted">ads position</span>
				</div>

				<div class="media-right media-middle">
					<i class="icon-versions icon-3x text-danger-400"></i>
				</div>
			</div>
		</div>
	</div>

	<div class="col-sm-6 col-md-3">
		<div class="panel panel-body">
			<div class="media no-margin">
				<div class="media-body">
					<h3 class="no-margin text-semibold">{{ $ads_client }}</h3>
					<span class="text-uppercase text-size-mini text-muted">ads client</span>
				</div>

				<div class="media-right media-middle">
					<i class="icon-person icon-3x text-danger-400"></i>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-sm-6 col-md-3">
		<div class="panel panel-body">
			<div class="media no-margin">
				<div class="media-body">
					<h3 class="no-margin text-semibold">{{ $ads_assets }}</h3>
					<span class="text-uppercase text-size-mini text-muted">ads assets</span>
				</div>

				<div class="media-right media-middle">
					<i class="icon-stack-check icon-3x text-danger-400"></i>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-sm-6 col-md-3">
		<div class="panel panel-body">
			<div class="media no-margin">
				<div class="media-body">
					<h3 class="no-margin text-semibold">{{ $breaking_news }}</h3>
					<span class="text-uppercase text-size-mini text-muted">Breaking News</span>
				</div>

				<div class="media-right media-middle">
					<i class="icon-notification2 icon-3x text-danger-400"></i>
				</div>
			</div>
		</div>
	</div>
	
</div>

<div class="row">
	<div class="col-md-6">
		<div class="panel panel-flat">

			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>							
							<th style="width: 230px;">Thumb</th>
							<th>Title</th>							
						</tr>
					</thead>
					<tbody>
						<tr class="active border-double">
							<td>Artikel Terkini</td>	
							<td><a href="{{site_url('posts/add') }}" class="btn btn-primary btn-xs">Add New Post</a></td>														
						</tr>

						<?php foreach($recents as $recent): ?>
						<tr>	
							<?php $date = str_replace('-', '', $recent->post_date); ?>
							<td>
								<div class="media-left media-middle">
									<a href="{{ site_url('read/news/'.$recent->post_id.'/'.$date.'/'.$recent->post_slug) }}" target="_blank"><img src="<?php echo image('/uploads/'.$recent->post_image, 'thumb_small'); ?>" alt="{{ $recent->image_title }}" width="50px;"/></a>
								</div>

								<div class="media-body">
									<div class="display-inline-block text-default text-semibold letter-icon-title">{{ $recent->cat_name }}</div>
									<div class="text-muted text-size-small">{{ date_indo($recent->post_date) }}</div>
								</div>
							</td>
							<td>
								<a href="#" class="text-default display-inline-block">
									<span class="text-semibold">{{ $recent->post_title }}</span>									
								</a>
							</td>							
						</tr>
						<?php endforeach ?>											
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="panel panel-flat">

			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>							
							<th style="width: 230px;">Thumb</th>
							<th>Title</th>							
						</tr>
					</thead>
					<tbody>						
						<tr class="active border-double">
							<td>Artikel Populer</td>	
							<td><a href="{{site_url() }}" class="btn btn-danger btn-xs" target="_blank">View Sites</a></td>
						</tr>

						<?php foreach($popular as $recent): ?>
						<tr>
							<?php $date = str_replace('-', '', $recent->post_date); ?>
							<td>
								<div class="media-left media-middle">
									<a href="{{ site_url('read/news/'.$recent->post_id.'/'.$date.'/'.$recent->post_slug) }}" target="_blank"><img src="<?php echo image('/uploads/'.$recent->post_image, 'thumb_small'); ?>" alt="{{ $recent->image_title }}" width="50px;"/></a>
								</div>

								<div class="media-body">
									<div class="display-inline-block text-default text-semibold letter-icon-title">{{ $recent->cat_name }}</div>
									<div class="text-muted text-size-small">{{ date_indo($recent->post_date) }}</div>
								</div>
							</td>
							<td>
								<a href="#" class="text-default display-inline-block">
									<span class="text-semibold">{{ $recent->post_title }}</span>									
								</a>
							</td>							
						</tr>
						<?php endforeach ?>
						
					</tbody>
				</table>
			</div>
		</div>
	</div>	
</div>

@endsection

@section('init')

@endsection