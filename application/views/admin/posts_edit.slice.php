@extends('admin.layouts.master')

@section('css')
<link rel="stylesheet" href="{{ base_url() }}assets/js/sweetalert/sweetalert2.min.css">
@include('admin.partials.uploader')
@include('admin.partials.vuetable')
@endsection

@section('js')
<script type="text/javascript" src="{{ base_url() }}assets/js/vue/vue.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/vue/axios.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/vue/vuetable-2.js"></script>

<script type="text/javascript" src="{{ base_url() }}assets/js/validator/validator.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/forms/inputs/formatter.min.js"></script>

<script type="text/javascript" src="{{ base_url() }}assets/js/sweetalert/sweetalert2.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/forms/selects/select2.min.js"></script>

<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/editors/summernote/summernote.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/forms/styling/uniform.min.js"></script>

<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/loaders/blockui.min.js"></script>

<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/uploaders/fileinput/plugins/purify.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/uploaders/fileinput/fileinput.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/pages/uploader_bootstrap.js"></script>

<script type="text/javascript" src="{{ base_url() }}assets/js/core/libraries/jquery_ui/interactions.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/core/libraries/jquery_ui/widgets.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/core/libraries/jquery_ui/effects.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/extensions/mousewheel.min.js"></script>

<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/pickers/anytime.min.js"></script>

@endsection


@section('page_title')
	{{ $title }}
@endsection

@section('page_subtitle')
	Edit Berita
@endsection

@section('breadcrumb')
<ul class="breadcrumb">
	<li><a href="{{ site_url() }}"><i class="icon-home2 position-left"></i> Home</a></li>
</ul>
@endsection

@section('breadcrumb_elements')
<ul class="breadcrumb-elements">
	<li><a href="{{ site_url('') }}"><i class="icon-file-plus position-left"></i> Additional</a></li>	
</ul>
@endsection

@section('main_content')	
<?php echo form_open_multipart('posts/update/'.$post->post_id,'id="postSave" data-toggle="validator"') ?>

<?php if($this->session->flashdata('upload_message')): ?>
	{{ $this->session->flashdata('upload_message') }}
<?php endif ?>

<?php if($this->session->flashdata('pk_message')): ?>
	<div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
		<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
		{{ $this->session->flashdata('pk_message') }}
	</div>
<?php endif ?>

<div class="row">

	<div class="col-md-8">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h5 class="panel-title text-semibold">Entry Baru</h5>				
			</div>

			<div class="panel-body">
				<div class="form-group no-margin-bottom">
					<label>Judul Berita</label>
					<input type="text" name="post_title" class="form-control" placeholder="Judul Berita" value="{{ $post->post_title }}" required>
					<span class="help-block with-errors"></span>
				</div>
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Sub Judul Berita" value="{{ $post->subtitle }}" id="subtitle">					
					<input type="hidden" name="post_subtitle" class="form-control" value="{{ $post->post_subtitle }}" id="subtitle_id">
					<span class="help-block with-errors"></span>
				</div>
				<div class="form-group">
					<label>Isi Berita</label>
					<textarea name="post_content" class="form-control" id="summernote" placeholder="Isi Berita">{{ $post->post_content }}</textarea>
					<span class="help-block with-errors"></span>
				</div>
				<div class="form-group">
					<label>Excerpt</label>
					<textarea name="post_excerpt" class="form-control" placeholder="Ringkasan Berita" required>{{ $post->post_excerpt }}</textarea>
					<span class="help-block with-errors"></span>
				</div>
				<div class="form-group">
					<label>Youtube</label>
					<input name="youtube" class="form-control" placeholder="Youtube Code" value="{{ $post->youtube }}">
					<span class="help-block with-errors"></span>
				</div>
				
				<div class="row">
					<div class="col-md-4">
						<div class="form-group no-margin-bottom">
							<label>Category and Tags</label>
							<?php echo form_dropdown('post_category', $post_category, $post->post_category,'class="form-control" id="post_category" required'); ?>
							<span class="help-block with-errors"></span>
						</div>
						<div class="form-group">					
							<?php echo form_multiselect('tags[]', $select_tag, $current_tag,'data-placeholder="Select Tags" class="form-control" id="tags"'); ?>
						</div>					
					</div>
					<div class="col-md-4">
						<div class="form-group no-margin-bottom">
							<label>Post and Comment Stat</label>
							<?php echo form_dropdown('comment_status', $comment_status, $post->comment_status,'class="form-control" id="comment_status" required'); ?>
							<span class="help-block with-errors"></span>
						</div>
						<div class="form-group">					
							<?php echo form_dropdown('post_status', $post_status, $post->post_status,'class="form-control" id="post_status" required'); ?>
							<span class="help-block with-errors"></span>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group no-margin-bottom">
							<label>Featured and Sticky</label>
							<?php echo form_dropdown('post_sticky', $post_sticky, $post->post_sticky,'class="form-control" id="post_sticky" required'); ?>
							<span class="help-block with-errors"></span>
						</div>
						<div class="form-group">					
							<?php echo form_dropdown('post_featured', $post_featured, $post->post_featured,'class="form-control" id="post_featured" required'); ?>
							<span class="help-block with-errors"></span>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label>Schedule Date</label>
					<input name="scheduled_date" class="form-control" id="scheduled_date" placeholder="Schedule Date" value="{{ $post->scheduled_date }}">
					<span class="help-block with-errors"></span>
				</div>
				
				
			</div>
		</div>
	</div>
	
	<div class="col-md-4">
		
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h6 class="panel-title text-semibold">Keywords</h6>				
			</div>
			
			<div class="panel-body">							
				<div class="form-group">
					<textarea name="keywords" class="form-control" placeholder="Keywords, pisahkan dengan koma">{{ $post->post_keywords }}</textarea>
				</div>	
			</div>
			
		</div>
		
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h6 class="panel-title text-semibold">Image</h6>				
			</div>
			
			<div class="panel-body">
				<div id="imagedata">
					<div class="form-group no-margin-bottom">
						<input type="text" name="image_title" id="image_title" class="form-control" placeholder="Judul Gambar" value="{{ $post->image_title }}">
						<span class="help-block with-errors"></span>
					</div>
					<div class="form-group">
						<input type="text" name="image_caption" id="image_caption" class="form-control" placeholder="Keterangan Gambar" value="{{ $post->image_caption }}">
						<span class="help-block with-errors"></span>
					</div>
				</div>
				<div id="imagecurrent">
					<img src="{{ base_url() }}uploads/{{ $post->post_image }}" alt="">					
				</div>
				<div id="imagenew"></div>
				
				<div class="form-group">
					<input type="file" name="post_image" id="post_image" class="file-input btn-block" data-show-remove="false" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-block" data-max-file-size="1280">
					<!-- get value from library -->
					<input type="hidden" name="image_id" id="image_id" value="">
				</div>
				<div class="form-group no-margin-bottom">
					<button type="button" class="btn btn-danger btn-block btn-lg" data-toggle="modal" data-target="#imagelib">Image Library</button>
				</div>	
			</div>
			
		</div>
	
		
	</div>
	
</div>

<div class="form-group">
	<button type="submit" class="btn btn-warning btn-block btn-labeled btn-xlg"><b><i class="icon-pin-alt"></i></b> Simpan</button>
</div>

</form>


<!-- Full width modal -->
<div id="imagelib" class="modal fade">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Select Image</h4>
			</div>

			<div class="modal-body">
				
				<div class="table-responsive" id="app">		

					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon bg-primary"><i class="icon-search4"></i></span>
							<input type="text" class="form-control" v-model="searchFor" @keyup="setFilter" placeholder="Telusuri gambar">
							<span class="input-group-addon"><i @click="resetFilter" class="icon-inbox"></i></span>
						</div>
					</div>
							
					<div :class="[{'data-table': true}, loading]">					
						<vuetable ref="vuetable"
							api-url="<?php echo site_url('posts/image_data/') ?>"
							:fields="columns"
							pagination-path=""
							:sort-order="sortOrder"
							:per-page="perPage"
							:append-params="moreParams"
							detail-row-component="my-detail-row"
							@vuetable:cell-clicked="onCellClicked"
							detail-row-transition="expand"					
							:css="css.table"
							track-by="image_id"
							@vuetable:pagination-data="onPaginationData"
							@vuetable:loading="showLoader"
							@vuetable:loaded="hideLoader">

							<template slot="actions" scope="props">
								<div class="btn-group">
									<button class="btn bg-primary btn-xs" @click="detailRow(props.rowData)">Pick</button>
									<button class="btn bg-warning btn-xs" @click="toEditor(props.rowData)">Editor</button>								
								</div>
							</template>
							
						</vuetable>					
					</div>
					
					
					<div class="data-table-pagination">
						<vuetable-pagination-info ref="paginationInfo"
							:info-template="paginationInfoTemplate">
						</vuetable-pagination-info>
						<vuetable-pagination ref="pagination"
							@vuetable-pagination:change-page="onChangePage"
							:css="css.pagination">
						</vuetable-pagination>			
					</div>
								
				</div>				
				
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>				
			</div>
		</div>
	</div>
</div>
<!-- /full width modal -->

@endsection				

@section('init')
<script type="text/x-template" id="expandtemplate">
	<div @click="onClick">
		<div class="panel panel-primary panel-bordered">
			<div class="panel-heading">
				<h6 class="panel-title">Details<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			</div>
			<div class="panel-body">
				<div class="row">					
					<div class="col-md-6">
						<img :src="'{{ base_url('uploads/') }}'+rowData.image_file_name" alt="" width="100%">
					</div>
				</div>
			</div>
		</div>				    
	</div>
</script>

<script>

Vue.component('my-detail-row', {
	template: '#expandtemplate',
	props: {
		rowData: {
			type: Object,
			required: true
		}
	},
	methods: {
		onClick (event) {
			console.log('my-detail-row: on-click', event.target)
		},		
	}
})

Vue.use(Vuetable);
var vm = new Vue({
	el: '#app',	
	data: {
		loading: '',
		searchFor: '',
		columns: [
			{
				name: 'image_thumb',
				title: 'Preview',
				sortField: 'image_thumb',
				callback: 'load_image',
			},				
			{
				name: 'image_title',
				title: 'Title',
				sortField: 'image_title'
			},						
			{
				name: 'image_caption',
				title: 'Caption',
				sortField: 'image_caption'
			},				
			{
				name: '__slot:actions',
				title: 'Actions',
			}
		],		
		moreParams: [],
		sortOrder: [{
			field: 'image_id',
			direction: 'desc'
		}],				
		css: {
			table: {
				tableClass: 'table table-xxs',
				ascendingIcon: 'icon-arrow-up22',
				descendingIcon: 'icon-arrow-down22',	
			},		
			pagination: {
				wrapperClass: "btn-group",
				activeClass: "active",
				disabledClass: "disabled",
				pageClass: "btn btn-default",
				linkClass: "btn btn-default",
				icons: {
					first: "icon-chevron-left",
					prev: "icon-arrow-left32",
					next: "icon-arrow-right32",
					last: "icon-chevron-right"
				}
			}
		},
		//paginationComponent: 'vuetable-pagination',
		perPage: 4,
		paginationInfoTemplate: '<strong>Showing record</strong> {from} to {to} from {total} item(s)',
		
	},	
	methods: {			
		setFilter () {
			this.moreParams = {
				'filter': this.searchFor
			}
			this.$nextTick(function() {
				this.$refs.vuetable.refresh()
			})
		},
		
		formatDate(value, fmt) {
			if (value == null) return ''
			fmt = (typeof fmt == 'undefined') ? 'D MMM YYYY' : fmt
			return moment(value, 'YYYY-MM-DD').format(fmt)
		},
		formatNumber(value, fmt) {
			if (value == null) return ''
			return $.number(value, fmt)
		},
		load_image(value) {			
			return '<img src="{{base_url('uploads/')}}'+value+'" alt="" height="50">'			
		},
				
		resetFilter () {
			this.searchFor = ''
			this.setFilter()
		},
		showLoader () {
			this.loading = 'loading'
		},
		hideLoader () {
			this.loading = ''
		},
		
		detailRow(rowData) {
			
			var img = document.createElement("IMG");
			img.src = '{{ base_url('uploads/') }}'+rowData.image_file_name;
			img.setAttribute('width', '100%');
			$('#imagenew').html(img); 
			$('#imagecurrent').hide(); 
			$('#image_title').prop('required', true);
			$('#image_caption').prop('required', true);
			
			//$('#image_title').val(rowData.image_title);
			//$('#image_caption').val(rowData.image_caption);
			$('#image_id').val(rowData.image_id);
			$('#imagelib').modal('hide');
		},	

		toEditor(rowData) {
			/* var image = rowData.to_editor;
			$('#summernote').summernote('code', '<img src="'+image+'">')
			$('#imagelib').modal('hide');*/
			//var url = '{{ base_url('uploads/') }}'
			var url = '/uploads/'
			var filename = rowData.image_file_name
			var image = rowData.to_editor
			console.log(url+filename);
			$('#summernote').summernote('insertImage', url+filename)
			$('#imagelib').modal('hide')		
		},
		
		onPaginationData (tablePagination) {
			this.$refs.paginationInfo.setPaginationData(tablePagination)
			this.$refs.pagination.setPaginationData(tablePagination)
		},
		onChangePage (page) {
			this.$refs.vuetable.changePage(page)
		},		
		onInitialized (fields) {
			console.log('onInitialized', fields)
			this.vuetableFields = fields
		},
		
		onCellClicked (data, field, event) {
			console.log('cellClicked: ', field.name)
			this.$refs.vuetable.toggleDetailRow(data.image_id)
		},
		
		onDataReset () {
			console.log('onDataReset')
			this.$refs.paginationInfo.resetData()
			this.$refs.pagination.resetData()
		},
		
	},
})

AnyTime.picker('scheduled_date');
$(document).ready(function(){
	
	var ac_config = {	
		source: "{{ site_url('posts/load_subtitle') }}",			
		select: function(event, ui){						
			$("#subtitle_id").val(ui.item.subtitle_id);			
		},
		minLength:1,
		delay: 0
	};
	$("#subtitle").autocomplete(ac_config);
	
	
	$('#summernote').summernote({
		height: 300
	})
	$('#tags').select2({
        minimumResultsForSearch: Infinity
    });
		
	$('#post_image').on('change', function(event) {		
		$('#image_title').prop('required', true);
		$('#image_caption').prop('required', true);
	})
	$('#post_image').on('fileclear', function(event) {		
		$('#image_title').prop('required', false);
		$('#image_caption').prop('required', false);
	})
	
	$('#postSave').validator().on('submit', function(e) {
		if (e.isDefaultPrevented()) {
			console.log('Not Submit');
		} else {		
			$.blockUI({ 
				message: '<i class="icon-spinner4 spinner"></i>',
				timeout: 0, //unblock after 2 seconds
				overlayCSS: {
					backgroundColor: '#1b2024',
					opacity: 0.8,
					zIndex: 1200,
					cursor: 'wait'
				},
				css: {
					border: 0,
					color: '#fff',
					padding: 0,
					zIndex: 1201,
					backgroundColor: 'transparent'
				}
			});			
		}
	})
	
})
</script>
@endsection