@extends('admin.layouts.master')

@section('css')
<style type="text/css">
.is-parent {margin-bottom: 2px; padding-bottom: 2px; border-bottom: 1px solid #eee; font-size: 20px; font-weight: 600;}
.has-child {margin-bottom: 5px; padding-bottom: 5px; border-bottom: 0; margin-left: 10px; font-size: 16px; font-weight: 400;}
.is-child.has-child {font-size: 12px; text-transform: capitalize}
.is-child:last-child {margin-bottom: 5px;}
</style>
@endsection

@section('js')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/buttons/spin.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/buttons/ladda.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/loaders/blockui.min.js"></script>
@endsection

@section('page_title')
	Categories
@endsection

@section('page_subtitle')
	Category Management
@endsection

@section('breadcrumb')
<ul class="breadcrumb">
	<li><a href="{{ site_url('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
</ul>
@endsection

@section('breadcrumb_elements')
<ul class="breadcrumb-elements">
	<li><a href="{{ site_url('') }}"><i class="icon-file-plus position-left"></i> Additional</a></li>	
</ul>
@endsection

@section('main_content')	
<!-- Simple panel -->

<?php if($this->session->flashdata('pk_msg')): ?>
<div class="alert alert-info">
	<?php echo $this->session->flashdata('pk_msg'); ?>
</div>
<?php endif ?>
					
<div class="row">
    <div class="col-md-3">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Tambah Kategori<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
				<?php echo form_open('category/save_category','id="categoryForm" data-toggle="validator"') ?>
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" name="category_name" class="form-control" placeholder="Nama Kategori" required>
						<span class="help-block with-errors"></span>
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        <textarea name="description" class="form-control" placeholder="Keterangan"></textarea>
						<span class="help-block with-errors"></span>
                    </div>
                    <div class="form-group">
                        <label>Parents</label>
                        <?php echo form_dropdown('category_parent', $category_parent, set_value('category_parent'),'class="form-control" id="category_parent"'); ?>
						<span class="help-block with-errors"></span>
                    </div>
					<div class="form-group">
						<button type="submit" class="btn bg-blue btn-block" id="block-page">Save</button>
					</div>
                <?php echo form_close() ?>
            </div>
        </div>
    </div>
    
    <!-- Sidebar, should be included -->
    <div class="col-md-4">        
       <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Layouts<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                    </ul>
                </div>
            </div>
            <div class="panel-body"> 				
                <div class="category">{{ (fetch_menu()) }}</div>
            </div>
        </div>
    </div>
	
	 <div class="col-md-5">        
       <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Actions<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                    </ul>
                </div>
            </div>
            <div class="panel-body">              
                <div class="table responsive">
					<table class="table table-xxs">
						<tbody>
							<?php foreach($categories as $cat): ?>
							<tr>
								<td>{{ $cat->cat_id }} &mdash; {{ $cat->cat_name }}</td>
								<td>
									<div class="btn-group">
										<a href="{{ site_url('category/edit/'.$cat->cat_id) }}" type="button" class="btn btn-warning btn-xs"><i class="icon-pencil3 position-left"></i> Edit</a>
										<a href="{{ site_url('category/delete/'.$cat->cat_id) }}" type="button" class="btn btn-danger btn-xs"><i class="position-left"></i> Hapus</a>
									</div>
								</td>
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
            </div>
        </div>
    </div>
    
</div>
<!-- /simple panel -->	
@endsection		

@section('init')
<script>
$(function() {
	
	$('#categoryForm').validator().on('submit', function(e) {
		if (e.isDefaultPrevented()) {
			console.log('Not Submit');
		} else {		
			$.blockUI({ 
				message: '<i class="icon-spinner4 spinner"></i>',
				timeout: 0, //unblock after 2 seconds
				overlayCSS: {
					backgroundColor: '#1b2024',
					opacity: 0.8,
					zIndex: 1200,
					cursor: 'wait'
				},
				css: {
					border: 0,
					color: '#fff',
					padding: 0,
					zIndex: 1201,
					backgroundColor: 'transparent'
				}
			});			
		}
	})
	
})	

</script>
@endsection		