@extends('admin.layouts.master')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css">
<link rel="stylesheet" href="{{ base_url() }}assets/css/icons/fontawesome/styles.min.css">
@include('admin.partials.vuetable')
@endsection

@section('js')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/buttons/spin.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/buttons/ladda.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/pickers/pickadate/picker.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/pickers/pickadate/picker.date.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/df-number-format/2.1.5/jquery.number.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/vue/vue.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/vue/axios.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/vue/vuetable-2.js"></script>
<script src="{{ base_url() }}assets/js/moment/moment.js"></script>
@endsection

@section('page_title')
	Media Library
@endsection

@section('page_subtitle')
	{{ $title }}
@endsection

@section('breadcrumb')
<ul class="breadcrumb">
	<li><a href="{{ site_url('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
</ul>
@endsection

@section('breadcrumb_elements')
<ul class="breadcrumb-elements">
	<li><a href="{{ site_url('media/add') }}"><i class="icon-file-plus position-left"></i> Add</a></li>	
</ul>
@endsection

@section('main_content')	
<!-- Simple panel -->					
<div class="row">

    <div class="panel panel-flat" id="app">
        <div class="panel-heading">
		
			<?php if($this->session->flashdata('pk_message')): ?>
			<div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
				<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
				{{ $this->session->flashdata('pk_message') }}
			</div>
			<?php endif ?>
		
            <h5 class="panel-title">Image</h5>
            <div class="heading-elements">              
            </div>
        </div>

        <div class="panel-body">
            <div class="table-responsive">		

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon bg-primary"><i class="icon-search4"></i></span>
                        <input type="text" class="form-control" v-model="searchFor" @keyup="setFilter" placeholder="Search Image">
                        <span class="input-group-addon"><i @click="resetFilter" class="icon-inbox"></i></span>
                    </div>
                </div>
                        
                <div :class="[{'data-table': true}, loading]">					
                    <vuetable ref="vuetable"
                        api-url="<?php echo site_url('media/media_data/') ?>"
                        :fields="columns"
                        pagination-path=""
                        :sort-order="sortOrder"
                        :per-page="perPage"
                        :append-params="moreParams"
                        detail-row-component="my-detail-row"
                        @vuetable:cell-clicked="onCellClicked"
                        detail-row-transition="expand"					
                        :css="css.table"
                        track-by="image_id"
                        @vuetable:pagination-data="onPaginationData"
                        @vuetable:loading="showLoader"
                        @vuetable:loaded="hideLoader">

                        <template slot="actions" scope="props">
                            <div class="btn-group">
                                <button class="btn bg-danger btn-xs" @click="delRow(props.rowData)">Delete</button>
                            </div>
                        </template>
                        
                    </vuetable>					
                </div>
                
                <div class="data-table-pagination">
                    <vuetable-pagination-info ref="paginationInfo"
                        :info-template="paginationInfoTemplate">
                    </vuetable-pagination-info>
                    <vuetable-pagination ref="pagination"
                        @vuetable-pagination:change-page="onChangePage"
                        :css="css.pagination">
                    </vuetable-pagination>			
                </div>
                            
            </div>
        </div>
    </div>

</div>
<!-- /simple panel -->
@endsection	

@section('init')
<script type="text/x-template" id="expandtemplate">
	<div @click="onClick">
		<div class="panel panel-primary panel-bordered">
			<div class="panel-heading">
				<h6 class="panel-title">Preview<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			</div>
			<div class="panel-body">
				<div class="row">					
					<div class="col-md-6">						
						<div class="table-responsive">                            
                            
                            <img :src="'{{ base_url('uploads/') }}'+rowData.image_file_name" alt="" width="100%">
							
						</div>
					</div>
				</div>
			</div>
		</div>				    
	</div>
</script>


<script>

Vue.component('my-detail-row', {
	template: '#expandtemplate',
	props: {
		rowData: {
			type: Object,
			required: true
		}
	},
	methods: {
		onClick (event) {
			console.log('my-detail-row: on-click', event.target)
		},		
	},
	filters: {
		formatNumber(value) {
			if (value == null) return ''
			return $.number(value, 0)
		},	
		formatDate(value, fmt) {
			if (value == null) return ''
			fmt = (typeof fmt == 'undefined') ? 'D MMM YYYY' : fmt
			return moment(value, 'YYYY-MM-DD').format(fmt)
		},
	}
})

Vue.use(Vuetable);
var vm = new Vue({
	el: '#app',	
	data: {
		loading: '',
		searchFor: '',
		columns: [
			{
				name: 'image_id',
				title: '#',
				sortField: 'image_id',
			},
			{
				name: 'image_thumb',
				title: 'Thumbnail',	
				callback: 'load_image',
			},   
            {
				name: 'image_title',
				title: 'Title',
				sortField: 'image_title',
			},
			{
				name: 'image_caption',
				title: 'Caption',
				sortField: 'image_caption',
			},
            {
				name: 'image_file_size',
				title: 'Size (Bytes)',
				sortField: 'image_file_size',
			},                     
			{
				name: 'attached',
				title: 'Status',
				sortField: 'attached',
				callback: 'status'
			}, 
			{
				name: '__slot:actions',
				title: 'Actions',
			}			
		],		
		moreParams: [],
		sortOrder: [{
			field: 'image_id',
			direction: 'desc'
		}],				
		css: {
			table: {
				tableClass: 'table table-xxs',
				ascendingIcon: 'icon-arrow-up22',
				descendingIcon: 'icon-arrow-down22',	
			},		
			pagination: {
				wrapperClass: "btn-group",
				activeClass: "active",
				disabledClass: "disabled",
				pageClass: "btn btn-default",
				linkClass: "btn btn-default",
				icons: {
					first: "icon-chevron-left",
					prev: "icon-arrow-left32",
					next: "icon-arrow-right32",
					last: "icon-chevron-right"
				}
			}
		},
		//paginationComponent: 'vuetable-pagination',
		perPage: 20,
		paginationInfoTemplate: '<strong>Showing record</strong> {from} to {to} from {total} item(s)',
		
	},	
	methods: {			
		setFilter () {
			this.moreParams = {
				'filter': this.searchFor
			}
			this.$nextTick(function() {
				this.$refs.vuetable.refresh()
			})
		},
		
		formatDate(value, fmt) {
			if (value == null) return ''
			fmt = (typeof fmt == 'undefined') ? 'D MMM YYYY' : fmt
			return moment(value, 'YYYY-MM-DD').format(fmt)
		},
		formatNumber(value, fmt) {
			if (value == null) return ''
			return $.number(value, fmt)
		},
		
		status(value) {
			return value == '1'
                ? '<span class="label label-sm label-success">attached</span>'
                : '<span class="label label-sm label-danger">unattached</span>'
		},
		
		resetFilter () {
			this.searchFor = ''
			this.setFilter()
		},
		showLoader () {
			this.loading = 'loading'
		},
		hideLoader () {
			this.loading = ''
		},
		
		load_image(value) {			
			return '<img src="{{base_url('uploads/')}}'+value+'" width="80">'			
		},	
		
		detailRow(rowData){						
			$(location).attr('href', '{{ site_url('media/edit/') }}' + rowData.image_id)
		},
		deleteRow(rowData){					
		},		
		onPaginationData (tablePagination) {
			this.$refs.paginationInfo.setPaginationData(tablePagination)
			this.$refs.pagination.setPaginationData(tablePagination)
		},
		onChangePage (page) {
			this.$refs.vuetable.changePage(page)
		},		
		onInitialized (fields) {
			console.log('onInitialized', fields)
			this.vuetableFields = fields
		},
		
		onCellClicked (data, field, event) {
			console.log('cellClicked: ', field.name)
			this.$refs.vuetable.toggleDetailRow(data.image_id)
		},
		
		onDataReset () {
			console.log('onDataReset')
			this.$refs.paginationInfo.resetData()
			this.$refs.pagination.resetData()
		},
		
	},
	
})    
</script>
@endsection			