@extends('admin.layouts.master')

@section('js')
<script type="text/javascript" src="{{ base_url() }}assets/js/validator/validator.min.js"></script>
<script type="text/javascript" src="{{ base_url() }}assets/js/plugins/loaders/blockui.min.js"></script>
@endsection

@section('page_title')
	{{ $title }}
@endsection

@section('page_subtitle')
	Blank Panel
@endsection

@section('breadcrumb')
<ul class="breadcrumb">
	<li><a href="{{ site_url() }}"><i class="icon-home2 position-left"></i> Home</a></li>
</ul>
@endsection

@section('main_content')	
<div class="panel panel-flat">
	<div class="panel-heading">
	
		<?php if($this->session->flashdata('pk_message')): ?>
			<div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
				<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
				{{ $this->session->flashdata('pk_message') }}
			</div>
		<?php endif ?>
	
		<h5 class="panel-title">Edit Klien</h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>				
			</ul>
		</div>
	</div>

	<div class="panel-body">
		<?php echo form_open('admanagement/update_client/'.$post->client_id,'id="clientSave" data-toggle="validator"') ?>
		
		<div class="form-group">
			<label>Nama Klien</label>
			<input type="text" name="client_name" class="form-control" placeholder="Nama Klien" required value="{{ $post->client_name }}">	
			<span class="help-block with-errors"></span>
		</div>
		
		<div class="form-group">
			<label>Alamat</label>
			<textarea name="client_address" rows="3" class="form-control" placeholder="Alamat Klien" required>{{ $post->client_address }}</textarea>
			<span class="help-block with-errors"></span>
		</div>
		
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label>Telepon</label>
					<input type="text" name="client_phone" class="form-control" placeholder="Telepon Klien" value="{{ $post->client_phone }}" required>	
					<span class="help-block with-errors"></span>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>Email</label>
					<input type="email" name="client_email" class="form-control" placeholder="Email Klien" value="{{ $post->client_email }}" required>	
					<span class="help-block with-errors"></span>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<label>Nama Perwakilan Klien</label>
			<input type="text" name="client_cp" class="form-control" placeholder="Nama perwakilan klien" value="{{ $post->client_cp}}" required>	
			<span class="help-block with-errors"></span>
		</div>
		
		<div class="form-group no-margin-bottom">
			<button type="submit" id="block-page" class="btn btn-danger btn-block btn-lg">Save Klien</button>
		</div>
		<?php echo form_close() ?>	
	</div>
</div>
@endsection		

@section('init')
<script>

$(function() {

	$('#clientSave').validator().on('submit', function(e) {
		if (e.isDefaultPrevented()) {
			console.log('Not Submit');
		} else {		
			$.blockUI({ 
				message: '<i class="icon-spinner4 spinner"></i>',
				timeout: 0, //unblock after 2 seconds
				overlayCSS: {
					backgroundColor: '#1b2024',
					opacity: 0.8,
					zIndex: 1200,
					cursor: 'wait'
				},
				css: {
					border: 0,
					color: '#fff',
					padding: 0,
					zIndex: 1201,
					backgroundColor: 'transparent'
				}
			});		
		}
	})	
	
})

</script>
@endsection		