@extends('admin.layouts.master')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css">
@endsection

@section('js')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.js"></script>
@endsection

@section('page_title')
	Backup Database
@endsection

@section('page_subtitle')
	Backup Database Manually
@endsection

@section('breadcrumb')
<ul class="breadcrumb">
	<li><a href="{{ site_url('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
</ul>
@endsection

@section('main_content')
<div class="panel panel-flat">
	<div class="panel-heading">
		<h6 class="panel-title text-bold">Backup List</h6>
		<div class="heading-elements">
			<button class="btn btn-danger" id="backup_now">Backup Now</button>
		</div>
	</div>
	
	<div class="panel-body">
	
		<?php if($this->session->flashdata('pk_msg')): ?>
		<div class="alert alert-info">
			<?php echo $this->session->flashdata('pk_msg'); ?>
		</div>
		<?php endif ?>
		
		<?php if(empty($backups)): ?>
		<div class="alert alert-warning">
			You have no backup, click <strong>Backup Now</strong> to backup your database
		</div>		
		<?php else: ?>
		<div class="table-responsive">
			<table class="table table-xxs">
				<thead>
					<tr>
						<th>File</th>
						<th>Date</th>
						<th>Time</th>
						<th>User</th>
						<th>Action</th>
						
					</tr>
				</thead>
				<tbody>
					@foreach($backups as $b)
					<?php $user = $this->ion_auth->user()->row($b->user_id); ?>
					<tr class="row-{{ $b->user_id }}">
						<td>{{ $b->backup_file_name }}</td>
						<td>{{ date_format(date_create($b->backup_date), 'j F Y') }}</td>
						<td>{{ $b->backup_time }}</td>
						<td>{{ $user->first_name }}</td>
						<td>
							<div class="btn-group">
								<a href="{{ site_url('backups/download/'.$b->backup_id) }}" class="btn btn-primary btn-xs">Download</a>
								<a href="{{ site_url('backups/delete/'.$b->backup_id) }}" class="btn btn-warning btn-xs">Delete</a>
							</div>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>					
		</div>
		<?php endif ?>
	</div>
	
</div>
@endsection

@section('init')
<script>
$(document).ready(function(){
	$('#backup_now').click(function(){
		$.get('{{ site_url('backups/backup') }}', function(data, status){
			swal({
				title: data.response.message,   
				text: "Backup OK, filename "+data.response.file_name, 
				type: "success",
				onClose: function(){
					$(location).attr('href', '{{ site_url('backups') }}')
				}
			})	
		});
	})
	
})
</script>
@endsection			