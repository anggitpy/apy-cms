<div class="sidebar sidebar-main">
	<div class="sidebar-content">

		<!-- User menu -->
		<div class="sidebar-user">
			<div class="category-content">
				<div class="media">
					<a href="#" class="media-left"><img src="{{ base_url() }}assets/images/image.png" class="img-circle img-sm" alt=""></a>
					<div class="media-body">
						<span class="media-heading text-semibold">{{ get_user()->first_name }}</span>
						<div class="text-size-mini text-muted">
							<i class="icon-user text-size-small"></i> &nbsp; {{ get_user()->email }}							
						</div>
					</div>

					<div class="media-right media-middle">
						<ul class="icons-list">
							<li>
								<a href="#"><i class="icon-cog3"></i></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- /user menu -->

		<!-- Main navigation -->
		<div class="sidebar-category sidebar-category-visible">
			<div class="category-content no-padding">
				<ul class="navigation navigation-main navigation-accordion">

					<!-- Main -->
					<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
					<li class="<?php if(alink(1,'dashboard') || alink(1,'')) echo 'active' ?>"><a href="{{ site_url('dashboard') }}"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
					
					<li class="<?php if(alink(1,'auth')) echo 'active' ?>">
						<a href="#"><i class="icon-users"></i> <span>Users</span></a>
						<ul>
							<li class="<?php if(alink(1,'auth') && alink(2,'')) echo 'active' ?>"><a href="{{ site_url('auth') }}">Users List</a></li>
							<li class="<?php if(alink(1,'auth') && alink(2,'create_user')) echo 'active' ?>"><a href="{{ site_url('auth/create_user') }}">Create User</a></li>
							<li class="<?php if(alink(1,'auth') && alink(2,'create_group')) echo 'active' ?>"><a href="{{ site_url('auth/create_group') }}">Create Group</a></li>
						</ul>
					</li>	

					<li class="<?php if(alink(1,'posts')|| alink(1,'')) echo 'active' ?>">
						<a href="#"><i class="icon-newspaper"></i> <span>Posts</span></a>
						<ul>
							<li class="<?php if(alink(1,'posts') && alink(2,'')) echo 'active' ?>"><a href="{{ site_url('posts') }}">List</a></li>
							<li class="<?php if(alink(1,'posts') && alink(2,'add')) echo 'active' ?>"><a href="{{ site_url('posts/add') }}">Add</a></li>
						</ul>
					</li>
					
					<li class="<?php if(alink(1,'breaking') || alink(1,'')) echo 'active' ?>"><a href="{{ site_url('breaking') }}"><i class="icon-folder"></i> <span>Breaking News</span></a></li>
					
					<li class="<?php if(alink(1,'category') || alink(1,'')) echo 'active' ?>"><a href="{{ site_url('category') }}"><i class="icon-folder6"></i> <span>Categories</span></a></li>
                                       
					<li class="<?php if(alink(1,'tags') || alink(1,'')) echo 'active' ?>"><a href="{{ site_url('tags') }}"><i class="icon-folder6"></i> <span>Tags</span></a></li>
					<li class="<?php if(alink(1,'subtitles') || alink(1,'')) echo 'active' ?>"><a href="{{ site_url('subtitles') }}"><i class="icon-folder6"></i> <span>Subtitles</span></a></li>
					
					<li class="<?php if(alink(1,'media') || alink(1,'')) echo 'active' ?>"><a href="{{ site_url('media') }}"><i class="icon-folder6"></i> <span>Media</span></a></li>										
					
					<li class="<?php if(alink(1,'admanagement')) echo 'active' ?>">
						<a href="#"><i class="icon-cabinet"></i> <span>Ad Management</span></a>
						<ul>
							<li class="<?php if(alink(1,'admanagement') && alink(2,'')) echo 'active' ?>"><a href="{{ site_url('admanagement') }}">Ads Materials</a></li>
							<li class="<?php if(alink(1,'admanagement') && alink(2,'clients')) echo 'active' ?>"><a href="{{ site_url('admanagement/clients') }}">Clients</a></li>
							<li class="<?php if(alink(1,'admanagement') && alink(2,'inventory')) echo 'active' ?>"><a href="{{ site_url('admanagement/inventory') }}">Inventory</a></li>
						</ul>
					</li>
					
					<li class="<?php if(alink(1,'theme') || alink(1,'')) echo 'active' ?>"><a href="{{ site_url('theme') }}"><i class="icon-mustache"></i> <span>Themes</span></a></li>                   
					
                    <!-- /main -->

				</ul>
			</div>
		</div>
		<!-- /main navigation -->

	</div>
</div>