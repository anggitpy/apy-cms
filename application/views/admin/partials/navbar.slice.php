<div class="navbar navbar-inverse">
	<div class="navbar-header">
		<a class="navbar-brand" href="{{ site_url('dashboard') }}"><!--<img src="{{ base_url() }}assets/images/logo_light.png" alt="">-->{{ settings('company_name') }}</a>

		<ul class="nav navbar-nav visible-xs-block">
			<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
			<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
		</ul>
	</div>

	<div class="navbar-collapse collapse" id="navbar-mobile">
		<ul class="nav navbar-nav">
			<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
		</ul>

		<ul class="nav navbar-nav navbar-right">			

			<li>
				<a href="{{ site_url('settings') }}">
					<i class="icon-cog3"></i>
					<span class="visible-xs-inline-block position-right">Icon link</span>
				</a>						
			</li>
            <li>
				<a href="{{ site_url() }}" target="_blank">
					Preview
				</a>						
			</li>

			<li class="dropdown dropdown-user">
				<a class="dropdown-toggle" data-toggle="dropdown">
					<img src="{{ base_url() }}assets/images/image.png" alt="">
					<span>{{ get_user()->first_name }}</span>
					<i class="caret"></i>
				</a>

				<ul class="dropdown-menu dropdown-menu-right">
					<li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
					<li><a href="{{ site_url('last_login') }}"><i class="icon-user"></i> Last Login</a></li>
					<li><a href="{{ site_url('backups') }}"><span class="badge badge-warning pull-right">{{ count_backup() }}</span> <i class="icon-usb-stick"></i> Backups</a></li>
					<li><a href="{{ site_url('dashboard/cache_management') }}"><i class="icon-database4"></i> Cache</a></li>
					<li class="divider"></li>
					<li><a href="{{ site_url('settings') }}"><i class="icon-cog5"></i> App settings</a></li>
					<li><a href="{{ site_url('auth/logout') }}"><i class="icon-switch2"></i> Logout</a></li>
				</ul>
			</li>
		</ul>
	</div>
</div>