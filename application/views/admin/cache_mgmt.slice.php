@extends('admin.layouts.master')

@section('page_title')
	Cache Viewer
@endsection

@section('page_subtitle')
	List of Cached Data
@endsection

@section('breadcrumb')
<ul class="breadcrumb">
	<li><a href="{{ site_url('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
</ul>
@endsection

@section('breadcrumb_elements')
<ul class="breadcrumb-elements">
	<li><a href="{{ site_url('dashboard/flushm') }}" id="flushCache"><i class="icon-reset position-left"></i> Flush All Cache</a></li>	
</ul>
@endsection

@section('main_content')	
<!-- Simple panel -->					
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Cache Type <strong>{{ $cache_type }}</strong></h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>				
			</ul>
		</div>
	</div>

	<div class="panel-body">
    
        <?php if($this->session->flashdata('pk_msg')): ?>
		<div class="alert alert-info">
			<?php echo $this->session->flashdata('pk_msg'); ?>
		</div>
		<?php endif ?>
		
        <?php if($cache_type == 'file'): ?>
		<div class="table-responsive">
			<table class="table table-xxs">
				<thead>
					<tr>
						<th>Key</th>						
						<th>Size</th>						
						<th>Date</th>						
					</tr>
				</thead>
				<tbody>
					<?php foreach($cache_info as $key => $c): ?>
					<tr>
						<td><?php echo $key ?></td>
						<td><?php echo $c['size'] ?> Bytes</td>						
						<td><?php echo unix_to_human($c['date']) ?></td>						
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>					
		</div>
        <?php endif ?>
        
        <?php if($cache_type == 'memcached'): ?>        
        <div class="table-responsive">
			<table class="table table-xxs">
				<thead>
					<tr>
						<th>Var</th>						
						<th>Value</th>						
					</tr>
				</thead>
				<tbody>
					<?php foreach($cache_info as $key => $c): ?>
					<tr>
						<td><?php echo $key ?></td>
						<td><?php echo $c ?></td>						
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>					
		</div>
        <?php endif ?>
        
	</div>
</div>
<!-- /simple panel -->
@endsection				