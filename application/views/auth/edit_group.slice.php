@extends('admin.layouts.master')

@section('page_title')
	<?php echo lang('edit_group_heading');?>
@endsection

@section('page_subtitle')
	<?php echo lang('edit_group_subheading');?>
@endsection

@section('breadcrumb')
<ul class="breadcrumb">
	<li><a href="{{ site_url() }}"><i class="icon-home2 position-left"></i> Home</a></li>
	<li><a href="{{ site_url('auth') }}"><i class="icon-user position-left"></i> Users List</a></li>
	<li class="active">Edit Group</li>
</ul>
@endsection

@section('breadcrumb_elements')
<ul class="breadcrumb-elements">
	<li><a href="{{ site_url('auth/create_user') }}"><i class="icon-user position-left"></i> Create User</a></li>	
	<li><a href="{{ site_url('auth/create_group') }}"><i class="icon-users position-left"></i> Create Group</a></li>	
</ul>
@endsection

@section('main_content')
<?php echo form_open(current_url(),'class="form-horizontal"');?>
	<div class="panel panel-flat">
		<div class="panel-heading">
			<div class="row">
				<div class="col-md-8 col-md-offset-1">
					<h4 class="panel-title">Edit Group</h4>
				</div>
			</div>
		</div>

		<div class="panel-body">
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
				
					<?php echo $message;?>
					
					<div class="form-group">
						<label class="col-md-3 control-label text-right"><?php echo lang('edit_group_name_label', 'group_name');?></label>
						<div class="col-md-5">
							<?php echo form_input($group_name);?>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-3 control-label text-right"><?php echo lang('edit_group_desc_label', 'description');?></label>
						<div class="col-md-5">
							<?php echo form_input($group_description);?>
						</div>
					</div>
										

					<div class="text-right">
						<button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
					</div>
					
				</div>
			</div>
		</div>
	</div>
<?php echo form_close() ?>
@endsection