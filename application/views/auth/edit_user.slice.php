@extends('admin.layouts.master')

@section('page_title')
	<?php echo lang('edit_user_heading');?>
@endsection

@section('page_subtitle')
	<?php echo lang('edit_user_subheading');?>
@endsection

@section('breadcrumb')
<ul class="breadcrumb">
	<li><a href="{{ site_url() }}"><i class="icon-home2 position-left"></i> Home</a></li>
	<li><a href="{{ site_url('auth') }}"><i class="icon-user position-left"></i> Users List</a></li>
	<li class="active">Edit User</li>
</ul>
@endsection

@section('breadcrumb_elements')
<ul class="breadcrumb-elements">
	<li><a href="{{ site_url('auth/create_user') }}"><i class="icon-user position-left"></i> Create User</a></li>	
	<li><a href="{{ site_url('auth/create_group') }}"><i class="icon-users position-left"></i> Create Group</a></li>	
</ul>
@endsection

@section('main_content')

<?php echo form_open(uri_string(),'class="form-horizontal"');?>
	<div class="panel panel-flat">
		<div class="panel-heading">
			<div class="row">
				<div class="col-md-8 col-md-offset-1">
					<h4 class="panel-title">Edit User <?php echo $message;?></h4>
				</div>
			</div>
		</div>

		<div class="panel-body">
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">			

					<div class="form-group">
						<label class="col-md-4 control-label text-right"><?php echo lang('edit_user_fname_label');?></label>
						<div class="col-md-4">
							<?php echo form_input($first_name);?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label text-right"><?php echo lang('edit_user_lname_label');?></label>
						<div class="col-md-4">
							<?php echo form_input($last_name);?>
						</div>							
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label text-right"> <?php echo lang('edit_user_company_label', 'company');?></label>
						<div class="col-md-4">
							<?php echo form_input($company);?>
						</div>							
					</div>	
					
					<div class="form-group">
						<label class="col-md-4 control-label text-right"><?php echo lang('edit_user_phone_label', 'phone');?></label>
						<div class="col-md-4">
							<?php echo form_input($phone);?>
						</div>							
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label text-right"><?php echo lang('edit_user_password_label', 'password');?></label>
						<div class="col-md-8">
							<?php echo form_input($password);?>
						</div>							
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label text-right"><?php echo lang('edit_user_password_confirm_label', 'password_confirm');?></label>
						<div class="col-md-8">
							<?php echo form_input($password_confirm);?>
						</div>							
					</div>
					
					<?php if ($this->ion_auth->is_admin()): ?>
					<div class="form-group">
						<label class="col-md-4 control-label text-right">Groups</label>
						<div class="col-md-8">
							<div class="form-group pt-15">
								<?php 
									foreach ($groups as $group):											
										$gID = $group['id'];
										$checked = null;
										//$domclass = null;
										$item = null;
										foreach($currentGroups as $grp) {
											if ($gID == $grp->id) {
												$checked= ' checked="checked"';
												//$domclass='class="checked"';
												break;
											}
										}
								?>
								<div class="checkbox">									
									<label>										
										<input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
										<?php echo htmlspecialchars($group['description'],ENT_QUOTES,'UTF-8');?> &mdash; <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
									</label>
								</div>
								<?php endforeach ?>
							</div>
							
						</div>
						
					</div>
					<?php endif ?>

					<?php echo form_hidden('id', $user->id);?>
					<?php echo form_hidden($csrf); ?>							

					<div class="text-right">
						<button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
					</div>
					
				</div>
			</div>
		</div>
	</div>
<?php echo form_close() ?>

@endsection