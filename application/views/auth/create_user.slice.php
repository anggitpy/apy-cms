@extends('admin.layouts.master')

@section('page_title')
	<?php echo lang('create_user_heading');?>
@endsection

@section('page_subtitle')
	<?php echo lang('create_user_subheading');?>
@endsection

@section('breadcrumb')
<ul class="breadcrumb">
	<li><a href="{{ site_url() }}"><i class="icon-home2 position-left"></i> Home</a></li>
	<li><a href="{{ site_url('auth') }}"><i class="icon-user position-left"></i> Users List</a></li>
	<li class="active">Create User</li>
</ul>
@endsection

@section('breadcrumb_elements')
<ul class="breadcrumb-elements">
	<li><a href="{{ site_url('auth/create_user') }}"><i class="icon-user position-left"></i> Create User</a></li>	
	<li><a href="{{ site_url('auth/create_group') }}"><i class="icon-users position-left"></i> Create Group</a></li>	
</ul>
@endsection

@section('main_content')

<?php echo form_open('auth/create_user','class="form-horizontal" id="signupForm"');?>
	<div class="panel panel-flat">
		<div class="panel-heading">
			<div class="row">
				<div class="col-md-8 col-md-offset-1">
					<h4 class="panel-title">Add User</h4>
				</div>
			</div>
		</div>

		<div class="panel-body">
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
				
					<div id="infoMessage"><?php echo $message;?></div>
					
					<div class="form-group">
						<label class="col-md-3 control-label text-right"><?php echo lang('create_user_fname_label');?></label>
						<div class="col-md-5">
							<?php echo form_input($first_name);?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label text-right"><?php echo lang('create_user_lname_label');?></label>
						<div class="col-md-5">
							<?php echo form_input($last_name);?>
						</div>							
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label text-right"><?php echo lang('create_user_company_label');?></label>
						<div class="col-md-4">
							<?php echo form_input($company);?>
						</div>							
					</div>							
												
					<?php
					if($identity_column!=='email') 
					{
						echo '<div class="form-group">';
						echo '<label class="col-md-3 control-label text-right">';
						echo lang('create_user_identity_label');
						echo form_error('identity');
						echo '</label>';									
						echo '<div class="col-md-4">';
						echo form_input($identity);
						echo '<span class="help-block">This field is Required</span>';
						echo '</div>';
						echo '</div>';
					}
					?>
					
					<div class="form-group">
						<label class="col-md-3 control-label text-right"><?php echo lang('create_user_email_label');?></label>
						<div class="col-md-9">
							<div class="input-group">
								<span class="input-group-addon">
									<i class="icon-envelope"></i>
								</span>
								<?php echo form_input($email);?>
							</div>									
						</div>							
					</div>
					
					<div class="form-group">
						<label class="col-md-3 control-label text-right"><?php echo lang('create_user_phone_label');?></label>
						<div class="col-md-4">
							<?php echo form_input($phone);?>
						</div>							
					</div>
					
					<div class="form-group">
						<label class="col-md-3 control-label text-right"><?php echo lang('create_user_password_label');?></label>
						<div class="col-md-4">
							<?php echo form_input($password);?>
						</div>							
					</div>
					
					<div class="form-group">
						<label class="col-md-3 control-label text-right"><?php echo lang('create_user_password_confirm_label');?></label>
						<div class="col-md-4">
							<?php echo form_input($password_confirm);?>
						</div>							
					</div>						

					<div class="text-right">
						<button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</form>

@endsection