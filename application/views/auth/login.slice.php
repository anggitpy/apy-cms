@extends('admin.layouts.login')

@section('login_form')

<?php echo form_open('auth/login');?>
	<div class="panel panel-body login-form">
		<div class="text-center">
			<div class="icon-object border-warning-400 text-warning-400"><i class="icon-people"></i></div>
			<h5 class="content-group-lg">Login to your account <small class="display-block">Enter your credentials</small></h5>
		</div>
		
		<?php echo $message;?>

		<div class="form-group has-feedback has-feedback-left no-margin-bottom">
			<?php echo form_input($identity);?>
			<div class="form-control-feedback">
				<i class="icon-user text-muted"></i>
			</div>
			<span class="help-block with-errors"></span>
		</div>

		<div class="form-group has-feedback has-feedback-left">
			<?php echo form_input($password);?>
			<div class="form-control-feedback">
				<i class="icon-lock2 text-muted"></i>
			</div>
			<span class="help-block with-errors"></span>
		</div>

		<div class="form-group login-options">
			<div class="row">
				<div class="col-sm-6">
					<label class="checkbox-inline">
						<?php echo form_checkbox('remember', '1', FALSE, 'id="remember" class="styled"');?>
						Remember
					</label>
				</div>
				
			</div>
		</div>

		<div class="form-group">
			<button type="submit" class="btn bg-blue btn-block" id="block-page">Login <i class="icon-circle-right2 position-right"></i></button>
		</div>

		<span class="help-block text-center no-margin">
			Copyright &copy; 2017 <a href="#" target="_blank">Poskotanews</a>
		</span>
	</div>
<?php echo form_close() ?>
	
@endsection

@section('init')
<script>
$(function() {
	$('#block-page').on('click', function() {
        $.blockUI({ 
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 0, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent'
            }
        });
    });
})
</script>
@endsection



