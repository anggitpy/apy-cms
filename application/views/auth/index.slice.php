@extends('admin.layouts.master')

@section('js')

<script>
<?php foreach ($users as $user):?>
$(function() {
	$('#modal_remote_<?php echo $user->id ?>').on('show.bs.modal', function() {
		$(this).find('.modal-inner').load('<?php echo site_url('auth/deactivate/'.$user->id) ?>', function() {
			// something
		});
	});
})
<?php endforeach ?>
</script>

@endsection

@section('page_title')
	{{ lang('index_heading') }}
@endsection

@section('page_subtitle')
	{{ lang('index_subheading') }}
@endsection

@section('breadcrumb')
<ul class="breadcrumb">
	<li><a href="{{ site_url() }}"><i class="icon-home2 position-left"></i> Home</a></li>
	<li class="active">User</li>
</ul>
@endsection

@section('breadcrumb_elements')
<ul class="breadcrumb-elements">
	<li><a href="{{ site_url('auth/create_user') }}"><i class="icon-user position-left"></i> Create User</a></li>	
	<li><a href="{{ site_url('auth/create_group') }}"><i class="icon-users position-left"></i> Create Group</a></li>	
</ul>
@endsection

@section('main_content')

<?php echo $message;?>

<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">User Management<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
			</ul>
		</div>
	</div>

	<div class="panel-body">		

        <div class="table-responsive">
            <table class="table table-hover table-xxs">
                <thead>
                    <tr>
                        <th><?php echo lang('index_fname_th');?></th>
                        <th><?php echo lang('index_lname_th');?></th>
                        <th><?php echo lang('index_email_th');?></th>
                        <th><?php echo lang('index_groups_th');?></th>
                        <th><?php echo lang('index_status_th');?></th>
                        <th><?php echo lang('index_action_th');?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($users as $user):?>
                        <tr>
                            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
                            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
                            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
                            <td>
                                <?php foreach ($user->groups as $group):?>
                                    <?php echo anchor("auth/edit_group/".$group->id, htmlspecialchars($group->description,ENT_QUOTES,'UTF-8'),'class="btn btn-warning btn-xs"') ;?>
                                <?php endforeach?>
                            </td>
                            <td>
                                <?php //echo ($user->active) ? anchor("auth/deactivate/".$user->id, lang('index_active_link'),'class="btn btn-default btn-xs"') : anchor("auth/activate/". $user->id, lang('index_inactive_link'),'class="btn btn-success btn-xs"');?>
                                <?php if($user->active): ?>
                                    <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modal_remote_<?php echo $user->id ?>"><?php echo lang('index_active_link')?> <i class="icon-check position-right"></i></button>
                                <?php else: ?>
                                    <?php echo anchor("auth/activate/". $user->id, lang('index_inactive_link').' <i class="icon-check position-right"></i>','class="btn btn-success btn-xs"');?>
                                <?php endif ?>
                            </td>
                            <td><?php echo anchor("auth/edit_user/".$user->id, 'Edit', 'class="btn btn-danger btn-xs"') ;?></td>
                        </tr>
                        
                         <!-- Remote source -->
                        <div id="modal_remote_<?php echo $user->id ?>" class="modal">
                            <div class="modal-dialog modal-xs">
                                <div class="modal-content">
                                    <div class="modal-inner">
                                    </div>								
                                </div>
                            </div>
                        </div>
                        <!-- /remote source -->

                        
                    <?php endforeach;?>
                </tbody>
            </table>
                     
        </div>
        
        <hr>
        <?php echo $links ?>
    
    </div>
    
    
</div>

@endsection