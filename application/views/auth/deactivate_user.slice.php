<div class="modal-header">									
	<h5 class="modal-title"><?php echo lang('deactivate_heading');?></h5>
	<p><?php echo sprintf(lang('deactivate_subheading'), $user->first_name);?></p>
</div>


<?php echo form_open("auth/deactivate/".$user->id);?>

<div class="modal-body">
	<div class="form-group">
		<div class="radio">
			<label>
				<input type="radio" name="confirm" value="yes" checked="checked" />
				<?php echo lang('deactivate_confirm_y_label', 'confirm');?>
			</label>
		</div>

		<div class="radio">
			<label>
				<input type="radio" name="confirm" value="no" />
				<?php echo lang('deactivate_confirm_n_label', 'confirm');?>
			</label>
		</div>
	</div>
	<?php echo form_hidden($csrf); ?>
	<?php echo form_hidden(array('id'=>$user->id)); ?>									
</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	<button type="submit" class="btn btn-primary">Save changes</button>
</div>

<?php echo form_close();?>